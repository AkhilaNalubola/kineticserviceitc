import React from 'react';
import { Router } from '@reach/router';
import {
  compose,
  lifecycle,
  withHandlers,
  withProps,
  withState,
} from 'recompose';
import { Redirect } from '@reach/router';
import {
  ActivityFeed,
  ErrorUnexpected,
  Loading,
  Utils,
} from '@kineticdata/bundle-common';
import { connect } from './redux/store';

import { actions as categoriesActions } from './redux/modules/servicesApp';
import { actions as favoriteActions } from './redux/modules/favorite';
import { actions as submissionCountActions } from './redux/modules/submissionCounts';
import { actions as formActions } from './redux/modules/forms';
import { actions as profileActions } from '../../app/src/redux/modules/profile';

import { PageTitle } from './components/shared/PageTitle';
import { Login } from './components/Login';
import { CategoryListContainer } from './components/category_list/CategoryListContainer';
import { CategoryContainer } from './components/category/CategoryContainer';
import { CatalogSearchResultsContainer } from './components/search_results/CatalogSearchResultsContainer';
import { FormContainer } from './components/form/FormContainer';
import { FormListContainer } from './components/form_list/FormListContainer';
import { RequestList } from './components/request_list/RequestList';
import { RequestShowContainer } from './components/request/RequestShowContainer';
import { Settings } from './components/settings/Settings';
import { I18n } from '@kineticdata/react';
import { Home } from './components/its_home/Home';
import { RequestService } from './components/its_my_activity/RequestService';
import { RequestForm } from './components/its_my_activity/RequestForm';
import { Header } from './components/its_header/Header';
import { OnboardingBundle } from './components/its_my_activity/OnboardingBundle';
import { MyActivity } from './components/its_my_activity/MyActivity';

import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { ActivityApproval } from './components/its_my_activity/ActivityApproval';
import { ActivityDetails } from './components/its_my_activity/ActivityDetails';
import { AdminActivityDetail } from './components/its_my_activity/AdminActivityDetail';
import { RequestActivityContainer } from './components/its_my_activity/RequestActivityContainer';
import { MyDevices } from './components/my_devices/MyDevices';
import { NotificationsList } from './components/its_notifications/Notifications';
import { Profile } from './components/its_profile/Profile';
import { Catalog as ITSCatalog } from './components/its_catalog/Catalog';
import { Help } from './components/its_help/Help';
import { Configurations } from './components/its_config/Configurations';
import { Footer } from './components/its_footer/Footer';
import { Article } from './components/its_article/Article';

/*****************************************************************************
 *** PRIVATE APP
 *****************************************************************************/

export const requestFeedKey = 'request-activity-feed';

const SubmissionRedirect = props => (
  <Redirect
    to={`${props.appLocation}/requests/request/${props.id}/${
      props.location.search.includes('review') ? 'review' : 'activity'
    }`}
    noThrow
  />
);

const AppComponent = props => {
  const headerBg = Utils.getAttributeValue(
    props.space,
    'Header Background Color',
    '#000',
  );
  const headerTxt = Utils.getAttributeValue(
    props.space,
    'Header Text Color',
    '#fff',
  );
  const footerBg = Utils.getAttributeValue(
    props.space,
    'Footer Background Color',
    '#000',
  );
  const footerTxt = Utils.getAttributeValue(
    props.space,
    'Footer Text Color',
    '#fff',
  );
  const supportInfo = Utils.getAttributeValue(props.space, 'Support Info', []);
  const footerCopyrights = Utils.getAttributeValue(
    props.space,
    'Footer Copyright',
    '',
  );
  const displayFooter = Utils.getAttributeValue(
    props.space,
    'Display Footer',
    1,
  );
  const chatbotHeader = Utils.getAttributeValue(
    props.space,
    'Chatbot Header Background',
    '#ccc',
  );
  const chatbotWidget = Utils.getAttributeValue(
    props.space,
    'Chatbot Widget Color',
    '#ccc',
  );

  // useEffect(() => {
  if (props.pathname == '/kapps/services/login' || props.pathname == '/login') {
    document.getElementsByClassName('widget-floating')[0].style.display =
      'none';
  } else {
    document.getElementsByClassName('widget-floating')[0].style.display =
      'block';
  }
  console.log(
    'props in app',
    props.pathname,
    document.getElementsByClassName('widget-floating')[0].style,
  );
  const chatbot_widget = document.getElementsByClassName(
    'shared__teaser-opener',
  )[0];
  const chatbot_header = document.getElementsByClassName(
    'widget-floating__header',
  )[0];

  chatbot_header.style.backgroundColor = chatbotHeader;
  chatbot_widget.style.background = chatbotWidget;
  // console.log('test2',chatbot_header.style.background='green')
  // }, [])

  if (props.error) {
    return <ErrorUnexpected />;
  } else if (props.loading) {
    return <Loading text="App is loading ..." />;
  } else {
    return props.render({
      header:
        props.pathname == '/kapps/services/login' ? (
          ''
        ) : (
          <Header background={headerBg} textColor={headerTxt} />
        ),
      main: (
        <I18n>
          <main className="package-layout package-layout--services">
            <PageTitle parts={['Loading...']} />
            <Router>
              <Settings path="settings/*" />
              <SubmissionRedirect
                path="submissions/:id"
                appLocation={props.appLocation}
              />
              <SubmissionRedirect
                path="forms/:formSlug/submissions/:id"
                appLocation={props.appLocation}
              />
              <Login path="login" />
              <Home path="/" />
              <RequestService path="requestService" />
              <RequestForm path="requestService/:categorySlug/requestForm/:formSlug" />
              <RequestForm path="requestService/:categorySlug/requestForm/:formSlug/:submissionId" />
              <RequestService path="requestService/:categorySlug" />
              <RequestForm path="requestForm" />
              <OnboardingBundle path="bundle" />
              <MyActivity path="myActivity" />
              <ActivityApproval path="myActivity/activityApproval" />
              <ActivityDetails path="myActivity/activityDetails" />
              <AdminActivityDetail path="myActivity/adminActivityDetails" />
              <MyDevices path="myDevices" />
              <NotificationsList path="notificationsList" />
              <Profile path="profile" />
              <ITSCatalog path="catalog/:from" />
              {/*   /catalog/newRequest  , /catalog/newTrouble  , /catalog/all */}
              <Help path="help" />
              <Configurations path="config" />
              <Article path="article" />
              <RequestActivityContainer path=":type/request/:submissionId/:mode" />
              {/* <FormContainer path="request/:submissionId" />
                <FormContainer path=":type/request/:submissionId" />
                <RequestShowContainer path="request/:submissionId/:mode" /> */}
              {/* <RequestShowContainer path=":type/request/:submissionId/:mode" /> */}
            </Router>
          </main>
          {Number(displayFooter.toString()) ? (
            <Footer
              background={footerBg}
              textColor={footerTxt}
              supportInfo={JSON.parse(supportInfo)}
              copyRights={footerCopyrights}
            />
          ) : (
            ''
          )}
        </I18n>
      ),
    });
  }
};

const mapStateToProps = state => {
  return {
    loading: state.servicesApp.loading,
    error: state.servicesApp.error,
    categories: state.servicesApp.categories,
    forms: state.servicesApp.homeForms,
    submissionCounts: state.submissionCounts.data,
    pathname: state.router.location.pathname,
    appLocation: state.app.location,
    profile: state.app.profile.profileAttributes,
    space: state.app.space,
  };
};

const mapDispatchToProps = {
  fetchAppDataRequest: categoriesActions.fetchAppDataRequest,
  fetchSubmissionCountsRequest:
    submissionCountActions.fetchSubmissionCountsRequest,
  fetchFavoriteForms: favoriteActions.fetchFavoritesRequest,
  fetchProfileRequest: profileActions.fetchProfileRequest,
  fetchForms: formActions.fetchFormsRequest,
};

export const App = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withProps(props => {
    return !props.categories || props.categories.isEmpty()
      ? {
          homePageMode: 'Forms',
          homePageItems: props.forms,
        }
      : {
          homePageMode: 'Categories',
          homePageItems: props.categories,
        };
  }),
  withState(
    'settingsBackPath',
    'setSettingsBackPath',
    props => props.appLocation,
  ),
  withHandlers({
    openSettings: props => () => props.setSettingsBackPath(props.pathname),
  }),
  lifecycle({
    componentDidMount() {
      this.props.fetchAppDataRequest();
      this.props.fetchFavoriteForms();
      this.props.fetchForms();
    },
  }),
)(AppComponent);

/*****************************************************************************
 *** PUBLIC APP
 *****************************************************************************/

export const PublicAppComponent = props => {
  return props.render({
    main: (
      <I18n>
        <main className="package-layout package-layout--services">
          <PageTitle parts={['Loading...']} />
          <Router>
            <FormContainer path="forms/:formSlug" />
            <FormContainer path="forms/:formSlug/:submissionId" />
            <FormContainer path="submissions/:submissionId" />
            <Redirect from="*" to={props.authRoute} noThrow />
          </Router>
        </main>
      </I18n>
    ),
  });
};

const mapStateToPropsPublic = state => ({
  authRoute: state.app.authRoute,
});

export const PublicApp = compose(connect(mapStateToPropsPublic))(
  PublicAppComponent,
);
