import { reducer as app } from './modules/app';
import servicesAppReducer from './modules/servicesApp';
import formsReducer from './modules/forms';
import searchReducer from './modules/search';
import submissionReducer from './modules/submission';
import submissionCountsReducer from './modules/submissionCounts';
import settingsFormsReducer from './modules/settingsForms';
import settingsCategoriesReducer from './modules/settingsCategories';
import { reducer as favorite } from './modules/favorite';
import { reducer as configurations } from './modules/configurations';
import submissionsReducer from './modules/submissions';

export default {
  app,
  forms: formsReducer,
  search: searchReducer,
  servicesApp: servicesAppReducer,
  submission: submissionReducer,
  submissionCounts: submissionCountsReducer,
  settingsForms: settingsFormsReducer,
  settingsCategories: settingsCategoriesReducer,
  favorite,
  configurations,
  submissions: submissionsReducer,
};
