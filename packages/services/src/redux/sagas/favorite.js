import { takeEvery, all, call, put, select } from 'redux-saga/effects';
import { actions, types } from '../modules/favorite';
import { addToast, addToastAlert, Utils } from '@kineticdata/bundle-common';
import { selectServicesKapp } from '../../utils';
import { updateProfile, fetchForms } from '@kineticdata/react';

// Expects a list of favorite slugs to retrieve corresponding forms for.
// If payload is null, will get the values from the profile attribute.
export function* fetchFavoritesRequestSaga({ payload }) {
  const kapp = yield select(selectServicesKapp);
  const me = yield select(state => state.app.profile);

  if (!kapp) {
    yield put(
      actions.fetchFavoritesFailure({
        message: 'Services Kapp could not be found.',
      }),
    );
  } else {
    const favoriteSlugs = Array.isArray(payload)
      ? payload
      : Utils.getProfileAttributeValues(me, 'Favorite', []);

    if (favoriteSlugs.length === 0) {
      yield put(actions.fetchFavoritesSuccess([]));
    } else {
      const { forms, error } = yield call(fetchForms, {
        kappSlug: kapp.slug,
        include: 'attributes[Icon]',
        orderBy: 'name',
        q: `slug IN (${favoriteSlugs.map(s => `"${s}"`)})`,
      });

      if (error) {
        yield put(actions.fetchFavoritesFailure(error));
      } else {
        yield put(actions.fetchFavoritesSuccess(forms));
      }
    }
  }
}

// Payload should be a form slug, that will be removed if it's already a
// favorite, or added if it's not a favorite
export function* updateFavoritesRequestSaga({ payload }) {
  console.log('testefav',payload)
  const me = yield select(state => state.app.profile);
  const favorites = Utils.getProfileAttributeValues(me, 'Favorite', []);
  const isRemove = favorites.includes(payload);
  const newFavorites = isRemove
    ? favorites.filter(s => s !== payload)
    : [...favorites, payload];

  const { profile, error } = yield call(updateProfile, {
    include: 'profileAttributesMap[Favorite]',
    profile: {
      profileAttributesMap: { Favorite: newFavorites },
    },
  });

  if (error) {
    addToastAlert({
      title: 'Failed to update favorite forms',
      message: error.message,
    });
  } else {
    if (isRemove) {
      addToast('Favorite form successfully removed');
    } else {
      addToast('Favorite form successfully added');
    }

    yield all([
      put(
        actions.fetchFavoritesRequest(
          Utils.getProfileAttributeValues(profile, 'Favorite', []),
        ),
      ),
    ]);
  }
}

export function* watchFavorite() {
  yield takeEvery(types.FETCH_FAVORITES_REQUEST, fetchFavoritesRequestSaga);
  yield takeEvery(types.UPDATE_FAVORITES_REQUEST, updateFavoritesRequestSaga);
}
