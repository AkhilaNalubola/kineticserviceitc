import { takeEvery, call, put, select } from 'redux-saga/effects';
import { actions, types } from '../modules/configurations';
import { selectServicesKapp, getFeatures } from '../../utils';
import { addToast, addToastAlert, Utils } from '@kineticdata/bundle-common';
import { updateKapp } from '@kineticdata/react';

export function* updateFeaturesRequestSaga({ payload }) {
  const inputType = payload.inputType;
  delete payload.inputType;

  const stateKapp = yield select(selectServicesKapp);
  const refreshApp = yield select(state => state.app.actions.refreshApp);

  if (!stateKapp) {
    addToastAlert({
      message: 'Services Kapp could not be found.',
    });
  } else {
    let featuresStr = stateKapp.attributesMap['Feature'];
    // Convert feature array of strings to array of objects
    const featuresObjects = getFeatures(featuresStr);
    // Find index of feature that will be updated
    const featureIndex = featuresObjects.findIndex(
      feature => feature.featureLabel === payload.featureLabel,
    );

    // Update and stringify feature
    const feature = {
      ...featuresObjects[featureIndex],
      ...payload,
    };
    if (inputType === 'checkbox') {
      featuresStr[featureIndex] = JSON.stringify(feature);
    } else {
      featuresStr = featuresObjects.map((featureObj, idx) =>
        JSON.stringify(
          idx === featureIndex
            ? feature
            : { ...featureObj, isLandingPage: false },
        ),
      );
    }

    const { kapp, error } = yield call(updateKapp, {
      kappSlug: stateKapp.slug,
      kapp: {
        attributesMap: { Feature: featuresStr },
      },
    });

    if (error) {
      addToastAlert({
        message: 'There was an error updating the features.',
      });
    } else {
      addToast({
        message: 'Features were successfully updated.',
      });
      refreshApp();
    }
  }
}

export function* watchConfigurations() {
  yield takeEvery(types.UPDATE_FEATURES_REQUEST, updateFeaturesRequestSaga);
}
