import { call, put, select, takeEvery } from 'redux-saga/effects';
import { searchSubmissions, SubmissionSearch } from '@kineticdata/react';

import * as constants from '../../constants';
import { actions, types } from '../modules/submissions';

export function* fetchSubmissionsRequestSaga({ payload }) {
  const kappSlug = yield select(state => state.app.kappSlug);
  const username = yield select(state => state.app.profile.username);
  const pageToken = yield select(state => state.submissions.pageToken);

  const searchBuilder = new SubmissionSearch()
    .type(constants.SUBMISSION_FORM_TYPE)
    .limit(payload.limit)
    .includes(['details', 'values']);

  searchBuilder
    .in(`values[${constants.STATUS_FIELD}]`, payload.status)
    .or()
    .eq(`values[${constants.REQUESTED_FOR_FIELD}]`, username)
    .eq(`values[${constants.REQUESTED_BY_FIELD}]`, username)
    .eq('submittedBy', username)
    .eq('createdBy', username)
    .end();

  if (pageToken) searchBuilder.pageToken(pageToken);
  const search = searchBuilder.build();

  const { submissions, nextPageToken, error } = yield call(searchSubmissions, {
    search,
    kapp: kappSlug,
  });

  if (error) {
    yield put(actions.fetchSubmissionsFailure(error));
  } else {
    yield put(
      actions.fetchSubmissionsSuccess({
        type: payload.type,
        submissions,
        nextPageToken,
      }),
    );
  }
}

export function* watchSubmissions() {
  yield takeEvery(
    [
      types.FETCH_SUBMISSIONS_REQUEST,
      types.FETCH_SUBMISSIONS_NEXT,
      types.FETCH_SUBMISSIONS_PREVIOUS,
      types.FETCH_SUBMISSIONS_CURRENT,
    ],
    fetchSubmissionsRequestSaga,
  );
}
