import { all } from 'redux-saga/effects';

import { watchApp } from './sagas/app';
import { watchServicesApp } from './sagas/servicesApp';
import { watchForms } from './sagas/forms';
import { watchSubmission, watchSubmissionPoller } from './sagas/submission';
import { watchSubmissionCounts } from './sagas/submissionCounts';
import { watchSettingsForms } from './sagas/settingsForms';
import { watchSettingsCategories } from './sagas/settingsCategories';
import { watchFavorite } from './sagas/favorite';
import { watchConfigurations } from './sagas/configurations';
import { watchSubmissions } from './sagas/submissions';

export default function*() {
  yield all([
    watchApp(),
    watchForms(),
    watchServicesApp(),
    watchSubmission(),
    watchSubmissionPoller(),
    watchSubmissionCounts(),
    watchSettingsForms(),
    watchSettingsCategories(),
    watchFavorite(),
    watchConfigurations(),
    watchSubmissions(),
  ]);
}
