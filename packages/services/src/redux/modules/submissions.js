import { List, Record } from 'immutable';
import { Utils } from '@kineticdata/bundle-common';
import * as constants from '../../constants';
import { StarRateSharp } from '@material-ui/icons';
const { noPayload, withPayload } = Utils;
const ns = Utils.namespaceBuilder('services/submissions');

export const types = {
  FETCH_SUBMISSIONS_REQUEST: ns('FETCH_SUBMISSIONS_REQUEST'),
  FETCH_SUBMISSIONS_NEXT: ns('FETCH_SUBMISSIONS_NEXT'),
  FETCH_SUBMISSIONS_PREVIOUS: ns('FETCH_SUBMISSIONS_PREVIOUS'),
  FETCH_SUBMISSIONS_CURRENT: ns('FETCH_SUBMISSIONS_CURRENT'),
  FETCH_SUBMISSIONS_SUCCESS: ns('FETCH_SUBMISSIONS_SUCCESS'),
  FETCH_SUBMISSIONS_FAILURE: ns('FETCH_SUBMISSIONS_FAILURE'),
};

export const actions = {
  fetchSubmissionsRequest: withPayload(types.FETCH_SUBMISSIONS_REQUEST),
  fetchSubmissionsNext: noPayload(types.FETCH_SUBMISSIONS_NEXT),
  fetchSubmissionsPrevious: noPayload(types.FETCH_SUBMISSIONS_PREVIOUS),
  fetchSubmissionsCurrent: noPayload(types.FETCH_SUBMISSIONS_CURRENT),
  fetchSubmissionsSuccess: withPayload(types.FETCH_SUBMISSIONS_SUCCESS),
  fetchSubmissionsFailure: withPayload(types.FETCH_SUBMISSIONS_FAILURE),

  fetchSubmissions: coreState => ({
    type: types.FETCH_SUBMISSIONS,
    payload: { coreState },
  }),
  fetchNextPage: coreState => ({
    type: types.FETCH_NEXT_PAGE,
    payload: { coreState },
  }),
  fetchPreviousPage: coreState => ({
    type: types.FETCH_PREVIOUS_PAGE,
    payload: { coreState },
  }),
  fetchCurrentPage: coreState => ({
    type: types.FETCH_CURRENT_PAGE,
    payload: { coreState },
  }),
  setSubmissions: (submissions, nextPageToken) => ({
    type: types.SET_SUBMISSIONS,
    payload: { submissions, nextPageToken },
  }),
};

export const State = Record({
  active: {
    error: null,
    data: null,
    coreState: null,
    limit: constants.PAGE_SIZE,
    paging: false,
    pageToken: null,
    nextPageToken: null,
    previousPageTokens: List(),
  },
  past: {
    error: null,
    data: null,
    coreState: null,
    limit: constants.PAGE_SIZE,
    paging: false,
    pageToken: null,
    nextPageToken: null,
    previousPageTokens: List(),
  },
});

const reducer = (state = State(), { type, payload = {} }) => {
  switch (type) {
    case types.FETCH_SUBMISSIONS_REQUEST:
      return state
        .setIn([payload.type, 'data'], null)
        .setIn([payload.type, 'error'], null)
        .setIn([payload.type, 'coreState'], payload.coreState)
        .setIn([payload.type, 'limit'], payload.limit || constants.PAGE_SIZE)
        .setIn([payload.type, 'pageToken'], null)
        .setIn([payload.type, 'nextPageToken'], null)
        .setIn([payload.type, 'previousPageTokens'], List());
    case types.FETCH_SUBMISSIONS_NEXT:
      return state
        .updateIn([payload.type, 'previousPageTokens'], t =>
          t.push(state.pageToken),
        )
        .setIn([payload.type, 'pageToken'], state.nextPageToken)
        .setIn([payload.type, 'nextPageToken'], null)
        .setIn([payload.type, 'paging'], true);
    case types.FETCH_SUBMISSIONS_PREVIOUS:
      return state
        .setIn([payload.type, 'nextPageToken'], null)
        .setIn([payload.type, 'pageToken'], state.previousPageTokens.last())
        .updateIn([payload.type, 'previousPageTokens'], t => t.pop())
        .setIn([payload.type, 'paging'], true);
    case types.FETCH_SUBMISSIONS_CURRENT:
      return state.setIn([payload.type, 'paging'], true);
    case types.FETCH_SUBMISSIONS_SUCCESS:
      return state
        .setIn([payload.type, 'data'], List(payload.submissions))
        .setIn([payload.type, 'nextPageToken'], payload.nextPageToken)
        .setIn([payload.type, 'paging'], false);
    case types.FETCH_SUBMISSIONS_FAILURE:
      return state
        .setIn([payload.type, 'error'], payload)
        .setIn([payload.type, 'paging'], false);
    default:
      return state;
  }
};

export default reducer;
