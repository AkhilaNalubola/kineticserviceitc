import { Record } from 'immutable';
import { Utils } from '@kineticdata/bundle-common';
const { withPayload } = Utils;
const ns = Utils.namespaceBuilder('services/features');

export const types = {
  UPDATE_FEATURES_REQUEST: ns('UPDATE_FEATURES_REQUEST'),
};

export const actions = {
  updateFeaturesRequest: withPayload(types.UPDATE_FEATURES_REQUEST),
};

export const State = Record({
  data: null,
  error: null,
});

export const reducer = (state = State(), { type, payload }) => {
  switch (type) {
    case types.UPDATE_FEATURES_REQUEST:
      return state;
    default:
      return state;
  }
};
