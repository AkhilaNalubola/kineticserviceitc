import { Record } from 'immutable';
import { Utils } from '@kineticdata/bundle-common';
const { withPayload } = Utils;
const ns = Utils.namespaceBuilder('services/favorites');

export const types = {
  FETCH_FAVORITES_REQUEST: ns('FETCH_FAVORITES_REQUEST'),
  FETCH_FAVORITES_SUCCESS: ns('FETCH_FAVORITES_SUCCESS'),
  FETCH_FAVORITES_FAILURE: ns('FETCH_FAVORITES_FAILURE'),
  UPDATE_FAVORITES_REQUEST: ns('UPDATE_FAVORITES_REQUEST'),
};

export const actions = {
  fetchFavoritesRequest: withPayload(types.FETCH_FAVORITES_REQUEST),
  fetchFavoritesSuccess: withPayload(types.FETCH_FAVORITES_SUCCESS),
  fetchFavoritesFailure: withPayload(types.FETCH_FAVORITES_FAILURE),
  updateFavoritesRequest: withPayload(types.UPDATE_FAVORITES_REQUEST),
};

export const State = Record({
  data: null,
  error: null,
});

export const reducer = (state = State(), { type, payload }) => {
  switch (type) {
    case types.FETCH_FAVORITES_REQUEST:
      return state.set('error', null);
    case types.FETCH_FAVORITES_SUCCESS:
      return state.set('error', null).set('data', payload);
    case types.FETCH_FAVORITES_FAILURE:
      return state.set('error', payload).set('data', null);
    case types.UPDATE_FAVORITES_REQUEST:
      return state.set('error',null)
    default:
      return state;
  }
};
