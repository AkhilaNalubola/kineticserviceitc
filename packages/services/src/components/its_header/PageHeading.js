import React from 'react';
import '../../assets/styles/_header.scss';
import { navigate } from '@reach/router';

export const PageHeading = props => {
    const { showSearchFilter } = props
    return (
        <div className="page-heading d-flex justify-content-between" >
            <div className="page-heading-name">
                {props.title}
            </div>
            <div>
                {showSearchFilter && <div className="heading-search">
                    <input type="search" placeholder="Search here" onInput={(e) => props.searchedKey(e.target.value)} />
                </div>}
            </div>
        </div>
    );
};
