import React, { useEffect } from 'react';
// import { withRouter } from 'react-router'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Button, IconButton } from '@material-ui/core';
import { navigate } from '@reach/router';

export const PrevPage = () => {
  return (
    <div className="">
      <div
        className="  text-left ml-2"
        style={{
          color: '#ccc',
          position: 'absolute',
          marginTop: '12px',
          zIndex: '2',
        }}
        title="Go Back"
      >
        <IconButton onClick={() => window.history.back()} title="Go Back">
          <ArrowBackIcon />
        </IconButton>
        {/* <Button onClick={() => props.history.goBack()}>
                Go Back
        </Button> */}
      </div>
    </div>
  );
};
