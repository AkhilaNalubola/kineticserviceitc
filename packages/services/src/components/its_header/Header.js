import React, { useEffect, useState } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Avatar } from '@material-ui/core';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import { Link } from '@reach/router';

import logo from '../../assets/images/Logo_Company_Tag_Reverse_small.png';
import { PrevPage } from './PrevPage';
import '../../assets/styles/_header.scss';
import HomeIcon from '@material-ui/icons/Home';
import { logout, I18n } from '@kineticdata/react';

const notificationData = [
  {
    date: '19/11/2020',
    info: 'Bundle Requested Successfully !',
  },
  {
    date: '19/11/2020',
    info: 'New Trouble issue Resolved !',
  },
  {
    date: '18/11/2020',
    info: 'New Device Requested Successfully !',
  },
  {
    date: '18/11/2020',
    info: 'Profile Update  Successfully !',
  },
  {
    date: '17/11/2020',
    info: 'User created Successfully !',
  },
];

export const Header = props => {
  console.log('header props', props);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorMore, setAnchorMore] = React.useState(null);

  const { background, textColor } = props;
  const headerLinkStyle = {
    color: textColor,
  };
  const handleClickMenu = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClickMore = event => {
    setAnchorMore(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const handleCloseMore = () => {
    setAnchorMore(null);
  };

  return (
    <div className="" id="its-header">
      <nav
        className="navbar navbar-expand-lg navbar-dark"
        style={{
          backgroundColor: background,
          color: textColor,
        }}
      >
        <div className="container">
          {/* <PrevPage/> */}

          <Link className="navbar-brand" to="/kapps/services/">
            <img
              alt="comapny-logo"
              className="img-fluid"
              height="50px"
              src={logo}
            />{' '}
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          {true && (
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item " />
                <li>
                  <Link
                    className="header-nav-link d-flex"
                    style={headerLinkStyle}
                    to="/kapps/services/"
                    title="Home"
                  >
                    {' '}
                    <HomeIcon />
                  </Link>
                </li>
                <li className="nav-item ">
                  <Link
                    className="header-nav-link"
                    style={headerLinkStyle}
                    to="/kapps/services/catalog/all"
                    activeClassName="selected"
                  >
                    Catalog
                  </Link>
                </li>

                <li className="nav-item ">
                  <Link
                    className="header-nav-link"
                    style={headerLinkStyle}
                    to="/kapps/services/myActivity"
                    activeClassName="selected"
                  >
                    {' '}
                    My Activity
                  </Link>
                </li>
                <li className="nav-item  float-right">
                  <Link
                    className="header-nav-link"
                    style={headerLinkStyle}
                    to="/kapps/services/myDevices"
                    activeClassName="selected"
                  >
                    {' '}
                    My Devices
                  </Link>
                </li>

                <li className="nav-item  float-right">
                  <Link
                    className="header-nav-link"
                    style={headerLinkStyle}
                    to="/kapps/services/config"
                    activeClassName="selected"
                  >
                    Admin
                  </Link>

                  {/* <div>
                    <span
                      className="header-nav-link curser-pointer"
                      aria-controls={'simple-menu'}
                      aria-haspopup="true"
                      onClick={handleClickMenu}
                    >
                      Admin
                    </span>
                    <Menu
                      id={'simple-menu'}
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleCloseMenu}
                    >
                      <MenuItem onClick={handleCloseMenu}><Link className="px-4" to="/adminActivity">Activity</Link></MenuItem>
                      <MenuItem onClick={handleCloseMenu}>
                        <Link className="px-4" to="/kapps/services/config">
                          Configurations
                        </Link>
                      </MenuItem>
                    </Menu>
                  </div> */}
                </li>
                <li className="nav-item  float-right">
                  {/* <Link className="header-nav-link" style={headerLinkStyle} to="/adminActivity" activeClassName="selected">Admin Activity </Link>n */}

                  <div>
                    <span
                      className="header-nav-link curser-pointer"
                      aria-controls={'simple-menu1'}
                      aria-haspopup="true"
                      onClick={handleClickMore}
                    >
                      Links
                    </span>
                    <Menu
                      id={'simple-menu1'}
                      anchorEl={anchorMore}
                      keepMounted
                      open={Boolean(anchorMore)}
                      onClose={handleCloseMore}
                    >
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          className="px-4"
                          href="https://advancedtech1.sharepoint.com/sites/HomeHub/"
                        >
                          ATS Intranet
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="http://www.atsadvancedtechnologyservices.corpmerchandise.com/ "
                          className="px-4"
                        >
                          ATS Merchandise Store
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://sts.advancedtech.com/adfs/ls/idpinitiatedsignon.aspx/"
                          className="px-4"
                        >
                          ATS Single SignOn Page
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://www.concursolutions.com/home.asp"
                          className="px-4"
                        >
                          Concur Travel & Expense
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://www.industrysafe.com/ADVANCEDTECH/"
                          className="px-4"
                        >
                          Industry Safe
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://secure.logmeinrescue.com/Customer/Code.aspx"
                          className="px-4"
                        >
                          LogMeIn 123
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://portal.office.com/Home"
                          className="px-4"
                        >
                          Office 365
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://wd5.myworkday.com/advancedtech/d/home.htmld"
                          className="px-4"
                        >
                          Workday
                        </a>
                      </MenuItem>
                      <MenuItem onClick={handleCloseMore}>
                        <a
                          target="_blank"
                          href="https://www.yammer.com/"
                          className="px-4"
                        >
                          Yammer
                        </a>
                      </MenuItem>
                    </Menu>
                  </div>
                </li>
                {/* <li className="nav-item  float-right"><Link className="header-nav-link" style={headerLinkStyle} to="/help" activeClassName="selected"> Help</Link></li> */}
              </ul>
              <ul className="navbar-nav">
                {/* <Link
                  className="header-nav-link d-flex"
                  activeClassName="selected"
                  style={headerLinkStyle}
                  to="/kapps/services/profile"
                > */}
                <div className="notification-dropdown">
                  <p
                    className="header-nav-link d-flex m-0 notification-icon"
                    title="Profile"
                  >
                    <span className="nav-profile-icon">
                      <Avatar />
                    </span>
                  </p>
                  <div className="notifications-list">
                    <Link className=" d-flex" to="/kapps/services/profile">
                      myPROFILE
                    </Link>
                    <div
                      className="text-dark pl-3 cursor-pointer"
                      onClick={logout}
                    >
                      <p>Logout</p>
                    </div>
                  </div>
                </div>
                {/* <span>Profile</span> */}
                {/* </Link> */}
                <Link
                  className="header-nav-link d-flex"
                  style={headerLinkStyle}
                  to="/kapps/services/help"
                  title="Help"
                >
                  {' '}
                  <HelpOutlineIcon />
                </Link>
                <div className="notification-dropdown">
                  <p
                    className="header-nav-link d-flex m-0 notification-icon"
                    title="Noifications"
                  >
                    <NotificationsIcon />
                    <span className="navbar-icon-badge"> 5 </span>
                  </p>
                  <div className="notifications-list">
                    {notificationData.map((data, i) => {
                      return (
                        <a
                          key={i * 10}
                          className="dropdown-item d-flex align-items-center"
                        >
                          <div className="mr-3">
                            <div className="icon-circle bg-primary text-white">
                              {i + 1}
                            </div>
                          </div>
                          <div>
                            <div className="small  text-left text-muted">
                              {data.date}
                            </div>
                            <span className="notifi-info">{data.info}</span>
                          </div>
                        </a>
                      );
                    })}
                    <div className="view-more-nofify text-center text-primary">
                      <Link to="/kapps/services/notificationsList">
                        View more &#8594;
                      </Link>
                    </div>
                  </div>
                </div>

                <div className="notification-dropdown">
                  <p
                    className="header-nav-link d-flex m-0 notification-icon "
                    style={headerLinkStyle}
                    title="Feedback"
                  >
                    <EmojiObjectsIcon />
                  </p>
                  <div className="notifications-list">
                    {
                      <div className="dropdown-item feedback-container d-flex align-items-center">
                        <h6>Post an Idea</h6>
                        <p>When you post an idea to our feedback forum,</p>
                        <textarea className="form-control" />
                        <button className="btn btn-success mt-2">Next</button>
                      </div>
                    }
                  </div>
                </div>
              </ul>
              {/* <Link className="header-nav-link d-flex" to="/chat"> <ForumIcon /></Link> */}
            </div>
          )}
        </div>
      </nav>

      <PrevPage />
    </div>
  );
};
