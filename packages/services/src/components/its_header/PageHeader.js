import React from 'react';
import '../../assets/styles/_header.scss';
import { navigate } from '@reach/router';

export const PageHeader = props => {
  return (
    <div className="page-header mb-3">
      {/* <div className="page-header-icon">{props.icon}</div> */}
      <div className="page-header-name">
        {props.name}
        <span className="page-header-status">{props.status}</span>
      </div>
    </div>
  );
};
