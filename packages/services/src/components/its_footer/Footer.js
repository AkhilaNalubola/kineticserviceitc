import { getSupportInfo } from 'prettier';
import React, { useEffect, useState } from 'react';
import { HomeSupport } from '../its_home/HomeSupport';

export const Footer = props => {
  const { background, textColor, supportInfo, copyRights } = props;
  return (
    <div
      className="bottom-footer text-center"
      style={{ backgroundColor: background, color: textColor }}
    >
      <div style={{ fontSize: '14px' }}>
        {supportInfo.length &&
          supportInfo.map((data, i) => (
            <>
              <label className="text-muted">{data.field} </label>
              <span> {data.value}</span> |
            </>
          ))}
      </div>
      <small>{copyRights} </small>
    </div>
  );
};
