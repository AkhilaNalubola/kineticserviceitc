import React from 'react';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';

export const Help = () => {
  return (
    <div>
      <div className="container">
        <div className="assist-me-search text-center container card p-5 my-3">
          <h5>Assist ME</h5>
          <input
            type="search"
            className="form-control"
            placeholder="Search Here"
          />
        </div>

        <div id="accordion">
          <div className="card">
            <div class="card-header" id="headingFour">
              <h5 class="mb-0">
                <button
                  class="btn btn-link collapsed"
                  data-toggle="collapse"
                  data-target="#collapseFour"
                  aria-expanded="false"
                  aria-controls="collapseFour"
                >
                  Instructions
                </button>
              </h5>
            </div>
            <div
              id="collapseFour"
              class="collapse"
              aria-labelledby="headingFour"
              data-parent="#accordion"
            >
              <div class="card-body">
              <div className="row my-2 ">
                      <div className="col-sm-3">
                        <a
                          href="https://userportal.enterprisewatch.co/kinetic/docs/Common/myE_v4_UserGuide_40_RD02c.pdf"
                          target="_blank"
                        >
                          <div class="card">
                            <div className="p-3 text-center">
                              <PictureAsPdfIcon />
                            </div>
                            <div class="card-body text-center">
                              <p class="card-text">Instructions</p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                </div>
              </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button
                  class="btn btn-link"
                  data-toggle="collapse"
                  data-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                  FAQ
                </button>
              </h5>
            </div>

            <div
              id="collapseOne"
              class="collapse show"
              aria-labelledby="headingOne"
              data-parent="#accordion"
            >
              <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a
                      class="nav-link active"
                      id="home-tab"
                      data-toggle="tab"
                      href="#home"
                      role="tab"
                      aria-controls="home"
                      aria-selected="true"
                    >
                      Software
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link"
                      id="profile-tab"
                      data-toggle="tab"
                      href="#profile"
                      role="tab"
                      aria-controls="profile"
                      aria-selected="false"
                    >
                      Hardware
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link"
                      id="contact-tab"
                      data-toggle="tab"
                      href="#contact"
                      role="tab"
                      aria-controls="contact"
                      aria-selected="false"
                    >
                      Mobile
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link"
                      id="service-tab"
                      data-toggle="tab"
                      href="#service"
                      role="tab"
                      aria-controls="service"
                      aria-selected="false"
                    >
                      Service
                    </a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div
                    class="tab-pane fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <div class="list-group text-left">
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        My Software not working
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        I need to request new software.
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        I need access to an application.
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        I need a change made to an application
                      </a>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade"
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <div class="list-group text-left">
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        My computer is making funny noises.
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        My computer crashes or has other issues
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        I need help with my mouse, keyboard, or other computer
                        device
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        The warranty on my computer is expired.
                      </a>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade"
                    id="contact"
                    role="tabpanel"
                    aria-labelledby="contact-tab"
                  >
                    <div class="list-group text-left">
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        How do I enroll my device?
                      </a>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade"
                    id="service"
                    role="tabpanel"
                    aria-labelledby="service-tab"
                  >
                    <div class="list-group text-left">
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        I need my password reset.
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        What does my domain account work for?
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        What is SAP?
                      </a>
                      <a
                        href="#"
                        class="list-group-item list-group-item-action"
                      >
                        How do I request access to a share drive?
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button
                  class="btn btn-link collapsed"
                  data-toggle="collapse"
                  data-target="#collapseTwo"
                  aria-expanded="false"
                  aria-controls="collapseTwo"
                >
                  Videos
                </button>
              </h5>
            </div>
            <div
              id="collapseTwo"
              class="collapse"
              aria-labelledby="headingTwo"
              data-parent="#accordion"
            >
              <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a
                      class="nav-link active"
                      id="desktop-tab"
                      data-toggle="tab"
                      href="#desktop"
                      role="tab"
                      aria-controls="desktop"
                      aria-selected="true"
                    >
                      Desktop
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link"
                      id="mobile-tab"
                      data-toggle="tab"
                      href="#mobile"
                      role="tab"
                      aria-controls="mobile"
                      aria-selected="false"
                    >
                      Mobile
                    </a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div
                    class="tab-pane  active"
                    id="desktop"
                    role="tabpanel"
                    aria-labelledby="desktop-tab"
                  >
                    <div class="list-group text-left">
                      <div className="row my-2">
                        <div className="col-sm-3">
                          <a
                            href="https://youtu.be/Dre32nHV6yw"
                            target="_blank"
                          >
                            <div class="card">
                              <div className="p-3 text-center">
                                <VideoLibraryIcon />
                              </div>
                              <div class="card-body text-center">
                                <p class="card-text">assistME</p>
                              </div>
                            </div>
                          </a>
                        </div>
                        <div className="col-sm-3">
                          <a
                            href="https://youtu.be/OafFW7Zobu0"
                            target="_blank"
                          >
                            <div class="card">
                              <div className="p-3 text-center">
                                <VideoLibraryIcon />
                              </div>
                              <div class="card-body text-center">
                                <p class="card-text">Getting Started Guide</p>
                              </div>
                            </div>
                          </a>
                        </div>
                        <div className="col-sm-3">
                          <a
                            href="https://youtu.be/kFqiBgzvFFo"
                            target="_blank"
                          >
                            <div class="card">
                              <div className="p-3 text-center">
                                <VideoLibraryIcon />
                              </div>
                              <div class="card-body text-center">
                                <p class="card-text">Creating a Request</p>
                              </div>
                            </div>
                          </a>
                        </div>
                        <div className="col-sm-3">
                          <a
                            href="https://youtu.be/Enbn05p2bOk"
                            target="_blank"
                          >
                            <div class="card">
                              <div className="p-3 text-center">
                                <VideoLibraryIcon />
                              </div>
                              <div class="card-body text-center">
                                <p class="card-text">myProfile Guide</p>
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                      {/* <a href="#" class="list-group-item list-group-item-action">Desktop view</a> */}
                    </div>
                  </div>
                  <div
                    class="tab-pane fade"
                    id="mobile"
                    role="tabpanel"
                    aria-labelledby="mobile-tab"
                  >
                    <div class="list-group text-left">
                      <div className="row my-2 ">
                        <div className="col-sm-3">
                          <a href="http://youtu.be/gG2RQQz4pEM" target="_blank">
                            <div class="card">
                              <div className="p-3 text-center">
                                <VideoLibraryIcon />
                              </div>
                              <div class="card-body text-center">
                                <p class="card-text">
                                  Mobile Request Submission
                                </p>
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button
                  class="btn btn-link collapsed"
                  data-toggle="collapse"
                  data-target="#collapseThree"
                  aria-expanded="false"
                  aria-controls="collapseThree"
                >
                  PDF's
                </button>
              </h5>
            </div>
            <div
              id="collapseThree"
              class="collapse"
              aria-labelledby="headingThree"
              data-parent="#accordion"
            >
              <div class="card-body">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a data-toggle="tab" href="#pdfDesktop">
                      Desktop
                    </a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div id="pdfDesktop" class="tab-pane  in active">
                    <div className="row my-2 ">
                      <div className="col-sm-3">
                        <a
                          href="https://userportal.enterprisewatch.co/kinetic/docs/Common/myE_v4_UserGuide_40_RD02c.pdf"
                          target="_blank"
                        >
                          <div class="card">
                            <div className="p-3 text-center">
                              <PictureAsPdfIcon />
                            </div>
                            <div class="card-body text-center">
                              <p class="card-text">User Guide 4.0</p>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-sm-3">
                        <a
                          href="https://userportal.enterprisewatch.co/kinetic/docs/ATS/myE_v4_Cheat_40_RD01b.pdf"
                          target="_blank"
                        >
                          <div class="card">
                            <div className="p-3 text-center">
                              <PictureAsPdfIcon />
                            </div>
                            <div class="card-body text-center">
                              <p class="card-text">Cheat Sheet</p>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-sm-3">
                        <a
                          href="https://userportal.enterprisewatch.co/kinetic/docs/Common/myE_v4_FAQ_40_RD03.pdf"
                          target="_blank"
                        >
                          <div class="card">
                            <div className="p-3 text-center">
                              <PictureAsPdfIcon />
                            </div>
                            <div class="card-body text-center">
                              <p class="card-text">FAQ's</p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
