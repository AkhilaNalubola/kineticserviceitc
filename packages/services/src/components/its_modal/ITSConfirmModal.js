import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export const ITSConfirmModal = (props) => {

    const { isOpenConfirmModal, onAcceptConfirm, onCancelConfirm, submitModalForm, info, modalFormData } = props
    return (<div>

        <Modal isOpen={isOpenConfirmModal}  >
            <ModalHeader><h4 className="px-4 pt-3 text-left w-100 text-primary">Confirmation !</h4></ModalHeader>
            <ModalBody>
                <div className="p-2 text-center">{info}</div>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={onAcceptConfirm}>Confirm</Button>{' '}
                <Button color="danger" onClick={onCancelConfirm}>Cancel</Button>
            </ModalFooter>
        </Modal>
    </div>)
}