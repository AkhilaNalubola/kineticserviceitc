import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export const ITSModal = (props) => {

    const { isOpen, modalHeading, closeModal, submitModalForm, component, modalFormData } = props
    return (<div>

        <Modal isOpen={isOpen}  >
            <ModalHeader><h4 className="px-4 pt-3 text-left w-100 text-primary">{modalHeading}</h4></ModalHeader>
            <ModalBody>
                {component}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={submitModalForm}>Submit</Button>{' '}
                <Button color="danger" onClick={closeModal}>Cancel</Button>
            </ModalFooter>
        </Modal>
    </div>)
}