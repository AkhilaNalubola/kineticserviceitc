import React, { useState } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import FilterListIcon from '@material-ui/icons/FilterList';
import '../../assets/styles/_my_devices.scss';

const ddata = [
  {
    service: 'Request 1',
    product: 'Mac Book pro',
    id: '1234',
    date: '02/11/2020',
    status: 'pendingRequest',
    statusLabel: 'Pending Request',
  },
  {
    service: 'Request 2',
    product: 'Mac Book pro',
    id: '123445',
    date: '01/11/2020',
    status: 'approvedRequest',
    statusLabel: 'Approved Request',
  },
  {
    service: 'Request 3',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'openAction',
    statusLabel: 'Open Action',
  },
  {
    service: 'Request 4',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'openAction',
    statusLabel: 'Open Action',
  },
  {
    service: 'Request 5',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'approvedRequest',
    statusLabel: 'Approved Request',
  },
  {
    service: 'Request 6',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'approvedRequest',
    statusLabel: 'Approved Request',
  },
  {
    service: 'Request 7',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'approvedRequest',
    statusLabel: 'Approved Request',
  },
  {
    service: 'Request 8',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'pendingRequest',
    statusLabel: 'Pending Request',
  },
  {
    service: 'Request 9',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'pendingRequest',
    statusLabel: 'Pending Request',
  },
  {
    service: 'Request 10',
    product: 'Mac Book pro',
    id: '3456',
    date: '02/10/2020',
    status: 'pendingRequest',
    statusLabel: 'Pending Request',
  },
];

var filteredList = ddata;
export const MyDevices = () => {
  const [globalFilter, setglobalFilter] = useState('');
  const [statusFilter, setStatusFilter] = useState('');
  const getStatusFilter = status => {
    setStatusFilter(status);
    if (status) {
      filteredList = ddata.filter(data => data.status == status);
    } else {
      console.log('filtered reset', ddata);
      filteredList = ddata;
    }
  };

  const header = (
    <div className="table-header">
      {/* <div className="my-activity-header p-3 border-bottom mb-2"> */}
      <h5 className="text-left">Past events</h5>
      {/* </div> */}
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          type="search"
          onInput={e => setglobalFilter(e.target.value)}
          placeholder="Search"
        />
      </span>
    </div>
  );
  return (
    <div className="mydevices-container">
      <div className="container">
        <section className="my-devices-list text-left mb-5 ">
          <h5 className="pt-4">My Devices</h5>
          <select className="form-control">
            <option value="">Hewlett-Packard EliteBook 840 G4</option>
          </select>
        </section>
        <section className="my-devices-current text-left my-5 bg-white p-5">
          {/* <h5 className="mb-4">Device : <span className="device-name">{'Hewlett-Packard EliteBook 840 G4'}</span></h5> */}
          <div>
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  id="pills-home-tab"
                  data-toggle="pill"
                  href="#pills-home"
                  role="tab"
                  aria-controls="pills-home"
                  aria-selected="true"
                >
                  Device Info
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="pills-softinfo-tab"
                  data-toggle="pill"
                  href="#pills-softinfo"
                  role="tab"
                  aria-controls="pills-softinfo"
                  aria-selected="true"
                >
                  Software Info
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="pills-profile-tab"
                  data-toggle="pill"
                  href="#pills-profile"
                  role="tab"
                  aria-controls="pills-profile"
                  aria-selected="false"
                >
                  Requests
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="pills-contact-tab"
                  data-toggle="pill"
                  href="#pills-contact"
                  role="tab"
                  aria-controls="pills-contact"
                  aria-selected="false"
                >
                  History
                </a>
              </li>
            </ul>
            <div className="tab-content" id="pills-tabContent">
              <div
                className="tab-pane fade show active"
                id="pills-home"
                role="tabpanel"
                aria-labelledby="pills-home-tab"
              >
                <div>
                  <ul className="my-device-details">
                    <li>
                      <div className="label-qtn">NAME</div>
                      <div className="qtn-ans">LAC170146</div>
                    </li>
                    <li>
                      <div className="label-qtn">SERIAL</div>
                      <div className="qtn-ans">5CG74033K4</div>
                    </li>
                    <li>
                      <div className="label-qtn">HEALTH</div>
                      <div className="qtn-ans">Unknown</div>
                    </li>
                    <li>
                      <div className="label-qtn">OWNER</div>
                      <div className="qtn-ans">Akhila Nalubola</div>
                    </li>
                    <li>
                      <div className="label-qtn">COMPANY</div>
                      <div className="qtn-ans">ATS</div>
                    </li>
                    <li>
                      <div className="label-qtn">LOCATION</div>
                      <div className="qtn-ans">ITS Palm City FL</div>
                    </li>
                    <li>
                      <div className="label-qtn">WARRANTY</div>
                      <div className="qtn-ans">NA</div>
                    </li>
                    <li>
                      <div className="label-qtn">STATUS</div>
                      <div className="qtn-ans">Deployed</div>
                    </li>
                  </ul>
                </div>
                <div>
                  <label>Request for:</label>
                  <select className="form-control w-50">
                    <option value="">-- Select --</option>
                    <option value="">Exchange a Device</option>
                    <option value="">Report Computer Incident</option>
                    <option value="">Request Software</option>
                    <option value="">Return Device</option>
                    <option value="">Request Computer Accessory</option>
                    <option value="">Change Device Ownership</option>
                    <option value="">Report Device Lost or Stolen</option>
                  </select>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="pills-softinfo"
                role="tabpanel"
                aria-labelledby="pills-softinfo-tab"
              >
                <div>
                  <DataTable
                    value={ddata}
                    paginator
                    rows={5}
                    rowsPerPageOptions={[5, 10, 20]}
                  >
                    <Column field="service" header="Software Vendor" sortable />
                    <Column field="product" header="Software Name" sortable />
                    <Column field="id" header="Software Version" sortable />
                    <Column
                      field="date"
                      header="Software InstallDate"
                      sortable
                    />
                  </DataTable>
                </div>
              </div>

              <div
                className="tab-pane fade"
                id="pills-profile"
                role="tabpanel"
                aria-labelledby="pills-profile-tab"
              >
                <div>
                  <div className="row text-right">
                    <div className="w-100 float-right">
                      <div class="dropdown show float-right mr-5">
                        <p
                          class="dropdown-toggle"
                          id="dropdownMenuLink"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <FilterListIcon />
                        </p>

                        <div
                          class="dropdown-menu"
                          aria-labelledby="dropdownMenuLink"
                        >
                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('openRequest')}
                          >
                            Open Request
                          </a>
                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('approvedRequest')}
                          >
                            Approved Request
                          </a>
                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('pendingRequest')}
                          >
                            Pending Request
                          </a>
                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('closedRequest')}
                          >
                            Closed Request
                          </a>

                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('openAction')}
                          >
                            Open Actions
                          </a>
                          <a
                            class="dropdown-item"
                            onClick={() => getStatusFilter('closedAction')}
                          >
                            Closed Actions
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  {filteredList.length ? (
                    <DataTable
                      value={filteredList}
                      paginator
                      rows={5}
                      rowsPerPageOptions={[5, 10, 20]}
                    >
                      <Column field="service" header="Request" sortable />
                      <Column field="product" header="Type" sortable />
                      <Column field="statusLabel" header="Status" />
                      <Column field="date" header="Created" sortable />
                    </DataTable>
                  ) : (
                    <div class="w-100 text-center">No data available for filtered status</div>
                  )}
                </div>
              </div>

              <div
                className="tab-pane fade"
                id="pills-contact"
                role="tabpanel"
                aria-labelledby="pills-contact-tab"
              >
                <div>
                  <DataTable
                    value={ddata}
                    paginator
                    rows={5}
                    rowsPerPageOptions={[5, 10, 20]}
                  >
                    <Column field="service" header="Owner" sortable />
                    <Column field="id" header="Action" sortable />
                    <Column field="date" header="Date" sortable />
                  </DataTable>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};
