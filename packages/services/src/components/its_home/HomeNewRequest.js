import React, { useEffect } from 'react';
import { connect } from '../../redux/store';
import { Map } from 'immutable';
import { RequestTemplate } from './RequestTemplate';
import WarningIcon from '@material-ui/icons/Warning';
import WifiIcon from '@material-ui/icons/Wifi';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import { addToast, addToastAlert, Utils } from '@kineticdata/bundle-common';

import { actions as favoriteActions } from '../../redux/modules/favorite';
// import { ReactSortable } from "react-sortablejs";
import { sortable } from 'react-sortable';
const formIcon = Map({
  warning: <WarningIcon />,
  wifi: <WifiIcon />,
  questionAnswer: <QuestionAnswerIcon />,
});

const getFormIcon = iconName => formIcon.get(iconName, <WarningIcon />);
class Item extends React.Component {
  render() {
    return <div {...this.props}>{this.props.children}</div>;
  }
}
class SortableList extends React.Component {
  state = {
    items: this.props.items,
  };

  onSortItems = items => {
    this.setState({
      items: items,
    });
  };

  render() {
    const { items } = this.state;

    return (
      <div className="row sortable-list">
        {this.props.items.map((form, i) => {
          return (
            <SortableItem
              key={i}
              onSortItems={this.onSortItems}
              items={this.props.items}
              sortId={i}
              className="col-lg-4 col-md-4 col-sm-4"
            >
              <RequestTemplate
                icon={form.icon}
                name={form.name}
                label={form.name}
                description={form.description}
                uri={form.slug}
                fav={this.props.isFavRequest(form)}
                formData={form}
                onClickFav={this.props.onClickFav}
                isHomePage={false}
              />
            </SortableItem>
          );
        })}
      </div>
    );
  }
}

var SortableItem = sortable(Item);

export const HomeNewRequestComponent = props => {
  var a = [];
  const onClickFav = form => {
    if (props.favoriteForms.length == 9) {
      if (!props.favoriteForms.find(data => data.slug == form.slug)) {
        addToastAlert({
          title: 'Failed to update favorite forms',
          message:
            'Maximum 9 Forms can be added in Favorites.Please Un Favorites other and add new',
        });
      } else {
        props.updateFavoritesForms(form.slug);
      }
    } else {
      props.updateFavoritesForms(form.slug);
      // addToastAlert({
      //   title: 'Success',
      //   message: 'Added',
      //   });
    }
    props.fetchFavoriteForms();
  };
  const isFavRequest = form => {
    // console.log('testefav1',props)
    return (
      props.favoriteForms &&
      props.favoriteForms.find(data => data.slug == form.slug)
    );
  };
  const compare = (a, b) => {
    // Use toUpperCase() to ignore character casing
    const bandA = a.sorting;
    const bandB = b.sorting;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  };

  return (
    <div className=" bg-white p-3">
      <h5 className="text-left">I want to make a request for</h5>
      {
        <div className="">
          <SortableList
            items={a.sort(compare)}
            favForms={props.favoriteForms}
            isFavRequest={isFavRequest}
            onClickFav={onClickFav}
          />

          {props.forms &&
            props.forms.map(form => {
              a.push(form);
              // return (

              //   <div key={form.slug} className="col-lg-4 col-md-4 col-sm-4">
              //     <RequestTemplate
              //       icon={form.icon}
              //       name={form.name}
              //       label={form.name}
              //       description={form.description}
              //       uri={form.slug}
              //       fav={isFavRequest(form)}
              //       formData={form}
              //       onClickFav={onClickFav}
              //     />
              //   </div>
              // );
            })}
          {console.log('testefav1', a.sort(compare))}
        </div>
      }
    </div>
  );
};

const mapDispatchToProps = {
  updateFavoritesForms: favoriteActions.updateFavoritesRequest,
  fetchFavoriteForms: favoriteActions.fetchFavoritesRequest,
};
const mapStateToProps = state => {
  return {
    favoriteForms: state.favorite.data,
  };
};

export const HomeNewRequest = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeNewRequestComponent);
