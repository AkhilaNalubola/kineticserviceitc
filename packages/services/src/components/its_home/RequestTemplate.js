import React from 'react';
import PersonIcon from '@material-ui/icons/Person';
import '../../assets/styles/_requestTemplate.scss';
import LabelIcon from '@material-ui/icons/Label';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';

import { Link } from '@reach/router';

export const RequestTemplate = props => {
  const clickedFav = data => {
    props.onClickFav(data);
  };
  return (
    <div className="req-temlate-container my-2">
      <div className="card">
        <div className="card-body">
          <div
            className={'req-card-badge'}
            onClick={() => clickedFav(props.formData)}
          >
            {props.fav ? (
              <span title="Favorite">
                <StarIcon />
              </span>
            ) : (
              <span title="Unfavorite">
                <StarBorderIcon />
              </span>
            )}
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-3">
              <div className="req-icon">
                {props.icon ? (
                  <i className={'fa fa-' + props.icon}> </i>
                ) : (
                  <PersonIcon />
                )}
              </div>
            </div>
            <div className="col-lg-9 col-md-9 col-sm-9">
              <div className="card-details">
                <span className="req-title-link">
                  <Link
                    to={
                      (props.fav && props.isHomePage
                        ? `requestService/service-request/`
                        : '') + `requestForm/${props.uri}`
                    }
                  >
                    {props.name ? props.name : 'Card title'}
                  </Link>
                </span>
                <span className="req-description">
                  {props.description || 'card desc'}
                </span>

                {/* <span className="req-cost">100$</span> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
