import React from 'react';
import { HomeCarousel } from './HomeCarousel';
import { HomeFavorites } from './HomeFavorites';
import { HomeMorquee } from './HomeMorquee';
import { HomeNewRequest } from './HomeNewRequest';
import { HomeRequestCategories } from './HomeRequestCategories';
import { HomeSearch } from './HomeSearch';
import { HomeSupport } from './HomeSupport';
import {HomeRequestOverview} from './HomeRequestOverview'

export const Home = props => {
  return (
    <>
    <div className="container" style={{marginTop:'0px'}}>
      <HomeSearch />
      {/* <HomeCarousel /> */}
      <HomeRequestCategories/>
      <div className="row">       
         <div className="col-sm-4 col-md-4 col-lg-4"><HomeMorquee/></div> 
         <div className="col-sm-4 col-md-4 col-lg-4"></div>
         <div className="col-sm-4 col-md-4 col-lg-4"><HomeRequestOverview/></div>  
      </div> 
      <HomeFavorites />
      
      {/* <HomeNewRequest /> */}
    </div>
    {/* <HomeSupport/> */}
    </>
  );
};
