import React from 'react';
import { connect } from '../../redux/store';
import { RequestTemplate } from './RequestTemplate';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import ListIcon from '@material-ui/icons/List';

import { actions as favoriteActions } from '../../redux/modules/favorite';

const data = [
  {
    name: 'Request Service',
    // icon: <ErrorOutlineIcon />,
    uri: '/kapps/services/requestService',
  },
  { name: 'Bundle', uri: '/kapps/services/bundle' },
];

export const HomeFavoritesComponent = props => {
  const onClickFav = form => {
    console.log('testefav1', form, props);
    props.updateFavoritesForms(form.slug);
    props.fetchFavoriteForms();
  };
  return (
    <div className="home-fav-container bg-white p-3 my-2">
      <h5 className="text-left" style={{ color: '#002f6c' }}>
        Your Favorites{' '}
      </h5>
      <div className="row">
        {props.favoriteForms && props.favoriteForms.length > 0 ? (
          props.favoriteForms.map(form => (
            <div key={form.slug} className="col-lg-4 col-md-4 col-sm-4">
              {console.log('form fav', form)}
              <RequestTemplate
                icon={form.icon}
                name={form.name}
                uri={form.slug}
                description={form.description}
                fav={true}
                formData={form}
                onClickFav={onClickFav}
                isHomePage={true}
              />
            </div>
          ))
        ) : (
          <div className="text-center w-100">No Favorite Forms selected</div>
        )}
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  updateFavoritesForms: favoriteActions.updateFavoritesRequest,
  fetchFavoriteForms: favoriteActions.fetchFavoritesRequest,
};
const mapStateToProps = state => {
  return {
    favoriteForms: state.favorite.data,
  };
};
export const HomeFavorites = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeFavoritesComponent);
