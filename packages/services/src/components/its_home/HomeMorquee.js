import React from 'react';

export const HomeMorquee=()=>{
    return (<div className="home-morquee-container">
        <div className="card">
            
            <div className="card-body">
            <h5 className="card-title" style={{color:'#002f6c'}}>Current Messages and Outage</h5>
                <div className="card-text">
                <marquee  behavior="scroll" direction="up" scrollamount="1" height="124px">
<ul>
    <li>Application was updated successfully.</li>
    <li>New Trouble issue Resolved !</li>
</ul>
</marquee>
                    </div> 
                    </div>
        </div>
    </div>)
}