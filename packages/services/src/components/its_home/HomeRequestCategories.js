import React from 'react';
import { Link } from '@reach/router';

import { connect } from '../../redux/store';
import Card1 from '../../assets/images/home/myECard1.jpg';
import Card2 from '../../assets/images/home/myECard2.jpg';
import Card3 from '../../assets/images/home/myECard3.jpg';

const staticCategories=[
    {
        name:'Browse Knowledge',
        slug:"incident-request",
        desc:'Find information to troubleshoot issues or learn how to do what you need.',
        src:Card2
    },
    {
        name:'Submit a Request',
        slug:"service-request",
        desc:'Explore the catalog of IT services.',
        src:Card1
    },
    {
        name:'Submit a Ticket',
        slug:'incident-request',
        desc:'Can’t find what you were looking for? Contact the Service Desk for assistance.',
        src:Card3
    },
]

export const RequestCategoriesCards = ({ categories }) => {
    return (<div className="bg-white p-4 mb-2">
        <div className="row">
    
            {staticCategories &&
                staticCategories.map((category, idx) => (
                    <div className="col-sm-6 col-md-6 col-lg-4" key={category.slug+'_'+idx} >
                        {console.log('categories',category)}
                        <Link to={`requestService/${category.slug}`}>
                        <div class="card home-req-cat-card" >
                            <img class="card-img-top" src={category.src} alt="Card image cap" />
                            <div class="card-body" style={{minHeight:'140px'}}>
                            <h5 class="card-title" style={{color:'#002f6c'}}>{category.name}</h5>                                
                                <p class="card-text">{category.desc}</p>
                            </div>
                        </div>
                        </Link>

                    </div>
                ))}
        </div>
    </div>)
}


export const HomeRequestCategories = connect(state => ({
    categories: state.servicesApp.categories,
}))(RequestCategoriesCards);
