import React from 'react';


export const HomeSupport = () => {
    return (<div className="home-support-container p-4 mt-2">
        <div className="row">
            <div className="col-sm-6 text-left">
                <div>
                    <label className="text-muted">Phone Number</label>
                    <div>
                        800.662.4001
                </div>
                </div>
                <div className="mt-2">
                    <label className="text-muted">Support Email</label>
                    <div>
                        support@its.com
                </div>
                </div>
            </div>
            <div className="col-sm-6 text-right">
                <label className="text-muted">Hours of Operation:</label>
                <div>
                    Monday - Friday : 12:00am - 11:59pm
                </div>
                <div>

                    Saturday - Sunday : 12:00am - 11:59pm
                </div>
            </div>
        </div>

    </div>)
}