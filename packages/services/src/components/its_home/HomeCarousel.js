import React from 'react';
import Carousel from 'react-material-ui-carousel';
import { Paper } from '@material-ui/core';
import '../../assets/styles/_home_carousel.scss';
import img1 from '../../assets/images/Logo_Company_Vertical_xxlarge.jpg';
import img2 from '../../assets/images/Logo_Company_Tagline_xxlarge.jpg';
import img3 from '../../assets/images/Logo_Company_Horizontal_xxlarge.png';
import img4 from '../../assets/images/Logo_xxlarge.jpg';

export const HomeCarousel = () => {
  return (
    <div className="home-carousel-container mt-3">
      {/* react-material-ui-carousel */}
      <Carousel
        className={'home-carousel'}
        interval={3000}
        animation="fade"
        navButtonsAlwaysVisible={true}
        startAt={0}
        autoPlay={true}
      >
        <Paper className="p-5">
          <h2>One</h2>
          <img src={img1} className="img-fluid" width="30%" />
          {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */}
        </Paper>
        <Paper className="p-5">
          <h2>Two</h2>
          <img src={img2} className="img-fluid" width="50%" />
          {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */}
        </Paper>
        <Paper className="p-5">
          <h2>Three</h2>
          <img src={img3} className="img-fluid" width="50%" />
          {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */}
        </Paper>
        <Paper className="p-5">
          <h2>Four</h2>
          <img src={img4} className="img-fluid" width="20%" />
          {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */}
        </Paper>
      </Carousel>
    </div>
  );
};
