import React, { useState,useEffect } from 'react';
import { connect } from '../../redux/store';
import { actions as submissionCountActions } from '../../redux/modules/submissionCounts';
import { navigate,Link } from "@reach/router"

export const HomeRequestOverViewComponent=(props)=>{
    useEffect(()=>{
        props.fetchSubmissionCountsRequest();
        console.log('propses',props)
      },[]);
      const  navigateTo=(dd)=>{
        navigate('#/kapps/service/myActivity')
      }
    return (<div className="home-reqoverview-container">
               <div className="card">
            
            <div className="card-body">
            <h5 className="card-title" style={{color:'#002f6c'}}>Request Overview</h5>
           <div  className="card-text" style={{minHeight:'130px'}}>
            <ul className="list-group req-overview-list">
                <li className="list-group-item d-flex justify-content-between align-items-center">
                    <Link to="/kapps/services/myActivity?mode=open" >Open</Link>
                    <span className="badge badge-primary badge-pill">{props.counts.Submitted || 0}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                    <Link to="/kapps/services/myActivity?mode=closed" >Closed</Link>
                    <span className="badge badge-primary badge-pill">{props.counts.Closed || 0}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                    <Link to="/kapps/services/myActivity?mode=pending" >Pending</Link>
                    <span className="badge badge-primary badge-pill">{props.counts.Draft || 0}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                    <Link to="/kapps/services/myActivity?mode=all" >Actions</Link>
                    <span className="badge badge-primary badge-pill">{props.counts.Submitted + props.counts.Closed + props.counts.Draft || 0}</span>
                </li>
            </ul>
           </div>
                
            </div>
            </div>
             
    </div>)
}


const mapDispatchToProps = {
    fetchSubmissionCountsRequest:submissionCountActions.fetchSubmissionCountsRequest,
  };
  const mapStateToProps = state => {
    return {
      counts:state.submissionCounts.data
    };
  };
  
  export const HomeRequestOverview = connect(
    mapStateToProps,
      mapDispatchToProps,
  )(HomeRequestOverViewComponent);