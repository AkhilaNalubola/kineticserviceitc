import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export const ExternalLinkForm = (props) => {
    console.log('testeswar', props)
    const { modalFormData } = props
    return (<div className="p-4">
        <Form>
            <FormGroup>
                <Label>Link Label</Label>
                <Input type="text" name="phoneNumber" value={modalFormData && modalFormData.name} placeholder="Please enter label" />
            </FormGroup>

            <FormGroup>
                <Label>Link</Label>
                <Input type="text" name="mf" value={modalFormData && modalFormData.slug} placeholder="Please enter link" />
            </FormGroup>

        </Form>
    </div>)

}