import React from 'react';

import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export const SupportForm = (props) => {
    const { modalFormData } = props
    return (<div className="p-4">
        <Form>
            <FormGroup>
                <Label>Service desk number</Label>
                <Input type="text" name="phoneNumber" value={modalFormData.service_number} placeholder="Please enter Number" />
            </FormGroup>
            <FormGroup>
                <Label>International Calls</Label>
                <Input type="text" name="phoneNumbe2r" value={modalFormData.international_number} placeholder="Please enter Number" />
            </FormGroup>
            <FormGroup>
                <Label>Service desk email</Label>
                <Input type="email" name="phoneNumber1" value={modalFormData.service_email} placeholder="Please enter Email" />
            </FormGroup>

            <FormGroup>
                <Label>Hours of Operation</Label>
                <Input type="text" name="mf" value={modalFormData.support_hours} placeholder="Please enter timings" />
            </FormGroup>

        </Form>
    </div>)
}