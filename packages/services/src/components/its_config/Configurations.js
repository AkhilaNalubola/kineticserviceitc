import React, { useEffect, useState, useCallback } from 'react';
import { connect } from '../../redux/store';
import { actions } from '../../redux/modules/configurations';
import { getFeatures } from '../../utils';
import '../../assets/styles/_configurations.scss';
import { ITSModal } from '../its_modal/ITSModal';
import { ExternalLinkForm } from './ExternalLinkForm';
import { SupportForm } from './SupportForm';
import CreateIcon from '@material-ui/icons/Create';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { ITSConfirmModal } from '../its_modal/ITSConfirmModal';


const supportData = {
  service_number: "800-662-4001",
  international_number: "+10 800 662 4001",
  service_email: "support@its.com",
  support_hours: "24 x 7 x 365"
}

const externalLinks = [
  {
    slug: "https://advancedtech1.sharepoint.com/sites/HomeHub/",
    name: "ATS Intranet"
  },
  {
    slug: 'http://www.atsadvancedtechnologyservices.corpmerchandise.com/',
    name: 'ATS Merchandise Store'

  },
  {
    slug: 'https://sts.advancedtech.com/adfs/ls/idpinitiatedsignon.aspx/',
    name: 'ATS Single SignOn Page'

  },
  {
    slug: 'https://www.concursolutions.com/home.asp',
    name: 'Concur Travel & Expense'

  },
  {
    slug: 'https://www.industrysafe.com/ADVANCEDTECH/',
    name: 'Industry Safe'

  },
  {
    slug: 'https://secure.logmeinrescue.com/Customer/Code.aspx',
    name: 'LogMeIn 123'

  },
  {
    slug: 'https://portal.office.com/Home',
    name: 'Office 365'

  },
  {
    slug: 'https://wd5.myworkday.com/advancedtech/d/home.htmld',
    name: 'Workday'
  },
  {
    slug: 'https://www.yammer.com/',
    name: 'Yammer'
  }
]

export const ConfigurationsComponent = ({ features, updateFeatures }) => {
  const handleFeatureCheckbox = useCallback(
    e =>
      updateFeatures({
        featureLabel: e.target.value,
        isChecked: e.target.checked,
        inputType: 'checkbox',
      }),
    [updateFeatures],
  );
  const handleFeatureRadio = useCallback(
    e =>
      updateFeatures({
        featureLabel: e.target.value,
        isLandingPage: e.target.checked,
        inputType: 'radio',
      }),
    [updateFeatures],
  );
  const [mainpageType, setMainpageType] = useState('color');
  const [brandImage, setBrandImage] = useState([
    { src: '' },
    { src: '' },
    { src: '' },
    { src: '' },
  ]);
  const [pageImage, setPageImage] = useState('');
  const [hcolor, setHColor] = useState('');
  const [chatbotColor, setChatbotColor] = useState('');
  const [cardPColor, setCardPColor] = useState('');
  const [fColor, setFColor] = useState('');
  const [modalAction, setmodalAction] = useState('');
  const [isModalShow, setisModalShow] = useState(false);
  const [modalFormData, setisModalFormData] = useState('');
  
  const [isOpenConfirmModal,setIsOpenConfirmModal] = useState(false);
  const [confirmModalHeader,setConfirmModalHeader] = useState('');
  const [confirmModalInfo,setConfirmModalInfo] = useState({});
  // TODO: may need to be a use callback
  const getUploadedImage = (e, i) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      let brandImageClone = brandImage;
      reader.onload = () => {
        brandImageClone[i].src = reader.result;
        setBrandImage(brandImageClone);
      };
      console.log('brandImage', brandImage, i);
    }
  };
  const getUploadedPageImage = e => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => setPageImage(reader.result);
    }
  };
  const setHeaderColor = e => {
    sessionStorage.headerColor = e.target.value;
    setHColor(sessionStorage.headerColor);
  };
  const cardPColorFun = e => {
    sessionStorage.cardColor = e.target.value;
    setCardPColor(sessionStorage.cardColor);
  };
  const chatbotColorFun = e => {
    sessionStorage.chatbotColor = e.target.value;
    setChatbotColor(sessionStorage.chatbotColor);
  };
  const footerColorFun = e => {
    sessionStorage.footerColor = e.target.value;
    setFColor(sessionStorage.footerColor);
  };
  const showModal = (isShow, action, data) => {
    setmodalAction(action);
    setisModalShow(isShow);
    setisModalFormData(data);
    if (isShow) {
      if (action == 'support') {

      }
    }
  }
  const closeModal = () => {
    setisModalShow(false)
  }
  const submitModalForm = () => {
    setisModalShow(false)
  }

  const deleteExternalLink=(data)=>{
    setIsOpenConfirmModal(true);
    setConfirmModalHeader('Are sure ,you want to delete this link?');
    setConfirmModalInfo(data)
  }
  const onAcceptConfirm=()=>{
    setIsOpenConfirmModal(false)
  }
  const onCancelConfirm=()=>{
    setIsOpenConfirmModal(false)
  }
  useEffect(() => {
    setHColor(sessionStorage.headerColor);
    setChatbotColor(sessionStorage.chatbotColor);
    setCardPColor(sessionStorage.cardColor);
    setFColor(sessionStorage.footerColor);
  }, []);
  return (
    <div className="bg-white d-flex p-5 admin-config-container container">
      {isModalShow &&
        <ITSModal
          modalHeading={modalAction == 'support' ? 'Update Support Data' : 'External Link Info'}
          isOpen={isModalShow}
          component={modalAction == 'support' ? <SupportForm modalFormData={supportData} /> : <ExternalLinkForm modalFormData={modalFormData} />}
          closeModal={closeModal}
          submitModalForm={submitModalForm}
          modalFormData={modalFormData}
        />}
    {isOpenConfirmModal &&
      <ITSConfirmModal
        isOpenConfirmModal={isOpenConfirmModal}
        info={confirmModalHeader}
        onAcceptConfirm={onAcceptConfirm}
        onCancelConfirm={onCancelConfirm}
      />
    }


      <div
        className="nav flex-column nav-pills"
        id="v-pills-tab"
        role="tablist"
        aria-orientation="vertical"
      >
        <a
          className="nav-link active"
          id="v-pills-home-tab"
          data-toggle="pill"
          href="#v-pills-home"
          role="tab"
          aria-controls="v-pills-home"
          aria-selected="true"
        >
          Application Features
        </a>
        <a
          className="nav-link"
          id="v-pills-profile-tab"
          data-toggle="pill"
          href="#v-pills-profile"
          role="tab"
          aria-controls="v-pills-profile"
          aria-selected="false"
        >
          Page Builder
        </a>
        {/* <a
          className="nav-link"
          id="v-pills-messages-tab"
          data-toggle="pill"
          href="#v-pills-messages"
          role="tab"
          aria-controls="v-pills-messages"
          aria-selected="false"
        >
          Carousel
        </a> */}
        <a
          className="nav-link"
          id="v-pills-externallinks-tab"
          data-toggle="pill"
          href="#v-pills-externallinks"
          role="tab"
          aria-controls="v-pills-externallinks"
          aria-selected="false"
        >
          External Links
        </a>
        <a
          className="nav-link"
          id="v-pills-settings-tab"
          data-toggle="pill"
          href="#v-pills-settings"
          role="tab"
          aria-controls="v-pills-settings"
          aria-selected="false"
        >
          Support
        </a>
      </div>
      <div className="tab-content w-100" id="v-pills-tabContent">
        <div
          className="tab-pane fade show active"
          id="v-pills-home"
          role="tabpanel"
          aria-labelledby="v-pills-home-tab"
        >
          <table className="table">
            <thead>
              <tr>
                <th scope="col" />
                <th className="text-left" scope="col">
                  Features
                </th>
                <th className="text-left" scope="col">
                  Set as a landing page
                </th>
              </tr>
            </thead>
            <tbody>
              {features &&
                features.map((data, i) => {
                  return (
                    <tr key={i * 10}>
                      <th scope="row">
                        <input
                          onChange={handleFeatureCheckbox}
                          value={data.featureLabel}
                          type="checkbox"
                          checked={data.isChecked}
                        />
                      </th>
                      <td className="text-left">{data.featureLabel}</td>
                      <td className="text-left">
                        {data.enableLandingPage && (
                          <React.Fragment>
                            <input
                              name="landingpage"
                              onClick={handleFeatureRadio}
                              type="radio"
                              value={data.featureLabel}
                              checked={data.isLandingPage}
                            />{' '}
                            {'Landing Page'}
                          </React.Fragment>
                        )}
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <div
          className="tab-pane fade ml-4"
          id="v-pills-profile"
          role="tabpanel"
          aria-labelledby="v-pills-profile-tab"
        >
          {/* Page Builder */}
          <div className="page-config-container">
            <div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Header color</div>
                </div>
                <div className="col-sm-6">
                  {' '}
                  <div>
                    <input
                      type="color"
                      value={hcolor}
                      className="d-block m-auto"
                      onInput={e => setHeaderColor(e)}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Footer color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={fColor}
                      onInput={e => footerColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Card Primary color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={cardPColor}
                      onInput={e => cardPColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Chat bot widget color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={chatbotColor}
                      onInput={e => chatbotColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Headings Color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={chatbotColor}
                      onInput={e => chatbotColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Request Page Heading Color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={chatbotColor}
                      onInput={e => chatbotColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div>Card Heading Color</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <input
                      type="color"
                      value={chatbotColor}
                      onInput={e => chatbotColorFun(e)}
                      className="d-block m-auto"
                    />
                  </div>
                </div>
              </div>
            
              <div className="row">
                <div className="col-sm-6">
                  <div>Login Page</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <div className="d-flex">
                      <div className="form-check mr-2">
                        <input
                          className="form-check-input"
                          value="color"
                          type="radio"
                          onClick={() => setMainpageType('color')}
                          name="exampleRadios"
                          id="exampleRadios1"
                          value="option1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          for="exampleRadios1"
                        >
                          Color
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          value="image"
                          type="radio"
                          onClick={() => setMainpageType('image')}
                          name="exampleRadios"
                          id="exampleRadios2"
                          value="option2"
                        />
                        <label
                          className="form-check-label"
                          for="exampleRadios2"
                        >
                          Image
                        </label>
                      </div>
                    </div>
                    {mainpageType == 'color' && (
                      <div>
                        <input type="color" className="d-block m-auto" />
                      </div>
                    )}
                    {mainpageType == 'image' && (
                      <div>
                        <div className="button-wrap mt-3">
                          <label className="upload-button" for="upload">
                            <div>
                              <i className="fa fa-upload" />
                            </div>
                            Upload Image
                          </label>
                          <input
                            id="upload"
                            type="file"
                            accept=".png"
                            onChange={e => getUploadedPageImage(e)}
                          />
                        </div>
                        <img src={pageImage} width="100px" />
                      </div>
                    )}
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-6">
                  <div>Main Page</div>
                </div>
                <div className="col-sm-6">
                  <div>
                    <div className="d-flex">
                      <div className="form-check mr-2">
                        <input
                          className="form-check-input"
                          value="color"
                          type="radio"
                          onClick={() => setMainpageType('color')}
                          name="exampleRadios"
                          id="exampleRadios1"
                          value="option1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          for="exampleRadios1"
                        >
                          Color
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          value="image"
                          type="radio"
                          onClick={() => setMainpageType('image')}
                          name="exampleRadios"
                          id="exampleRadios2"
                          value="option2"
                        />
                        <label
                          className="form-check-label"
                          for="exampleRadios2"
                        >
                          Image
                        </label>
                      </div>
                    </div>
                    {mainpageType == 'color' && (
                      <div>
                        <input type="color" className="d-block m-auto" />
                      </div>
                    )}
                    {mainpageType == 'image' && (
                      <div>
                        <div className="button-wrap mt-3">
                          <label className="upload-button" for="upload">
                            <div>
                              <i className="fa fa-upload" />
                            </div>
                            Upload Image
                          </label>
                          <input
                            id="upload"
                            type="file"
                            accept=".png"
                            onChange={e => getUploadedPageImage(e)}
                          />
                        </div>
                        <img src={pageImage} width="100px" />
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <label className="text-left w-100">Header Logo</label>
                  <input
                    className="form-control"
                    placeholder="Please enter logo dimension(w x h)"
                  />
                  <small>Please upload 148 x 50 size images</small>
                </div>
                <div className="col-sm-6 d-flex">
                  <div className="button-wrap mt-3">
                    <label className="upload-button" for="upload">
                      <div>
                        <i className="fa fa-upload" />
                      </div>
                  Upload Image
                </label>
                    <input
                      id="upload"
                      type="file"
                      accept=".png"
                      onChange={e => getUploadedImage(e, '0')}
                    />
                  </div>
                  <img src={brandImage[0].src} width="100px" className="ml-2" />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <label className="text-left w-100">Login page Logo</label>
                  <input
                    className="form-control"
                    placeholder="Please enter logo dimension(w x h)"
                  />
                  <small>Please upload 148 x 50 size images</small>
                </div>
                <div className="col-sm-6 d-flex">
                  <div className="button-wrap mt-3">
                    <label className="upload-button" for="upload">
                      <div>
                        <i className="fa fa-upload" />
                      </div>
                  Upload Image
                </label>
                    <input
                      id="upload"
                      type="file"
                      accept=".png"
                      onChange={e => getUploadedImage(e, '0')}
                    />
                  </div>
                  <img src={brandImage[0].src} width="100px" className="ml-2" />
                </div>
              </div>
              
              <div className="mt-2">
                <button
                  className="btn btn-success text-right "
                  onClick={() => window.location.reload()}
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="tab-pane fade ml-4 brand-config-container"
          id="v-pills-messages"
          role="tabpanel"
          aria-labelledby="v-pills-messages-tab"
        >
          {/* Branding */}
          <div className="row">
            <div className="col-sm-6">
              <label className="text-left w-100">Carousel Image 1</label>
              <input
                className="form-control"
                placeholder="Please enter description"
              />
            </div>
            <div className="col-sm-6 d-flex">
              <div className="button-wrap mt-3">
                <label className="upload-button" for="upload">
                  <div>
                    <i className="fa fa-upload" />
                  </div>
                  Upload Image
                </label>
                <input
                  id="upload"
                  type="file"
                  accept=".png"
                  onChange={e => getUploadedImage(e, '0')}
                />
              </div>
              <img src={brandImage[0].src} width="100px" className="ml-2" />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <label className="text-left w-100">Carousel Image 2</label>
              <input
                className="form-control"
                placeholder="Please enter description"
              />
            </div>
            <div className="col-sm-6 d-flex">
              <div className="button-wrap mt-3">
                <label className="upload-button" for="upload">
                  <div>
                    <i className="fa fa-upload" />
                  </div>
                  Upload Image
                </label>
                <input
                  id="upload"
                  type="file"
                  accept=".png"
                  onChange={e => getUploadedImage(e, '1')}
                />
              </div>
              <img src={brandImage[1].src} width="100px" className="ml-2" />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <label className="text-left w-100">Carousel Image 3</label>
              <input
                className="form-control"
                placeholder="Please enter description"
              />
            </div>
            <div className="col-sm-6 d-flex">
              <div className="button-wrap mt-3">
                <label className="upload-button" for="upload">
                  <div>
                    <i className="fa fa-upload" />
                  </div>
                  Upload Image
                </label>
                <input
                  id="upload"
                  type="file"
                  accept=".png"
                  onChange={e => getUploadedImage(e, '2')}
                />
              </div>
              <img src={brandImage[2].src} width="100px" className="ml-2" />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <label className="text-left w-100">Carousel Image 4</label>
              <input
                className="form-control"
                placeholder="Please enter description"
              />
            </div>
            <div className="col-sm-6 d-flex">
              <div className="button-wrap mt-3">
                <label className="upload-button" for="upload">
                  <div>
                    <i className="fa fa-upload" />
                  </div>
                  Upload Image
                </label>
                <input
                  id="upload"
                  type="file"
                  accept=".png"
                  onChange={e => getUploadedImage(e, '3')}
                />
              </div>
              <img src={brandImage[3].src} width="100px" className="ml-2" />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6 text-left align-self-center">
              Carousel width(px)
            </div>
            <div className="col-sm-6">
              <input
                type="number"
                min="550"
                className="form-control"
                placeholder="Please enter width"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6 text-left align-self-center">
              Carousel Height(px)
            </div>
            <div className="col-sm-6">
              <input
                type="number"
                min="170"
                className="form-control"
                placeholder="Please enter height"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6 text-left align-self-center">
              Carousel Timer(sec)
              <div class="text-muted"><small>Default 3 seconds are applied for Carousel timer</small></div>
            </div>
            <div className="col-sm-6">
              <input
                type="number"
                min="3"
                className="form-control"
                placeholder="Please enter time"
                value={'3'}
              />
            </div>
          </div>

        </div>
        <div
          className="tab-pane fade"
          id="v-pills-externallinks"
          role="tabpanel"
          aria-labelledby="v-pills-externallinks-tab"
        >
          <div className="text-right w-100 mb-2">
            <button className="btn btn-info" onClick={() => showModal(true, 'external_Links')}>Add New Link</button>
          </div>
          <div className="ml-5 external-links">
            <ul className="list-group">
              {
                externalLinks && externalLinks.map((data, i) =>
                  <li className="list-group-item d-flex justify-content-between">
                    <div>{data.name}</div>
                    <div>
                      <span title="Edit"><CreateIcon onClick={() => showModal(true, 'external_Links', data)} /></span>
                      <span title="Delete"><HighlightOffIcon onClick={() => deleteExternalLink(data)} /></span>
                    </div>
                  </li>)
              }
            </ul>
          </div>
        </div>

        <div
          className="tab-pane fade"
          id="v-pills-settings"
          role="tabpanel"
          aria-labelledby="v-pills-settings-tab"
        >
          <div className="w-100 text-right">
            <button className="btn btn-info" onClick={() => showModal(true, 'support')}>Edit</button>
          </div>
          <div className="row text-left">
            <div className="col-sm-6 ml-3">

              <div className="border-bottom pb-2 mb-2">
                <label>Service Desk Number</label>
                <div className="fw-500">{supportData.service_number}</div>
              </div>
              <div className="border-bottom pb-2 mb-2">
                <label>International Callers</label>
                <div className="fw-500">{supportData.international_number}</div>
              </div>
              <div className="border-bottom pb-2 mb-2">
                <label>Service Desk Email</label>
                <div className="fw-500">{supportData.service_email}</div>
              </div>
              <div className="border-bottom pb-2 mb-2">
                <label>Hours of support</label>
                <div className="fw-500">{supportData.support_hours}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const mapStateToProps = state => {
  const features =
    state.app.kapp &&
    // Below check could be removed, but it's safer to have it included.
    state.app.kapp.attributesMap &&
    state.app.kapp.attributesMap['Feature'] &&
    getFeatures(state.app.kapp.attributesMap['Feature']);

  return {
    features,
  };
};

export const Configurations = connect(
  mapStateToProps,
  {
    updateFeatures: actions.updateFeaturesRequest,
  },
)(ConfigurationsComponent);
