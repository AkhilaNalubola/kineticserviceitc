import React from 'react';
import { InputText } from 'primereact/inputtext';
export const Article = () => {
    return (<div className="container">
        <div className="bg-white p-5 mt-4">
            <div className="catalog-search m-2 pt-3">
                <span className="p-input-icon-left w-100 ">
                    <i className="pi pi-search" />
                    <InputText
                        type="search"
                        className="w-100"
                        placeholder="Search Here"
                    />
                </span>
            </div>
            <div className="p-4 text-center">Please search for articles</div>
        </div>
    </div>)
}