import React, { useState,useEffect } from 'react';
import { connect } from '../../redux/store';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import '../../assets/styles/_profile.scss';
import { Link } from '@reach/router';
import AccountImg from '../../assets/images/profile/account.png';
import UserImg from '../../assets/images/profile/user.png';
import Bman from '../../assets/images/profile/bman.png';
import Businessman from '../../assets/images/profile/businessman.png';
import Hair from '../../assets/images/profile/hair.png';
import Talking from '../../assets/images/profile/talking.png';
import Profile1 from '../../assets/images/profile/user_profile_1.png';
import Profile2 from '../../assets/images/profile/user_profile_2.png';
import Profile3 from '../../assets/images/profile/user_profile_3.png';
import Profile4 from '../../assets/images/profile/user_profile_4.png';
import Profile5 from '../../assets/images/profile/user_profile_5.png';
import Profile6 from '../../assets/images/profile/user_profile_6.png';


import { actions as submissionCountActions } from '../../redux/modules/submissionCounts';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(9),
    height: theme.spacing(9),
    textAlign: 'center',
  },
}));

let data = [
  {
    label: 'Non-Inventory Item',
    value: '',
  },
  {
    label: 'Request Badge Access',
    value: '',
  },
  {
    label: 'Request Call Forwarding',
    value: '',
  },
];
const profileImages = [
  {
    name: AccountImg,
    id: 'account',
  },
  {
    name: UserImg,
    id: 'user',
  },
  {
    name: Profile2,
    id: 'profile2',
  },
  {
    name: Profile3,
    id: 'profile3',
  },
  {
    name: Profile1,
    id: 'profile1',
  },
  {
    name: Talking,
    id: 'talking',
  },
  {
    name: Profile4,
    id: 'profile4',
  },
  {
    name: Profile5,
    id: 'profile5',
  },
  {
    name: Profile6,
    id: 'profile6',
  },
];

export const ProfileComponent = (props) => {
  const classes = useStyles();
  const [action, setAction] = useState('');
  const [profileImg, setProfileImg] = useState(AccountImg);
  useEffect(()=>{
    props.fetchSubmissionCountsRequest()
  },[])

  const sendProfileImg = data => {
    setProfileImg(data.name);
  };
  console.log('testep',props)
  return (
    <div>
      <div className="container profile-page-container bg-white pb-3">
        <div className="row my-3 bg-white ">
          <div className="col-sm-6">
            <section className="profile-contact-info text-left">
              <h5>Contact Info</h5>
              <div>
                <ul>
                  <li className="row">
                    <span className="profile-contact-lable col-sm-2">Id</span>
                    <span className="profile-contact-ans">1234</span>
                  </li>
                  <li className="row">
                    <span className="profile-contact-lable col-sm-2">
                      Email
                    </span>
                    <span className="profile-contact-ans">
                      analubol@innovativetech.com
                    </span>
                  </li>
                  <li className="row">
                    <span className="profile-contact-lable col-sm-2">Team</span>
                    <span className="profile-contact-ans">1234</span>
                  </li>
                  <li className="row">
                    <span className="profile-contact-lable col-sm-2">
                      Address
                    </span>
                    <span className="profile-contact-ans">1234</span>
                  </li>
                  <li className="row">
                    <span className="profile-contact-lable col-sm-2">
                      Phone
                    </span>
                    <span className="profile-contact-ans">1234</span>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div className="col-sm-6">
            <div className="profile-page-card">
              <div className="dropdown text-center">
                <small
                  className=" dropdown-toggle"
                  id="dropdownMenuButton"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Edit profile icon
                </small>
                <div
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton"
                >
                  <div className="profile-images-list">
                    {profileImages.map(data => {
                      return (
                        <div
                          className="dropdown-item curser-pointer"
                          onClick={() => sendProfileImg(data)}
                        >
                          <img
                            className="img-fluid"
                            src={data.name}
                            width="20px"
                          />
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="profile-avatar">
                <Avatar
                  alt="Remy Sharp"
                  src={profileImg}
                  className={classes.large}
                />
              </div>
              <div className="profile-name text-center">Akhila Nalubola</div>
              <div className="profile-role text-center">Consultant - X001</div>
            </div>
          </div>
        </div>
        <div>
          <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
            {/* <li className="nav-item">
              <a
                className="nav-link active"
                id="pills-home-tab"
                data-toggle="pill"
                href="#pills-home"
                role="tab"
                aria-controls="pills-home"
                aria-selected="true"
              >
                Actions
              </a>
            </li> */}
            <li className="nav-item">
              <a
                className="nav-link active"
                id="pills-profile-tab"
                data-toggle="pill"
                href="#pills-profile"
                role="tab"
                aria-controls="pills-profile"
                aria-selected="false"
              >
                Settings
              </a>
            </li>
            <li className="nav-item">
              {/* <a className="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Support</a> */}
            </li>
          </ul>
          <div className="tab-content" id="pills-tabContent">
            <div
              className="tab-pane fade "
              id="pills-home"
              role="tabpanel"
              aria-labelledby="pills-home-tab"
            >
              <section className="profile-request-overview text-left my-3">
                <div className="profile-custom-shead">
                  {' '}
                  <h5 className="py-2">Request overview</h5>
                  <Link
                    className="custom-link"
                    to={'/kapps/services/catalog/newRequest'}
                  >
                    New request
                  </Link>
                  <Link
                    className="custom-link"
                    to={'/kapps/services/catalog/newTrouble'}
                  >
                    Report trouble ticket
                  </Link>
                </div>

             
              </section>
            </div>
            <div
              className="tab-pane fade show active"
              id="pills-profile"
              role="tabpanel"
              aria-labelledby="pills-profile-tab"
            >
              <div>
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      id="home-tab"
                      data-toggle="tab"
                      href="#home"
                      role="tab"
                      aria-controls="home"
                      aria-selected="true"
                    >
                      Home Page
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      id="profile-tab"
                      data-toggle="tab"
                      href="#profile"
                      role="tab"
                      aria-controls="profile"
                      aria-selected="false"
                    >
                      Navigation Aids
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      id="contact-tab"
                      data-toggle="tab"
                      href="#contact"
                      role="tab"
                      aria-controls="contact"
                      aria-selected="false"
                    >
                      Alternate Approvers
                    </a>
                  </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <div className="text-left">
                      <div className="form-group">
                        <label>Home Page</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="profile">Profile</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <div className="text-left">
                      {/* <div className="form-group">
                        <label>Masthead Tooltips:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="show-tooltip">Show Tooltips</option>
                          <option value="hide-tooltip">Hide Tooltips</option>
                        </select>
                      </div> */}
                      {/* <div className="form-group">
                        <label>Left Menu State:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="closed">Closed</option>
                          <option value="opend">Opened</option>
                        </select>
                      </div> */}
                      <div className="form-group">
                        <label>Default Rows:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="10">Show 10</option>
                          <option value="25">Show 25</option>
                          <option value="50">Show 50</option>
                          <option value="all">Show All</option>
                        </select>
                      </div>
                      {/* <div className="form-group">
                        <label>Temperature Units:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="fahrenheit">Fahrenheit</option>
                          <option value="cesius">Celsius</option>
                        </select>
                      </div> */}
                      <div className="form-group">
                        <label>Date Format:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="mmddyyyy">MM/DD/YYYY</option>
                          <option value="ddmmyyyy">DD/MM/YYYY</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="contact"
                    role="tabpanel"
                    aria-labelledby="contact-tab"
                  >
                    <div className="text-left">
                      <div className="form-group">
                        <label>Alternate Approver:</label>
                        <select className="form-control">
                          <option value="">-- Select --</option>
                          <option value="u1">User 1</option>
                          <option value="u2">User 2</option>
                          <option value="u3">User 3</option>
                          <option value="u4">User 4</option>
                        </select>
                      </div>
                      <div className="form-group">
                        <label>Start Date:</label>
                        <input className="form-control" type="date" />
                      </div>
                      <div className="form-group">
                        <label>End Date:</label>
                        <input className="form-control" type="date" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="pills-contact"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <div className="row text-left">
                <div className="col-sm-6">
                  <div className="border-bottom pb-2 mb-2">
                    <label>Phone Number</label>
                    <div className="fw-500">800.662.4001</div>
                  </div>
                  <div>
                    <label>Hours of Operation:</label>
                    <div>
                      Monday - Friday :{' '}
                      <span className="fw-500">12:00am - 11:59pm</span>
                    </div>
                    <div>
                      Saturday - Sunday :{' '}
                      <span className="fw-500">12:00am - 11:59pm</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  fetchSubmissionCountsRequest:submissionCountActions.fetchSubmissionCountsRequest,
};
const mapStateToProps = state => {
  return {
    counts:state.submissionCounts.data
  };
};

export const Profile = connect(
  mapStateToProps,
    mapDispatchToProps,
)(ProfileComponent);
