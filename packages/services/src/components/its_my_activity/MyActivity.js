import React, { useState, useEffect } from 'react';
import { connect } from '../../redux/store';
import { actions } from '../../redux/modules/submissions';
import moment from 'moment';
import ItemsCarousel from 'react-items-carousel';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import ReorderIcon from '@material-ui/icons/Reorder';
import FeaturedPlayListIcon from '@material-ui/icons/FeaturedPlayList';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import CloseIcon from '@material-ui/icons/Close';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import FilterListIcon from '@material-ui/icons/FilterList';

import '../../assets/styles/_my_activity.scss';
import { ActiveRequests } from './ActiveRequests';

const pastData = [
  {
    service: 'Request 1',
    product: 'Mac Book pro',
    status: 'Approved',
    id: '1234',
    date: '02/11/2020',
  },
  {
    service: 'Request 2',
    product: 'Mac Book pro',
    status: 'Approved',
    id: '123445',
    date: '01/11/2020',
  },
  {
    service: 'Request 3',
    product: 'Mac Book pro',
    status: 'Closed Action',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 4',
    product: 'Mac Book pro',
    status: 'Closed Action',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 5',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 6',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 7',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 8',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 9',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 10',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
];
const ddata = [
  {
    service: 'Request 1',
    product: 'Mac Book pro',
    status: 'Pending Request',
    id: '1234',
    date: '02/11/2020',
  },
  {
    service: 'Request 2',
    product: 'Mac Book pro',
    status: 'Open Request',
    id: '123445',
    date: '01/11/2020',
  },
  {
    service: 'Request 3',
    product: 'Mac Book pro',
    status: 'Open Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 4',
    product: 'Mac Book pro',
    status: 'Open Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 5',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 6',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 7',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 8',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 9',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
  {
    service: 'Request 10',
    product: 'Mac Book pro',
    status: 'Closed Request',
    id: '3456',
    date: '02/10/2020',
  },
];

const listData = [
  {
    name: 'Request 1',
    status: 'pendingRequest',
    role: 'custom',
    statusLabel: 'Pending Request',
  },
  {
    name: 'Request 2',
    status: 'approvedRequest',
    role: 'custom',
    statusLabel: 'Approved Request',
  },
  {
    name: 'Request 3',
    status: 'pendingRequest',
    role: 'custom',
    statusLabel: 'Pending Request',
  },
  {
    name: 'Request 4',
    status: 'approvedRequest',
    role: 'custom',
    statusLabel: 'Approved Request',
  },
  {
    name: 'Request 1',
    status: 'openAction',
    role: 'admin',
    statusLabel: 'Open Action',
  },
  {
    name: 'Request2',
    status: 'openAction',
    role: 'admin',
    statusLabel: 'Open Action',
  },
  {
    name: 'Request 3',
    status: 'openAction',
    role: 'admin',
    statusLabel: 'Open Action',
  },
  {
    name: 'Request 4',
    status: 'openAction',
    role: 'admin',
    statusLabel: 'Open Action',
  },
];
var filteredList = listData;
export const MyActivityComponent = ({
  fetchSubmissions,
  activeEvents,
  pastEvents,
}) => {
  useEffect(() => {
    fetchSubmissions({
      status: ['Open Request', 'Pending Request', 'Open Actions'],
      limit: 20,
      type: 'active',
    });
  }, []);

  useEffect(() => {
    fetchSubmissions({
      status: ['Approved Request', 'Closed Request', 'Closed Actions'],
      limit: 20,
      type: 'past',
    });
  }, []);
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const [eventViewStyle, seteventViewStyle] = useState('box');
  const [globalFilter, setglobalFilter] = useState('');
  const [statusFilter, setStatusFilter] = useState('');
  // console.log('statusFilter', statusFilter, filteredList);
  const activelistEventActionBody = rowData => {
    return (
      <div>
        <a>
          <CloseIcon />
        </a>
        <a>
          <ArrowForwardIcon />
        </a>
      </div>
    );
  };
  const getStatusFilter = status => {
    setStatusFilter(status);
    if (status) {
      filteredList = listData.filter(data => data.status == status);
    } else {
      // console.log('filtered reset', listData);
      filteredList = listData;
    }
    // if(status == 'pendingRequest'){
    //     filteredList=   listData.filter(data=>data.status == "In Progress")
    // }else if(status == 'approvedRequest'){
    //     filteredList=   listData.filter(data=>data.status == "Approved")
    // }else{
    //     filteredList = listData
    // }
  };

  const header = (
    <div className="table-header">
      {/* <div className="my-activity-header p-3 border-bottom mb-2"> */}
      <h5 className="text-left" style={{color:'#002f6c'}}>Past events</h5>
      {/* </div> */}
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          type="search"
          onInput={e => setglobalFilter(e.target.value)}
          placeholder="Search"
        />
      </span>
    </div>
  );
  return (
    <div className="my-activity-container container">
      <div className="my-activity-header p-3 mb-2">
        <h5 className="text-left mt-2" style={{color:'#002f6c'}}>Active events</h5>
        {(activeEvents && activeEvents.length > 0)&&
        <div className="activity-filter-actions">
          <div class="dropdown show">
            <p
              class="dropdown-toggle"
              id="dropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <FilterListIcon />
            </p>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a
                class="dropdown-item"
                onClick={() => getStatusFilter('openRequest')}
              >
                Open Request
              </a>
              {/* <a
                class="dropdown-item"
                onClick={() => getStatusFilter('approvedRequest')}
              >
                Approved Request
              </a> */}
              <a
                class="dropdown-item"
                onClick={() => getStatusFilter('pendingRequest')}
              >
                Pending Request
              </a>
              {/* <a
                class="dropdown-item"
                onClick={() => getStatusFilter('closedRequest')}
              >
                Closed Request
              </a> */}

              <a
                class="dropdown-item"
                onClick={() => getStatusFilter('openAction')}
              >
                Open Actions
              </a>
              {/* <a
                class="dropdown-item"
                onClick={() => getStatusFilter('closedAction')}
              >
                Closed Actions
              </a> */}
            </div>
          </div>

          <p onClick={() => seteventViewStyle('list')} title="List View">
            <ReorderIcon />
          </p>
          <p onClick={() => seteventViewStyle('box')} title="Card View">
            <FeaturedPlayListIcon />
          </p>
          <p onClick={() => getStatusFilter('')} title="Reset Filter">
            <RotateLeftIcon />
          </p>
        </div>
     }
      </div>
      {activeEvents && activeEvents.length > 0 ?
      <>
      {eventViewStyle == 'box' && (
        <div style={{ padding: `0 ${'40'}px` }}>
          <ItemsCarousel
            requestToChangeActive={setActiveItemIndex}
            activeItemIndex={activeItemIndex}
            numberOfCards={3}
            gutter={20}
            leftChevron={<span className="next-prev-i">&#8249;</span>}
            rightChevron={<span className="next-prev-i">&#8250;</span>}
            outsideChevron
            chevronWidth={'0'}
          >
            
            {activeEvents && activeEvents.length > 0 ? (
              activeEvents.map((data,i) => {
                return (
                  <ActiveRequests
                    requestStatus={data.status}
                    statusLabel={data.statusLabel}
                    name={data.name}
                    role={data.role}
                    id={data.id}
                    desc={data.desc}
                    index={i}
                    label={data.label}
                    type={data.type}
                    createdAt={data.createdAt}
                    updatedAt={data.updatedAt}
                    submittedBy={data.submittedBy}

                  />
                );
              })
            ) : (
              <div>No data available to display</div>
            )}
          </ItemsCarousel>
        </div>
      )}
      {eventViewStyle == 'list' && (
        <div>
          {
            <div>
              {activeEvents && activeEvents.length > 0 ? (
                <DataTable value={activeEvents} paginator rows={5}>
                  <Column field="name" header="Request" />
                  <Column field="status" header="Status" />
                  <Column field="id" header="Request ID" />
                  <Column
                    field="actions"
                    body={activelistEventActionBody}
                    header="Actions"
                  />
                </DataTable>
              ) : (
                <div>No data available to display</div>
              )}
            </div>
          }
        </div>
      )}
      </> 
      :<div className="bg-white text-center w-100 p-3">No Active Events are available to display</div>}

      <div className="mt-3">
        {pastEvents && pastEvents.length > 0 ? (
          <DataTable
            value={pastEvents}
            paginator
            rows={5}
            rowsPerPageOptions={[5, 10, 20]}
            globalFilter={globalFilter}
            header={header}
          >
            <Column field="service" header="Service" sortable />
            <Column field="product" header="Product" sortable />
            <Column field="id" header="Request ID" sortable />
            <Column field="status" header="Status" sortable />

            <Column field="date" header="Date" sortable />
          </DataTable>
        ) : (<>
        <h5 className="text-left mt-2 p-3" style={{color:'#002f6c'}}>Past events</h5>
          <div className="bg-white text-center w-100 p-3">No Past Events are available to display</div>
        </>
        )}
      </div>
    </div>
  );
};

const getEvent = event =>{
  console.log('event',event)
return {
  label: event.label,
  submittedBy: event.submittedBy,
  status: event.values['Status'],
  id: event.handle,
  desc:event.values['Description'],
  name:event.values['Requested For Display Name'],
  submissionId:event.id,
  createdAt: moment(event.createdAt).format('MM/DD/YYYY'),
  updatedAt: moment(event.updatedAt).format('MM/DD/YYYY'),
  type:event.type
}};

export const MyActivity = connect(
  state => {
    const activeEvents =
      state.submissions.active.data &&
      state.submissions.active.data.map(event => getEvent(event)).toJS();
    const pastEvents =
      state.submissions.past.data &&
      state.submissions.past.data.map(event => getEvent(event)).toJS();
    return { activeEvents, pastEvents };
  },
  { fetchSubmissions: actions.fetchSubmissionsRequest },
)(MyActivityComponent);
