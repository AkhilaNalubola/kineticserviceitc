import React, { useEffect, useState } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { Link } from '@reach/router';
import { PageHeader } from '../its_header/PageHeader';
import { HomeNewRequest } from '../its_home/HomeNewRequest';
import { connect } from '../../redux/store';
import { PageHeading } from '../its_header/PageHeading';

export const CategoriesContainer = props => {
  const [formsList, setFormsList] = useState([]);
  const [searchedVal, setSearchedVal] = useState([]);
  useEffect(() => {
    setFormsList(props.category.forms);
  }, []);
  const getSearchedKey = value => {
    setSearchedVal(value);
    let filteredForms = props.category.forms.filter((data, i) =>
      data.name.toLowerCase().includes(value.toLowerCase()),
    );
    setFormsList(filteredForms);
  };
  const { category } = props;

  return (
    <div>
      <PageHeading
        title={category ? category.name : props.categorySlug}
        showSearchFilter={true}
        searchedKey={getSearchedKey}
      />

      <div className=" px-4 bg-white">
        <HomeNewRequest forms={formsList} />
        {/* {category.forms
                .sort((a, b) => a.name.localeCompare(b.name))
                .map(form => console.log('my props2',form))} */}
        {category.formCount === 0 && (
          <div title="There are no services in this category.">No data </div>
        )}
        {searchedVal &&
          !formsList.size && (
            <div className="text-center p-4">Search results not found</div>
          )}
      </div>
    </div>
  );
};

const mapStateToProps = (state, props) => ({
  // console.log('locationprops',props)
  categories: state.servicesApp.categories,
  category: state.servicesApp.categoryGetter(props.categorySlug),
  formsListData: state,
});

export const RequestService = connect(
  mapStateToProps,
  null,
)(CategoriesContainer);
// export const RequestService = () => {
//   return (<CategoriesContainer/>
//   );
// };
