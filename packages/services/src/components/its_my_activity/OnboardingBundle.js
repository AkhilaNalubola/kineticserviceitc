import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

import PersonIcon from '@material-ui/icons/Person';
import { PageHeader } from '../its_header/PageHeader';
import { Link } from '@reach/router';
import '../../assets/styles/_onboard_bundle.scss';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    textAlign: 'left',
  },
}));

export const OnboardingBundle = () => {
  const classNamees = useStyles();
  const [activePanel, setPanel] = useState('panel2');
  const [rating, setRating] = useState(0);
  const changeRating = (value, name) => {
    setRating(value);
  };

  const handleChange = panel => (event, isExpanded) => {
    setPanel(isExpanded ? panel : false);
  };
  return (
    <div>
      <PageHeader name={'Bundle'} icon={<PersonIcon />} />
      <div className="row px-4 mt-5 bg-white">
        <div className="col-lg-8 col-md-8 col-sm-8">
          <div className="desc-container my-3 pb-3 pt-2 text-left border-bottom">
            <h5 className="description-title ">Description</h5>
            <div className="description-content">
              Lorem ipsum, or lipsum as it is sometimes known, is dummy text
              used in laying out print, graphic or web designs. The passage is
              attributed to an unknown typesetter in the 15th century who is
              thought to have scrambled parts of Cicero's De Finibus Bonorum et
              Malorum for use in a type specimen book.
            </div>
            <p className="text-right border-top pt-2 mt-2">
              <a href="#">Show more</a>
            </p>
          </div>
          <div className="bundle-items-container">
            <h6 className="text-left">4 Bundle Items</h6>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm-2">
                    <PersonIcon />
                  </div>
                  <div className="col-sm-5">
                    <div className="text-left board-name">
                      <a href="#">Product</a>
                    </div>
                  </div>
                  <div className="col-sm-1">Cost</div>

                  <div className="col-sm-3">
                    <Switch
                      // checked={state.checkedB}
                      // onChange={handleChange}
                      color="primary"
                      name="checkedB"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </div>
                </div>
              </li>
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm-2">
                    <PersonIcon />
                  </div>
                  <div className="col-sm-5">
                    <div className="text-left board-name">
                      <a href="#">Product</a>
                    </div>
                  </div>
                  <div className="col-sm-1">Cost</div>

                  <div className="col-sm-3">
                    <Switch
                      // checked={state.checkedB}
                      // onChange={handleChange}
                      color="primary"
                      name="checkedB"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </div>
                </div>
              </li>
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm-2">
                    <PersonIcon />
                  </div>
                  <div className="col-sm-5">
                    <div className="text-left board-name">
                      <a href="#">Product</a>
                    </div>
                  </div>
                  <div className="col-sm-1">Cost</div>

                  <div className="col-sm-3" />
                </div>
              </li>
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm-2">
                    <PersonIcon />
                  </div>
                  <div className="col-sm-5">
                    <div className="text-left board-name">
                      <a href="#">Product</a>
                    </div>
                  </div>
                  <div className="col-sm-1">Cost</div>

                  <div className="col-sm-3">
                    <Switch
                      // checked={state.checkedB}
                      // onChange={handleChange}
                      color="primary"
                      name="checkedB"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

        <div className="col-lg-4 col-md-4 col-sm-4 border-left">
          <div className="btns-container">
            <button
              type="button"
              className="btn btn-primary reqest-service-btn my-1"
            >
              Request Now
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
