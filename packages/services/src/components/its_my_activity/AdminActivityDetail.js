import React, { useState } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { PageHeader } from '../its_header/PageHeader';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import { OrderItems } from './OrderItems';

function getSteps() {
  return ['Submitted', 'Approval', 'Inprogress', 'Completed'];
}
const orders = ['order1', 'order2', 'order3', 'order4'];
export const AdminActivityDetail = () => {
  const useStyles = makeStyles(theme => ({
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      textAlign: 'left',
    },
  }));
  const classes = useStyles();

  const steps = getSteps();
  // const [activeStep, setActiveStep] = React.useState(1);
  // const [activePanel, setPanel] = useState('');

  // const handleChange = (panel) => (event, isExpanded) => {
  //     setPanel(isExpanded ? panel : false);
  // };
  return (
    <div>
      <PageHeader
        name={'Approval needed for 4 items'}
        icon={<PersonIcon />}
        status={'Waiting for your approval'}
      />
      <div className="row bg-white p-3">
        <div className="col-sm-8 border-right pl-4">
          <div className="border-bottom py-3 d-flex mt-2">
            <div className="col-sm-4" />
            <div className="col-sm-4">
              <div className="request-id">
                <span>Order ID</span>
                <span>{'12345'}</span>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="request-for text-left">
                <span>Request By</span>
                <span>
                  {' '}
                  <div className="user d-flex  ">
                    <Avatar className={classes.small}>A</Avatar>{' '}
                    <span className="pl-2 user-name d-flex align-items-center">
                      {'Akhila'}
                    </span>
                  </div>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="btns-container">
            <button type="button" className="btn btn-secondary my-1">
              Reject All
            </button>
            <button
              type="button"
              className="btn btn-primary reqest-service-btn my-1"
            >
              Approve All
            </button>
          </div>
        </div>

        <div className="col-sm-12 border-right pl-4">
          <div className="border-bottom py-3">
            <h5 className="text-left pl-3">Items</h5>
            <OrderItems orders={orders} />
          </div>
        </div>
      </div>
    </div>
  );
};
