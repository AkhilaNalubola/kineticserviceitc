import React, { useState } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { PageHeader } from '../its_header/PageHeader';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

function getSteps() {
  return ['Submitted', 'Approval', 'Inprogress', 'Completed'];
}

export const ActivityDetails = () => {
  const useStyles = makeStyles(theme => ({
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
  }));
  const classes = useStyles();

  const steps = getSteps();
  const [activeStep, setActiveStep] = React.useState(2);
  const [showAddComntSection, setShowAddComntSection] = React.useState(false);
  return (
    <div>
      <PageHeader
        name={'Activity Detail'}
        icon={<PersonIcon />}
        status={'Pending request'}
      />
      <div className="row bg-white ">
        <div className="col-sm-12 border-right pl-4">
          <div className="border-bottom py-3">
            <div className="row">
              <div className="col-sm-4" />
              <div className="col-sm-4">
                <div className="request-id">
                  <span>Request ID</span>
                  <span>{'12345'}</span>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="request-for text-left">
                  <span>Request For</span>
                  <span>
                    {' '}
                    <div className="user d-flex  ">
                      <Avatar className={classes.small}>A</Avatar>{' '}
                      <span className="pl-2 user-name d-flex align-items-center">
                        {'Akhila'}
                      </span>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="border-bottom text-left py-3">
            <h5>Progress</h5>
            <div>
              <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map(label => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
            </div>
          </div>
          <div className="border-bottom text-left py-3">
            <h5>Details</h5>
            <ul className="activity-details-list">
              <li>
                <div className="label-qtn">Submitted</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">Last Updated</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">Requested for mail</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">Requested for Phone</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">Requested for comapany</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">
                  What is the name of the comapny or guest?
                </div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">How many accounts do you need?</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">What is the start date?</div>
                <div className="qtn-ans">Answer</div>
              </li>
              <li>
                <div className="label-qtn">What is the end date?</div>
                <div className="qtn-ans">Answer</div>
              </li>
            </ul>
          </div>
          <div className="border-bottom text-left py-3">
            <h5>Comments</h5>
            <div className="">
              {!showAddComntSection && (
                <button
                  className="btn btn-primary"
                  onClick={() => setShowAddComntSection(true)}
                >
                  Add comment
                </button>
              )}
              {showAddComntSection && (
                <div className="border p-3">
                  <div>
                    <textarea
                      className="form-control"
                      placeholder="Leave your comment here"
                    />
                  </div>
                  <div>
                    <input type="file" className="" />
                  </div>
                  <div className="text-right">
                    <button
                      type="button"
                      className="btn btn-secondary mx-1"
                      onClick={() => setShowAddComntSection(false)}
                    >
                      Cancel
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary reqest-service-btn mx-1"
                    >
                      Submit
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
