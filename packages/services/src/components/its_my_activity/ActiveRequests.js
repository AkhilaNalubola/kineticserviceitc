import React, { useEffect } from 'react';
import PersonIcon from '@material-ui/icons/Person';

import '../../assets/styles/_my_activity.scss';
import { Link } from '@reach/router';
let customUri = '/';

export const ActiveRequests = props => {
  useEffect(() => {
    getDetailsUrl(props);
  }, []);
  const getDetailsUrl = params => {
    console.log('params', params);
    if (params.role == 'admin') {
      customUri = 'adminActivityDetails';
    } else {
      if (params.requestStatus == 'Approved') {
        customUri = 'activityApproval';
      } else {
        customUri = 'activityDetails';
      }
    }
  };
  return (
    <div>
      <div className="active-request-card card">
        <div
          className="card-header  text-white text-left"
          style={{ backgroundColor: sessionStorage.cardColor || '#002f6c' }}
        >
          <h6 className="card-title"> {props.label}</h6>
          <small className="card-subtitle mb-2">
            <em>{props.requestStatus}</em>
          </small>
        </div>
        <div className="card-body p-0">
          <div className=" bg-light-blue p-2">
            <div className="active-request-detail d-flex">
              <div className="request-icon col-sm-2">
                <PersonIcon />
              </div>
              <div className="request-naem col-sm-8 text-left d-flex">{props.submittedBy}</div>
            </div>
          </div>
          <div className="request-id text-left pl-2 pt-1">
            <small>
            <ul>
              <li>Request ID: {props.id}</li>
              <li>Type: {props.type}</li>
              <li>Created Date: {props.createdAt}</li>
              {/* <li>Submitted By: {props.submittedBy}</li> */}
              <li>Last Updated: {props.updatedAt}</li>
            </ul>
            </small>
          </div>
        </div>
        <div className="card-footer bg-white card-f-btns">
          {props.role == 'admin' ? (
            <React.Fragment>
              <span className="text-left">
                <Link to="/" className="text-danger mr-2">
                  Reject
                </Link>
                <Link to="/">Approve</Link>
              </span>
              <span className="text-right">
                <Link to={customUri}>Details</Link>
              </span>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <span className="text-left">
                <Link to="/" className="text-danger mr-2">
                  Cancel
                </Link>
                <Link
                  to={
                    props.requestStatus == 'Approved'
                      ? 'activityApproval'
                      : 'activityDetails'
                  }
                >
                  Details
                </Link>
              </span>{' '}
            </React.Fragment>
          )}
        </div>
      </div>
    </div>
  );
};
