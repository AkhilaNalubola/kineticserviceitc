import React, { useState } from 'react';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import PersonIcon from '@material-ui/icons/Person';
import { PageHeader } from '../its_header/PageHeader';
import { Link } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

function getSteps() {
  return ['Submitted', 'Approval', 'Inprogress', 'Completed'];
}

export const OrderItems = props => {
  const useStyles = makeStyles(theme => ({
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      textAlign: 'left',
      display: 'flex',
    },
  }));
  const classes = useStyles();

  const steps = getSteps();
  const [activeStep, setActiveStep] = React.useState(1);
  const [activePanel, setPanel] = useState('');

  const handleChange = panel => (event, isExpanded) => {
    setPanel(isExpanded ? panel : false);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClickMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const { orders } = props;
  return (
    <div>
      {orders &&
        orders.map((data, i) => {
          console.log('data', data, i);
          return (
            <Accordion
              expanded={activePanel == data}
              onChange={handleChange(data)}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  <div>
                    <PersonIcon />
                  </div>
                  {data}

                  <div className="text-left text-muted accordian-s-head mx-4">
                    Waiting Approval
                  </div>
                  <div>
                    <Button
                      aria-controls={'simple-menu' + i}
                      aria-haspopup="true"
                      onClick={handleClickMenu}
                    >
                      <MoreVertIcon />
                    </Button>
                    <Menu
                      id={'simple-menu' + i}
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleCloseMenu}
                    >
                      <MenuItem onClick={handleCloseMenu}>Reject</MenuItem>
                      <MenuItem onClick={handleCloseMenu}>Approve</MenuItem>
                      <MenuItem onClick={handleCloseMenu}>On Hold</MenuItem>
                      <MenuItem onClick={handleCloseMenu}>
                        Reassign Approval
                      </MenuItem>
                    </Menu>
                  </div>
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <div className="d-flex w-100 flex-column">
                  <div className="  border-bottom py-3 w-100">
                    <div className="row">
                      <div className="col-sm-4">
                        <div className="request-id">
                          <span>Request ID</span>
                          <span>{'12345'}</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className=" w-100 border-bottom  py-3">
                    <div className="row pl-3">
                      <h5>Progress</h5>
                      <div>
                        <Stepper activeStep={activeStep} alternativeLabel>
                          {steps.map(label => (
                            <Step key={label}>
                              <StepLabel>{label}</StepLabel>
                            </Step>
                          ))}
                        </Stepper>
                      </div>
                    </div>
                  </div>
                  <div className="w-100 border-bottom py-3">
                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                      <li className="nav-item">
                        <a
                          className="nav-link active"
                          id="details-tab"
                          data-toggle="tab"
                          href="#details"
                          role="tab"
                          aria-controls="details"
                          aria-selected="true"
                        >
                          Details
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          id="approval-tab"
                          data-toggle="tab"
                          href="#approval"
                          role="tab"
                          aria-controls="approval"
                          aria-selected="false"
                        >
                          Approval
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          id="comments-tab"
                          data-toggle="tab"
                          href="#comments"
                          role="tab"
                          aria-controls="comments"
                          aria-selected="false"
                        >
                          Comments
                        </a>
                      </li>
                    </ul>
                    <div className="tab-content" id="myTabContent">
                      <div
                        className="tab-pane fade show active"
                        id="details"
                        role="tabpanel"
                        aria-labelledby="details-tab"
                      >
                        <ul className="activity-details-list mt-3">
                          <li>
                            <div className="label-qtn text-left">In Bundle</div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">Submitted</div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Last Updated
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Requested for mail
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Requested for Phone
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Requested for comapany
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Please enter recipient
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Please enter reason for request?
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                          <li>
                            <div className="label-qtn text-left">
                              Please select computer type?
                            </div>
                            <div className="qtn-ans text-left">Answer</div>
                          </li>
                        </ul>
                      </div>
                      <div
                        className="tab-pane fade"
                        id="approval"
                        role="tabpanel"
                        aria-labelledby="approval-tab"
                      >
                        .p..
                      </div>
                      <div
                        className="tab-pane fade"
                        id="comments"
                        role="tabpanel"
                        aria-labelledby="comments-tab"
                      >
                        .c..
                      </div>
                    </div>
                  </div>
                </div>
              </AccordionDetails>
            </Accordion>
          );
        })}
    </div>
  );
};
