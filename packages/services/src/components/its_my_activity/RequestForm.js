import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PersonIcon from '@material-ui/icons/Person';
import { PageHeader } from '../its_header/PageHeader';
import { FormContainer } from '../form/FormContainer';
import '../../assets/styles/_my_activity.scss';
import { PageHeading } from '../its_header/PageHeading';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    textAlign: 'left',
  },
}));

export const RequestForm = (props) => {
  console.log('reqform',props)
  const classes = useStyles();
  const [activePanel, setPanel] = useState('panel2');

  const handleChange = panel => (event, isExpanded) => {
    setPanel(isExpanded ? panel : false);
  };
  return (
    <div>
      <PageHeading title={props.formSlug} />
      <div className="row px-4">
        <div className="col-lg-6 col-md-6 col-sm-6 p-2 bg-white mt-4">
          <Accordion
            expanded={activePanel == 'panel1'}
            onChange={handleChange('panel1')}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                Request Summary
                {/* <div className="text-left text-muted accordian-s-head">
                  Request for: <span>{props.formSlug}</span>
                </div> */}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <div>
                  <span className="text-muted">Service Category: </span><span className="text-capitalize">{props.categorySlug}</span>
                </div>
                <div>
                <span className="text-muted">Request Form Name:</span>  <span className="text-capitalize">{props.formSlug}</span>
                </div>
              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion
            expanded={activePanel == 'panel2'}
            onChange={handleChange('panel2')}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                Form Details
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <FormContainer
                formSlug={props.formSlug}
                navigate={props.navigate}
                submissionId={props.submissionId}
              />
            </AccordionDetails>
          </Accordion>
        </div>
      </div>
    </div>
  );
};
