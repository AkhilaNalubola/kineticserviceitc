import React, { useState } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';

const notificationData = [
  {
    date: '19/11/2020',
    info: 'Bundle Requested Successfully !',
    id: '12345',
  },
  {
    date: '19/11/2020',
    info: 'New Trouble issue Resolved !',
    id: '01256',
  },
  {
    date: '18/11/2020',
    info: 'New Device Requested Successfully !',
    id: '88745',
  },
  {
    date: '18/11/2020',
    info: 'Profile Update  Successfully !',
    id: '23126',
  },
  {
    date: '17/11/2020',
    info: 'User created Successfully !',
    id: '67893',
  },
];

export const NotificationsList = props => {
  const [globalFilter, setglobalFilter] = useState('');
  const header = (
    <div className="table-header">
      {/* <div className="my-activity-header p-3 border-bottom mb-2"> */}
      <h5 className="text-left">Notifications</h5>
      {/* </div> */}
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          type="search"
          onInput={e => setglobalFilter(e.target.value)}
          placeholder="Search"
        />
      </span>
    </div>
  );
  return (
    <div className="mt-3">
      <div className="container">
        <DataTable
          value={notificationData}
          paginator
          rows={5}
          rowsPerPageOptions={[5, 10, 20]}
          globalFilter={globalFilter}
          header={header}
        >
          <Column field="id" header="ID" sortable />
          <Column field="date" header="Date" sortable />
          <Column field="info" header="Message" />
        </DataTable>
      </div>
    </div>
  );
};
