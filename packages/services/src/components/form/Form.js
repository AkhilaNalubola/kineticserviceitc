import React, { Fragment } from 'react';
import { CoreForm } from '@kineticdata/react';
import {
  ErrorNotFound,
  ErrorUnauthorized,
  ErrorUnexpected,
} from '@kineticdata/bundle-common';
import { PageTitle } from '../shared/PageTitle';

import { I18n } from '@kineticdata/react';

export const Form = ({
  form,
  type,
  category,
  submissionId,
  handleCreated,
  handleUpdated,
  handleCompleted,
  handleLoaded,
  handleUnauthorized,
  values,
  kappSlug,
  formSlug,
  authenticated,
}) => (
  <div className="embedded-core-form--wrapper w-100">
    {submissionId ? (
      <I18n submissionId={submissionId} public={!authenticated}>
        <CoreForm
          submission={submissionId}
          loaded={handleLoaded}
          updated={handleUpdated}
          completed={handleCompleted}
          unauthorized={handleUnauthorized}
          public={!authenticated}
        />
      </I18n>
    ) : (
      <I18n
        context={`kapps.${kappSlug}.forms.${formSlug}`}
        public={!authenticated}
      >
        <CoreForm
          kapp={kappSlug}
          form={formSlug}
          loaded={handleLoaded}
          created={handleCreated}
          updated={handleUpdated}
          completed={handleCompleted}
          unauthorized={handleUnauthorized}
          values={values}
          notFoundComponent={ErrorNotFound}
          unauthorizedComponent={ErrorUnauthorized}
          unexpectedErrorComponent={ErrorUnexpected}
          public={!authenticated}
        />
      </I18n>
    )}
  </div>
);
