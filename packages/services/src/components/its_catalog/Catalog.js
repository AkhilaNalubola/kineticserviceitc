import React, { useState } from 'react';
import { Link } from '@reach/router';
import { InputText } from 'primereact/inputtext';
import ReportProblemIcon from '@material-ui/icons/ReportProblem';
import AddBoxIcon from '@material-ui/icons/AddBox';
import {
  ErrorOutline,
  PlaylistAddCheck,
  Wifi,
  FormatListBulleted,
  History,
  Description,
  Announcement,
} from '@material-ui/icons';

let catalogData = [];
let ddata = [
  // {
  //   name: 'Request Service',
  //   description: 'Description',
  //   action: 'Request Now',
  //   url: 'requestService',
  //   icon: <ErrorOutline />,
  // },
  {
    name: 'Service Request',
    description: 'Service Request has request forms for new requests and access from organization',
    action: 'Request Now',
    url: 'requestService/service-request',
    icon: <ErrorOutline />,
  },
  {
    name: 'Incident Request',
    description: 'Incident Request has request forms for rise new issues and troubleshooting etc.',
    action: 'Request Now',
    url: 'requestService/incident-request',
    icon: <Announcement />,
  },
  
  // {
  //   name: 'Bundle',
  //   description: 'Description',
  //   action: 'Request Now',
  //   url: 'bundle',
  //   icon: <List />,
  // },
  // {
  //   name: 'Setting Up VPN',
  //   description: 'Description',
  //   action: 'Request Now',
  //   url: 'myActivity',
  //   icon: <Wifi />,
  // },
  {
    name: 'Article',
    description: 'This article will provide information about requests',
    action: 'View Article',
    url: 'article',
    icon: <Description />,
  },
  {
    name: 'Approvals',
    description: 'Approval pages provides the list of approved events present in my activity page',
    action: 'View Approvals ',
    url: 'myActivity',
    icon: <PlaylistAddCheck />,
  },
];
const newRequestData = [
  {
    name: 'Non-Inventory Item',
    value: '1',
  },
  {
    name: 'Request Badge Access',
    id: '',
  },
  {
    name: 'Request Call Forwarding',
    id: '',
  },
  {
    name: 'Request Mobile Device',
    id: '',
  },
  {
    name: 'Request Phone Accessory',
    id: '',
  },
  {
    name: 'Request Soft Phone',
    id: '',
  },
  {
    name: 'Request Voice Mail to Email',
    id: '',
  },
  {
    name: 'Exchange a Device',
    id: '',
  },
  {
    name: 'Request Mobile Device Accessory',
    id: '',
  },
  {
    name: 'Training Request',
    id: '',
  },
  {
    name: 'Request Teleconference Audio Bridge',
    id: '',
  },
  {
    name: 'Submit an Email Request',
    id: '',
  },
  {
    name: 'Request a Computer',
    id: '',
  },
  {
    name: 'Request Software',
    id: '',
  },
  {
    name: 'Enroll Mobile Device',
    id: '',
  },
  {
    name: 'Request Loaner Equipment',
    id: '',
  },
  {
    name: 'Request a Software Enhancement',
    id: '',
  },
  {
    name: 'Return a Device',
    id: '',
  },
  {
    name: 'Request Application Access',
    id: '',
  },
  {
    name: 'Request Computer Accessory',
    id: '',
  },
  {
    name: 'Request Network Shared Drive Access',
    id: '',
  },
  {
    name: 'Request Digital Signage',
    id: '',
  },
  {
    name: 'Blocked Content Access',
    id: '',
  },
  {
    name: 'Change Device Ownership',
    id: '',
  },
  {
    name: 'Add an Existing Computer',
    id: '',
  },
  {
    name: 'Site Request',
    id: '',
  },
  {
    name: 'Request Inventory and Promo Items',
    id: '',
  },
  {
    name: 'Request Promo Items - Coins and Poster',
    id: '',
  },
  {
    name: 'Request a New myEnterprise Report',
    id: '',
  },
  {
    name: 'Request a Power BI Report',
    id: '',
  },
  {
    name: 'Request IP Desk Phone',
    id: '',
  },
  {
    name: 'Request SharePoint Site',
    id: '',
  },
];
const newTroubleData = [
  {
    name: 'Report a Computer Incident',
    id: '',
  },
  {
    name: 'Desk or Soft Phone Incident',
    id: '',
  },
  {
    name: 'Report a Printer Incident',
    id: '',
  },
  {
    name: 'Add an Existing Computer',
    id: '',
  },
  {
    name: 'Report Device Lost or Stolen',
    id: '',
  },
];
export const Catalog = props => {
  // const [catalogData,setcatalogData]=useState([])
  const search = props.location.search;
  const params = new URLSearchParams(search);
  const foo = props.from;
  if (props.from == 'newRequest') {
    catalogData = newRequestData;
  } else if (props.from == 'newTrouble') {
    catalogData = newTroubleData;
  } else {
    catalogData = ddata;
  }
  console.log('props,', props, foo);
  return (
    <div className="catalog-container container">
      {/* <div className="catalog-search m-2 pt-3">
        <span className="p-input-icon-left w-100 ">
          <i className="pi pi-search" />
          <InputText
            type="search"
            className="w-100"
            placeholder="Search Here"
          />
        </span>
      </div>
      */}
      <div className="search-content-result mt-4">
        <ul class="list-group list-group-flush">
          {foo == 'all' &&
            catalogData.map((data, index) => {
              return (
                <li class="list-group-item" key={'catalog_item_' + index}>
                  <div className="row">
                    <div className="col-sm-1 d-flex align-items-center" style={{ color: 'gray' }}>
                      {data.icon}
                    </div>
                    <div className="col-sm-3 text-left d-flex align-items-center">
                      <h6>{data.name}</h6>
                    </div>
                    <div className="col-sm-5">{data.description}</div>

                    <div className="col-sm-3 text-left d-flex align-items-center">
                      <Link to={'/kapps/services/' + data.url}>
                        {data.action}
                      </Link>
                    </div>
                  </div>
                  {/* {data.name} */}
                </li>
              );
            })}
          {(foo == 'newRequest' || foo == 'newTrouble') &&
            catalogData.map((data, index) => {
              return (
                <li class="list-group-item">
                  <div className="row">
                    <div className="col-sm-1">
                      <a data-toggle="collapse" href={'#collapse' + index}>
                        {foo == 'newTrouble' ? (
                          <ReportProblemIcon />
                        ) : (
                          <AddBoxIcon />
                        )}
                      </a>
                    </div>
                    <div className="col-sm-5 text-left">{data.name}</div>
                    <div className="col-sm-6 text-center">
                      <Link to={props.uri}>Request Now</Link>
                    </div>
                  </div>
                  <div
                    id={'collapse' + index}
                    class="panel-collapse collapse text-left text-secondary"
                  >
                    <div className="row">
                      <div className="col-sm-1" />
                      <div className="col-sm-11">
                        Description about <em>{data.name}</em>
                        <p>
                          Lorem ipsum, or lipsum as it is sometimes known, is
                          dummy text used in laying out print, graphic or web
                          designs. The passage is attributed to an unknown
                          typesetter in the 15th century who is thought to have
                          scrambled parts of Cicero's De Finibus Bonorum et
                          Malorum for use in a type specimen book.
                        </p>
                      </div>
                    </div>
                  </div>
                </li>
              );
            })}
        </ul>
      </div>
    </div>
  );
};
