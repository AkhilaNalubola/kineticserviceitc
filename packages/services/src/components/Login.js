import React, { useEffect } from 'react';
import { Link } from '@reach/router';
import { Nav, NavItem } from 'reactstrap';
import { get } from 'immutable';
import { services } from '@kineticdata/bundle-common';
import { I18n } from '@kineticdata/react';
import { isActiveClass } from '../utils';
import logo from '../assets/images/Logo_Company_Tagline_small.jpg';

import { Helmet } from 'react-helmet';

import '../assets/styles/_login.scss';

export const Login = ({ loginProps }) => {
  console.log('loginProps', loginProps);
  return (
    <div>
      <Helmet>
        <title>signin | Kinetic CE </title>
      </Helmet>
      <div className="">
        <div className="its-login-page   col-sm-12">
          <div className="row  ">
            <div className="col-sm-7 border-right" />
            <div className="col-sm-5 its-login-container bg-white p-5">
              <div class="card text-center">
                <div>
                  <img src={logo} className="img-fluid" width="50%" />
                </div>
                <div class="card-header font-weight-bold">Login</div>
                <div class="card-body">
                  <form>
                    <div class="form-group text-left">
                      <label for="exampleInputEmail1">User Name </label>
                      <input
                        type="email"
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                      />
                      {/* <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> */}
                    </div>
                    <div class="form-group text-left">
                      <label for="exampleInputPassword1">Password</label>
                      <input
                        type="password"
                        class="form-control"
                        id="exampleInputPassword1"
                      />
                    </div>

                    <button type="submit" class="btn btn-primary ">
                      Submit
                    </button>
                    <div>
                      <span className="text-danger">
                        <I18n>{loginProps.error || ' '}</I18n>
                      </span>
                      <em>
                        <small className="text-danger">
                          {/* If you need login assistance please contact your service desk */}
                          For login assistance please contact your IT service
                          desk using contact methods shown below
                          <sup>*</sup>
                        </small>
                      </em>
                    </div>
                    {/* <div class="form-group form-check mt-3">
                  <a href="#">Forgot Password?</a>
                </div> */}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
