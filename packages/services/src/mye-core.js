$(document).ready(function() {
  /* Initialize User Voice Widget SDK */
  UserVoice = window.UserVoice || [];
  (function() {
    var uv = document.createElement('script');
    uv.type = 'text/javascript';
    uv.async = true;
    uv.src = '//widget.uservoice.com/JKbZnqXe48Mb0AWguDLA.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(uv, s);
  })();
  EW.Portal.wireup();
  $.doTimeout('sessionCheckTimer', 30000, function() {
    return EW.Portal.checkSession();
  });
  EW.ServiceItems.wireup();
  $(window).one('load', EW.Portal.resize);
  EW.User.setLoggedInUserHomepage();
});

var EW = EW || {};

// EW.Portal contains functions and variables used to run the portal
// including loading of data and interactions between portal elements.
EW.Portal = {
  _lastViewport: '', // stores the visible viewport when the overlay is shown
  sessionCountdown: 0,
  sessionCountdownLength: 300,
  sessionWarningAt: 8,

  wireup: function() {
    /* Set User Voice Widget Options */
    UserVoice.push([
      'set',
      {
        forum_id: '348867',
        accent_color: '#e41c3d',
        trigger_color: 'white',
        trigger_background_color: '#e41c3d',
        contact_enabled: false,
        mode: 'feedback',
      },
    ]);

    //Identify Currently Logged In User With User Voice
    UserVoice.push([
      'identify',
      { email: EW.User.LoggedIn.email, name: EW.User.LoggedIn.fullname },
    ]);

    /*Enable Chat If Needed and Add Custom User Voice Trigger */
    if (EW.Portal.chatEnabled) {
      MyEChat.init({
        chatServer: BUNDLE.config.chatServer,
        username: EW.User.LoggedIn.remedyid,
        target: 'chatTrigger',
      });
      UserVoice.push(['addTrigger', '#feedbackTrigger', {}]);
    } else {
      UserVoice.push(['addTrigger', { trigger_position: 'bottom-right' }]);
    }

    $(window).on('resize', EW.Portal.resize);
    $('#masthead-menu-more').on('click', EW.Portal.showMastheadMore);

    $('#myE-menu')
      .mmenu({
        searchfield: {
          add: true,
          addTo: 'menu',
          placeholder: 'How Can I Help You?',
          search: false,
        },
        offCanvas: {
          position: 'left',
          zposition: 'front',
          pageSelector: '#content-wrapper',
        },
        footer: {
          add: true,
          content: 'myENTERPRiSE',
        },
      })
      .on('opening.mm', function() {
        /* Moves Left Nav Section Along With Menu When Opening & Closing && Hide QuickLinks */
        $('.myE-leftnav-quicklink, .myE-leftnav-assistMe').fadeOut(100);
        $('#mm-0').perfectScrollbar('update');
        $('.left-nav').animate(
          { width: '+=' + $('#myE-menu').outerWidth() + 'px' },
          300,
        );
      })
      .on('closing.mm', function() {
        /* Moves Left Nav Section Along With Menu When Opening & Closing && Hide QuickLinks */
        $('.myE-leftnav-quicklink, .myE-leftnav-assistMe').fadeIn(100);
        $('.left-nav').animate(
          { width: '-=' + $('#myE-menu').outerWidth() + 'px' },
          300,
        );
        /* Resets Menu Level */
        $('#mm-0').trigger('open.mm');
      });

    /* Add Custom ID To Search Input Field */
    $('.mm-search input').attr('id', 'myE-leftnav-search');

    /* Hide Request Something And Report Trouble Sections As They Have Their Own Links */
    $('span:contains(Request Something), span:contains(Report Trouble)')
      .parent()
      .addClass('mm-hidden');

    /* This Is the Click Handler Section For The Top Nav Buttons (Request Something and Report Trouble) && QuickLinks */
    $('#myE-menu').on('click', '.myE-leftnav-topbutton', function() {
      var id = $('#myE-menu')
        .find('span:contains(' + $(this).attr('myE-panel') + ')')
        .parent()
        .find('a')
        .attr('href');
      $(id).trigger('open.mm');
    });
    $('.left-nav').on('click', '.myE-leftnav-quicklink', function() {
      var id = $('#myE-menu')
        .find('span:contains(' + $(this).attr('myE-panel') + ')')
        .parent()
        .find('a')
        .attr('href');
      $(id + ', #myE-menu').trigger('open.mm');
    });

    /* On Hovers */
    $('.mye-icon-menuqlreq').hover(
      function() {
        $(this)
          .removeClass('mye-icon-menuqlreq')
          .addClass('mye-icon-menuqlreqfill');
      },
      function() {
        $(this)
          .removeClass('mye-icon-menuqlreqfill')
          .addClass('mye-icon-menuqlreq');
      },
    );
    $('.mye-icon-menuqlinc').hover(
      function() {
        $(this)
          .removeClass('mye-icon-menuqlinc')
          .addClass('mye-icon-menuqlincfill');
      },
      function() {
        $(this)
          .removeClass('mye-icon-menuqlincfill')
          .addClass('mye-icon-menuqlinc');
      },
    );

    $('.mye-icon-leftreq').hover(
      function() {
        $(this)
          .removeClass('mye-icon-leftreq')
          .addClass('mye-icon-leftreqhover');
      },
      function() {
        $(this)
          .removeClass('mye-icon-leftreqhover')
          .addClass('mye-icon-leftreq');
      },
    );

    $('.mye-icon-leftinc').hover(
      function() {
        $(this)
          .removeClass('mye-icon-leftinc')
          .addClass('mye-icon-leftinchover');
      },
      function() {
        $(this)
          .removeClass('mye-icon-leftinchover')
          .addClass('mye-icon-leftinc');
      },
    );

    $('.mye-icon-leftam').hover(
      function() {
        $(this)
          .removeClass('mye-icon-leftam')
          .addClass('mye-icon-leftamhover');
      },
      function() {
        $(this)
          .removeClass('mye-icon-leftamhover')
          .addClass('mye-icon-leftam');
      },
    );

    $('.menu-icon-link').hover(
      function() {
        $(this).css('color', '#fff');
      },
      function() {
        $(this).css('color', '#ccc');
      },
    );

    /* Remaining Clickhandlers For the Rest of the content */

    $('#myE-menu').on('click', '[data-action]', function() {
      var f = $(this)
        .attr('data-action')
        .split('~');

      if (EW.ServiceItems.serviceItemOpen === false) {
        if ($('#siViewport').is(':visible')) {
          EW.ServiceItems.close();
        }
        EW.Helpers.executeFunctionByName(f[0], window, f.slice(1));
      } else {
        EW.ServiceItems.showInterfaceProtectionWarning(f[0], f.slice(1));
      }

      $('#myE-menu').trigger('close.mm');
    });

    $('#masthead-more-external').on('click', 'li', function() {
      $('#masthead-more-menu')
        .hide()
        .off('clickoutside');
      $('#masthead-menu-more').removeClass('masthead-menu-selected');

      if (
        $(this)
          .attr('mye-link')
          .indexOf('http://') < 0 &&
        $(this)
          .attr('mye-link')
          .indexOf('https://') < 0
      ) {
        window.open('http://' + $(this).attr('mye-link'));
      } else {
        window.open($(this).attr('mye-link'));
      }
    });

    $('#mm-0').perfectScrollbar();

    EW.Modules.Omnibox.load();

    // Set two default functions to prevent the answerValue class from being copied by Select2.
    $.fn.select2.defaults.adaptDropdownCssClass = function(c) {
      return c === 'answerValue' ? '' : c;
    };
    $.fn.select2.defaults.adaptContainerCssClass = function(c) {
      return c === 'answerValue' ? '' : c;
    };

    /* Initialize Formats For Datatable Sorting of Various Date Formats */
    $.fn.dataTable.moment('MM.DD.YYYY');
    $.fn.dataTable.moment('DD.MM.YYYY');
    $.extend($.fn.dataTable.defaults, {
      language: {
        lengthMenu: '_MENU_',
        info: 'SHOWING _START_ TO _END_ OF _TOTAL_',
        infoEmpty: 'NO RECORDS TO SHOW',
        emptyTable:
          '<div class="mye-icon-noreports" style="margin: 10px auto 20px auto"></div><div class="mye-gray-text">No data available</div>',
        zeroRecords:
          '<div class="mye-icon-noreports" style="margin: 10px auto 20px auto"></div><div class="mye-gray-text">No records to display</div>',
      },
      sPaginationType: 'myE',
      dom: 't<"mye-dt-foot"lpi>',
      lengthMenu: [
        [10, 25, 50, -1],
        ['Show 10', 'Show 25', 'Show 50', 'Show All'],
      ],
      initComplete: function(oSettings, json) {
        $(oSettings.aanFeatures.l)
          .find('select')
          .select2({ minimumResultsForSearch: -1 })
          .on('change', function() {
            if ($('.mye-slider:visible').length > 0) {
              $('.mye-slider:visible').myeSlider('resizePanelViewport');
            }
          });
      },
    });

    $('#masthead-search, #myE-leftnav-search').on(
      'keyup',
      $.debounce(1000, function(e) {
        if ($(this).val() !== '') {
          if (EW.ServiceItems.serviceItemOpen === false) {
            if ($('#siViewport').is(':visible')) {
              EW.ServiceItems.close();
            }
            EW.Modules.Omnibox.show(e, $(this).val());
            $(this).val('');
          } else {
            EW.ServiceItems.showInterfaceProtectionWarning(function() {
              $('#masthead-search, #myE-leftnav-search').trigger('keyup');
            });
          }

          $('#myE-menu').trigger('close.mm');
        }
      }),
    );

    $('#modalViewport').on(
      'click',
      '.mye-mo-cancel',
      EW.Portal.hideModalOverlay,
    );
  },

  resize: function() {
    $('#masthead-more-items li').appendTo('#masthead-menu-ul');
    $('#masthead-menu-more').appendTo('#masthead-menu-ul');

    var cw = $(window).width();

    // Adjust the logo
    if (cw > 1100) {
      if ($('#masthead-logo').hasClass('masthead-logo-small')) {
        $('#masthead-logo')
          .removeClass('masthead-logo-small')
          .addClass('masthead-logo-large');
      }
    } else {
      if ($('#masthead-logo').hasClass('masthead-logo-large')) {
        $('#masthead-logo')
          .removeClass('masthead-logo-large')
          .addClass('masthead-logo-small');
      }
    }

    var $el = $('#masthead-more-menu'),
      tempWidth = 0,
      maxWidth =
        $('#masthead').outerWidth() -
        $('#masthead-user').outerWidth() -
        $('#masthead-logo').outerWidth() -
        $('#masthead-menu-more').outerWidth() -
        40,
      $moreItems = $('#masthead-menu-ul li')
        .not('#masthead-menu-more')
        .filter(function() {
          tempWidth += $(this).outerWidth();
          if (tempWidth >= maxWidth) return true;
          else return false;
        });

    if ($moreItems.size() > 0) {
      $('#masthead-menu-more').show();
      $('#masthead-more-items')
        .show()
        .append($moreItems);
    } else {
      if ($('#masthead-more-external').size() > 0) {
        $('#masthead-menu-more').show();
      } else {
        $('#masthead-menu-more').hide();
      }
    }

    $el
      .position({
        my: 'left top',
        at: 'left bottom',
        of: '#masthead-menu-more',
      })
      .css({ top: '54px' });

    // resize content-wrapper width to window width minus left nav menu width (40).
    $('#content-wrapper').css({ width: cw - 53 });

    if ($('#obsViewport').is(':visible')) EW.Modules.Omnibox.resize();
  },

  showMastheadMore: function(e) {
    var $el = $('#masthead-more-menu');

    e.stopPropagation();

    if ($el.is(':visible')) {
      $el.hide();
      $('#masthead-menu-more').removeClass('masthead-menu-selected');
    } else {
      $el
        .show()
        .position({
          my: 'left top',
          at: 'left bottom',
          of: '#masthead-menu-more',
        })
        .css({ top: '54px' });
      $('#masthead-menu-more').addClass('masthead-menu-selected');

      $el.one('clickoutside', function() {
        $el.hide().off('clickoutside');
        $('#masthead-menu-more').removeClass('masthead-menu-selected');
      });
    }
  },

  showUserMenu: function(e) {
    e.stopPropagation();

    $('#masthead-user-menu')
      .show()
      .position({
        my: 'right top',
        at: 'right bottom',
        of: '#masthead-user-image',
      })
      .css('margin-top', '8px');

    $('#masthead-user-image').attr('onclick', '');
    $('#masthead-user-menu').one('clickoutside', function() {
      $('#masthead-user-menu')
        .hide()
        .css('margin-top', '')
        .off('clickoutside');
      $('#masthead-user-image').attr(
        'onclick',
        'EW.Portal.showUserMenu(event);',
      );
    });
  },

  /* Shows a full size panel with content. 
	 *
	 * arguments:   content [, data] [, callback] [, callbackData]
	 * content is either a Url or a static string to display. Url must be first characters to be considered.
	 * data is optional data to send to JSP
	 * callback is option callback function.
	 */
  showModalOverlay: function() {
    EW.Portal.extendSession();
    EW.Portal._showModalOverlay();

    var s = arguments[0],
      data = {},
      callback = null,
      cbData = {};

    if (arguments.length === 2) {
      if (typeof arguments[1] === 'function') {
        callback = arguments[1];
      } else {
        if (typeof arguments[1] === 'object') {
          data = arguments[1];
        }
      }
    } else if (arguments.length === 3) {
      if (typeof arguments[2] === 'function') {
        if (typeof arguments[1] === 'object') {
          data = arguments[1];
        }

        callback = arguments[2];
      } else if (typeof arguments[1] === 'function') {
        callback = arguments[1];
        cbData = arguments[2];
      }
    } else if (arguments.length === 4) {
      if (typeof arguments[1] === 'object') {
        data = arguments[1];
      }

      if (typeof arguments[2] === 'function') {
        callback = arguments[2];
        cbData = arguments[3];
      }
    }

    // test s to see if it is a Url and starts with the Url. Otherwise assume it is a string to display.
    if (
      0 ===
      s.search(
        /([(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6})|(http:\/\/localhost)\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi,
      )
    ) {
      EW.Portal.displayLoadingImage();
      $.ajax({
        type: 'GET',
        url: s,
        data: $.extend({ _: new Date().getTime() }, data),
        success: function(data) {
          $('#modalViewportContent')
            .fadeIn(500)
            .html($.trim(data));

          if (callback && typeof callback === 'function') callback(cbData);
        },
        error: function(data) {
          $('#modalViewportContent').html(data);
        },
      }).always(EW.Portal.hideLoadingImage);
    } else {
      $('#modalViewportContent').html(s);
    }

    $(
      '#masthead-logo, #masthead-menu-ul > li, #masthead-more-items > li, span[data-action]',
    )
      .not('#masthead-menu-more')
      .on('click', EW.Portal.hideModalOverlay);
  },

  hideModalOverlay: function() {
    $(
      '#masthead-logo, #masthead-menu-ul > li, #masthead-more-items > li, span[data-action]',
    )
      .not('#masthead-menu-more')
      .off('click', EW.Portal.hideModalOverlay);

    EW.Portal.Viewports.restore();
  },

  // private method to handle the showing of the model overlay panel.
  // registers a clickoutside event handler. Handler is removed when panel is hidden.
  _showModalOverlay: function() {
    $('#modalViewportContent').empty();
    EW.Portal.Viewports.swap('modalViewport');
  },

  loadContent: function(panel, url, callback, callbackData) {
    EW.Portal.extendSession();

    var $panel;

    // If panel is not provided default to #contentViewport else ensure first character is # and add if not.
    if (panel === null || typeof panel !== 'string') {
      panel = '#contentViewport';
    } else {
      if (panel.slice(0) !== '#') panel = '#' + panel;
    }

    $panel = $(panel);

    if ($panel.size() === 0) {
      console.log('Unable to locate viewport: ' + panel);
      return;
    }

    $('#content-wrapper').scrollTop(0);
    if (EW.ServiceItems.serviceItemOpen === false) {
      EW.Portal.displayLoadingImage();
    }

    $.ajax({
      type: 'GET',
      url: url,
      data: {
        _: new Date().getTime() /*, u: EW.User.OfInterest.remedyid, c: EW.User.OfInterest.company */,
      },
      success: function(data) {
        if (EW.ServiceItems.serviceItemOpen === false) {
          $panel
            .hide()
            .empty()
            .fadeIn(1000)
            .html($.trim(data));
        } else {
          $panel.empty().html($.trim(data));
        }

        if (callback && typeof callback === 'function') callback(callbackData);
      },
      error: function(data) {
        $('#contentViewport').html(data);
      },
    }).always(EW.Portal.hideLoadingImage);
  },

  changeModuleTo: function(m) {
    $('#masthead-menu-ul li, #masthead-more-items li').removeClass(
      'masthead-menu-selected',
    );
    $('#' + m).addClass('masthead-menu-selected');

    if (m === 'masthead-dashboard') {
      $('#contentViewport')
        .addClass('dashboard-viewport-offset')
        .hide();
    } else {
      $('#contentViewport')
        .removeClass('dashboard-viewport-offset')
        .hide();
    }

    // Hide the more menu and remove clickoutside event handler.
    $('#masthead-more-menu')
      .hide()
      .off('clickoutside');
    $('#masthead-menu-more').removeClass('masthead-menu-selected');

    if (!$('.masthead-menu-selected').is(':visible')) {
      $('#masthead-menu-ul > li')
        .not('#masthead-menu-more')
        .last()
        .appendTo('#masthead-more-items');
      $('.masthead-menu-selected').insertBefore('#masthead-menu-more');
    }
  },

  displayLoadingImage: function() {
    if (
      !$('#mye-portal-loader').is(':visible') &&
      !$('.portal-dialog').is(':visible')
    ) {
      $('body, #contentViewport').scrollTop(0);
      $('#mye-portal-loader')
        .fadeIn()
        .position({ my: 'center center', at: 'center center', of: window });
      $('body').prepend(
        $('<div></div>')
          .attr('id', 'overlay')
          .addClass('portalOverlay'),
      );
    }
  },

  hideLoadingImage: function() {
    if ($('#mye-portal-loader').is(':visible')) {
      $('#overlay').remove();
      $('#mye-portal-loader').hide();
    }
  },

  CrumbView: {
    // Stores the crumbs
    _crumbs: [],

    // Next id when new panel is created
    _nextId: 0,

    show: function() {
      // Save viewport to the stack
      EW.Portal.Viewports.swap('crumbViewport');
    },

    // Clears all crumbs - called when a new crumb stack is created.
    clear: function() {
      EW.Portal.CrumbView._crumbs = [];
    },

    nextId: function() {
      return EW.Portal.CrumbView._nextId++;
    },

    // Searches crumb stack for panel. Reurns panel id if found else false.
    find: function(url) {
      var retVal = false;

      for (var i = 0; i < EW.Portal.CrumbView._crumbs.length; i++) {
        if (EW.Portal.CrumbView._crumbs[i].url === url) {
          retVal = EW.Portal.CrumbView._crumbs[i].id;
          break;
        }
      }

      return retVal;
    },

    add: function(title, url, callback) {
      var p = EW.Portal.CrumbView.find(url);

      // New panel
      if (p === false) {
        p = 'crumb' + EW.Portal.CrumbView.nextId();
        EW.Portal.CrumbView._crumbs.push({ title: title, url: url, id: p });

        $('<div></div>')
          .attr('id', p)
          .addClass('crumb-panel')
          .appendTo('#crumbViewport');

        EW.Portal.loadContent(p, url, callback);
      }

      // Show the new or existing panel
      EW.Portal.CrumbView.showPanel(p);

      return p;
    },

    showPanel: function(p) {
      $('.crumb-panel').hide();
      $('#' + p).fadeIn(1000);
    },
  },

  executeTree: function(treeGroup, tree, dataContent, callback) {
    /* dataContent is an object */
    EW.Portal.extendSession();
    EW.Portal.displayLoadingImage();
    treeGroup = encodeURIComponent(treeGroup);
    tree = encodeURIComponent(tree);
    $.ajax({
      type: 'POST',
      url:
        BUNDLE.config.taskServer +
        'kinetic-task/app/api/v1/run-tree/Request API/' +
        treeGroup +
        '/' +
        tree,
      data: JSON.stringify(dataContent),
      contentType: 'application/json',
      dataType: 'json',
      complete: function(data) {
        if (typeof callback === 'function') {
          callback();
        }
        EW.Portal.hideLoadingImage();
      },
    });
  },

  launchDialog: function(
    title,
    content,
    continueText,
    cancelText,
    proceedFunction,
  ) {
    $('body, #contentViewport').scrollTop(0);
    $('body').prepend(
      $('<div></div>')
        .attr('id', 'overlay')
        .addClass('portalOverlay'),
    );

    if (continueText !== '' && continueText != null) {
      $('#myeDialog .portal-dialog-leave-button').html(continueText);
    } else {
      $('#myeDialog .portal-dialog-leave-button').html('Proceed');
    }

    if (cancelText !== '' && cancelText != null) {
      $('#myeDialog .portal-dialog-default-button').html(cancelText);
    } else {
      $('#myeDialog .portal-dialog-default-button').html('Cancel');
    }

    $('#myeDialog .portal-dialog-header').html(title);
    $('#myeDialog .portal-dialog-message').html(content);

    $('#myeDialog')
      .fadeIn(200, function() {
        $('#myeDialog .portal-dialog-leave-button').on('click', function() {
          EW.Portal.hideDialog();
          proceedFunction();
        });

        $('#myeDialog .portal-dialog-default-button').on('click', function() {
          EW.Portal.hideDialog();
        });
      })
      .position({ my: 'center center', at: 'center center', of: window });
  },

  hideDialog: function() {
    $('#overlay').remove();
    $('#myeDialog').fadeOut(200, function() {
      $('#myeDialog .portal-dialog-header').empty();
      $('#myeDialog .portal-dialog-message').empty();
      $('#myeDialog .portal-dialog-leave-button')
        .empty()
        .off('click');
      $('#myeDialog .portal-dialog-default-button')
        .empty()
        .off('click');
    });
  },

  checkSession: function() {
    var now = new Date();
    var targetTime =
      EW.Portal.sessionExpires - 60000 * EW.Portal.sessionWarningAt;
    var newMilli = now.getTime();

    if (
      EW.Portal.sessionExpires - now.getTime() <
      60000 * EW.Portal.sessionWarningAt
    ) {
      EW.Portal.showSessionWarning();
      $.doTimeout('sessionCheckTimer');
      $.doTimeout('sessionLogoutTimer', 1000, function() {
        return EW.Portal.updateSessionCountdown();
      });
      return false;
    } else {
      return true;
    }
  },

  extendSession: function() {
    $.doTimeout('sessionLogoutTimer');

    EW.Portal.updateSessionTimeout();

    $.ajax({
      type: 'GET',
      url: BUNDLE.packagePath + 'interface/callbacks/Session/PingSession.jsp',
      success: function(data) {
        if ($('#sessionTimeoutDialog').is(':visible')) {
          EW.Portal.hideSessionWarning();
        }

        EW.Portal.updateSessionTimeout();
        $.doTimeout('sessionCheckTimer', 30000, function() {
          return EW.Portal.checkSession();
        });
      },
    });

    $.doTimeout('sessionCheckTimer', 30000, function() {
      return EW.Portal.checkSession();
    });
  },

  formatCountdownTime: function(s) {
    s = Math.floor(s / 1000);
    var countdownMinutes = Math.floor(s / 60);
    var countdownSeconds = s - countdownMinutes * 60;

    var countdownText = '' + countdownMinutes + ':';
    if (countdownSeconds < 10) countdownText += '0';
    countdownText += countdownSeconds;

    return countdownText;
  },

  updateSessionCountdown: function() {
    var sessionCountdownDelta =
      EW.Portal.sessionExpires -
      EW.Portal.sessionWarningAt * 60000 +
      EW.Portal.sessionCountdownLength * 1000 -
      new Date().getTime();

    $('#sessionExpiryTimer').text(
      EW.Portal.formatCountdownTime(sessionCountdownDelta),
    );

    if (sessionCountdownDelta <= 0) {
      $.doTimeout('sessionLogoutTimer');
      EW.Portal.logout();
      return false;
    }
    return true;
  },

  updateSessionTimeout: function() {
    EW.Portal.sessionExpires =
      new Date().getTime() + EW.Portal.sessionLength * 60 * 1000;
  },

  invalidateSession: function() {
    $.get(
      BUNDLE.packagePath +
        'interface/callbacks/Session/InvalidateSession.jsp?_' +
        new Date().getTime(),
    );
  },

  showSessionWarning: function() {
    $('body').prepend(
      $('<div></div>')
        .attr('id', 'overlay')
        .addClass('portalOverlay'),
    );
    $('#sessionTimeoutDialog')
      .show()
      .position({ my: 'center center', at: 'center center', of: window });
    $(document).on('keypress', function(e) {
      if (e.which === 13) {
        EW.Portal.extendSession();
      }
    });
  },

  hideSessionWarning: function() {
    $('#overlay').remove();
    $('#sessionTimeoutDialog').hide();
  },

  logout: function() {
    /*
		*****Code For SSO******

		var idProvider = $("<form></form>");
		idProvider.attr(
		{
			id     : "idProvider",
			action : BUNDLE.config.idp + '/atriumsso/saml2/jsp/idpSingleLogoutInit.jsp?binding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST&goto=' + window.location.origin + '/kinetic/themes/myenterprise/packages/myE/logout.jsp',
			method : 'POST'
		});
		$("body").append(idProvider);
		$("#idProvider").submit();
		*/
    var logoutForm = $('<form></form>');
    logoutForm.attr({
      id: 'logoutform',
      action: BUNDLE.packagePath + 'logout.jsp',
      method: 'POST',
    });
    logoutForm.append(
      $('<input />').attr({
        type: 'hidden',
        name: 'portalUrl',
        value: parent.window.location.href,
      }),
    );

    $('body').append(logoutForm);
    $('#logoutform').submit();
  },

  Viewports: {
    // viewport stack
    _v: [],

    swap: function(showViewportId, callback, callbackData) {
      var $v = $('#content-wrapper')
        .children(':visible')
        .first();
      if (typeof $v.attr('id') != 'undefined') {
        EW.Portal.Viewports._v.push($v.attr('id'));
      }
      $v.stop()
        .hide()
        .promise()
        .done(function() {
          $('#' + showViewportId).fadeIn(1000, function() {
            $v.hide();
            if (typeof callback === 'function') {
              callback(callbackData);
            }
          });
        });
    },

    restore: function() {
      if (EW.Portal.Viewports._v.length < 1) {
        EW.Portal.Viewports._v.push('contentViewport');
      }
      $('#content-wrapper')
        .children(':visible')
        .stop()
        .hide()
        .promise()
        .done(function() {
          $('#' + EW.Portal.Viewports._v.pop()).fadeIn(1000);
        });
    },

    clear: function() {
      EW.Portal.Viewports._v = [];
    },
  },
};

// EW.Helpers contains commonly used functions
EW.Helpers = {
  // Return array of string values, or NULL if CSV string not well formed.
  CSVtoArray: function(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = []; // Initialize array to receive values.
    text.replace(
      re_value, // "Walk" the string using replace with callback.
      function(m0, m1, m2, m3) {
        // Remove backslash from \' in single quoted values.
        if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
        // Remove backslash from \" in double quoted values.
        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
        else if (m3 !== undefined) a.push(m3);
        return ''; // Return empty string.
      },
    );
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
  },

  // Safely executes a function represented as a string
  // usage:
  // executeFunctionByName('foo', window, 2, 'test', { prop1: 'value1' });
  // would be the same as:
  // foo(2, 'test', { prop1: 'value1' });
  executeFunctionByName: function(functionName, context) {
    var args = Array.prototype.slice.call(arguments).slice(2),
      namespaces = functionName.split('.'),
      func = namespaces.pop(),
      i = 0;

    for (i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    }

    return context[func].apply(this, args);
  },

  /*
	 * Creates and/or returns a JavaScript namespace.
	 * Usage: 
	 *			var ns = namespace('EW.ServiceItems');
	 *			ns.populateAffecedUserDetails(string);
	 */
  namespace: function(namespaceString) {
    var parts = namespaceString.split('.'),
      parent = window,
      currentPart = '';
    for (var i = 0, length = parts.length; i < length; i++) {
      currentPart = parts[i];
      parent[currentPart] = parent[currentPart] || {};
      parent = parent[currentPart];
    }
    return parent;
  },

  // Query String Helper Function
  querystring: function(key) {
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [],
      m;
    while ((m = re.exec(document.location.search)) != null) r.push(m[1]);
    return r;
  },

  /*
	 * Updates a query string parameter or adds it if missing.
	 * Does not support creating an array in query string. A different approach needed if that functionality is required.
	 */
  updateUrlParameter: function(url, param, paramVal) {
    var newAdditionalURL = '';
    var tempArray = url.split('?');
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = '';
    if (additionalURL) {
      tempArray = additionalURL.split('&');
      for (i = 0; i < tempArray.length; i++)
        if (tempArray[i].split('=')[0] != param) {
          newAdditionalURL += temp + tempArray[i];
          temp = '&';
        }
    }
    var rows_txt = temp + '' + param + '=' + paramVal;
    return baseURL + '?' + newAdditionalURL + rows_txt;
  },

  /*
	 * zero pads a number to a minimum number of characters
	 * EW.Helpers.zeroPad(77, 4)   -> 0077
	 * EW.Helpers.zeroPad(2232, 3) -> 2232    (number longer than 3 characters without padding) 
	 */
  zeroPad: function(n, w) {
    var an = Math.abs(n);
    var digitCount = 1 + Math.floor(Math.log(an) / Math.LN10);
    if (digitCount >= w) {
      return n;
    }
    var zeroString = Math.pow(10, w - digitCount)
      .toString()
      .substr(1);
    return n < 0 ? '-' + zeroString + an : zeroString + an;
  },
};

EW.FileLoader = {
  showFileUploadDialog: function(id) {
    $('body, #contentViewport').scrollTop(0);
    $('body').prepend(
      $('<div></div>')
        .attr('id', 'overlay')
        .addClass('portalOverlay'),
    );

    id = id.replace('ATTACHLOAD_', '');

    $('#fileUploader')
      .fadeIn(200, function() {
        $('#fileUploadForm')
          .children('#CsrfToken')
          .prop('value', KD.utils.Util.getCookie('CsrfToken'));
        $('#fileUploadAttachment')
          .prop('name', id)
          .prop('readonly', null);
        KD.utils.FileLoader.questionObj = $('#SRVQSTN_' + id)[0];
      })
      .position({ my: 'center center', at: 'center center', of: window });
  },

  getFileName: function(file) {
    if (!file) return '';
    var pathSep = '\\';
    if (file.indexOf('\\') == -1) {
      pathSep = '/';
    }
    while (file.indexOf(pathSep) != -1)
      file = file.slice(file.indexOf(pathSep) + 1);
    return file;
  },

  getFileSize: function(file) {
    if (!file) return '';
    return '' + file.fileSize;
  },

  isValidFileType: function(file, fileTypes) {
    if (!file) return false;
    while (file.indexOf('\\') != -1) file = file.slice(file.indexOf('\\') + 1);
    ext = file.slice(file.lastIndexOf('.')).toLowerCase();

    if (fileTypes.indexOf(ext) != -1 || !fileTypes || fileTypes == '')
      return true;

    return false;
  },

  submitFileUpload: function() {
    var KDfileLoader = KD.utils.FileLoader,
      $form = $('#fileUploadForm'),
      $attachObj = $('#fileUploadAttachment'),
      sessionID = encodeURIComponent(KD.utils.ClientManager.sessionId),
      qstnID = $attachObj.attr('name');

    if ($attachObj == null) {
      return null;
    }
    if ($attachObj.val() == null || $attachObj.val() === '') {
      $('#fileUploadResponse').html(
        'Please select a file before trying to upload.',
      );
      return null;
    }

    var valid = true;
    if (KDfileLoader.enforceFileTypes) {
      if (
        !EW.FileLoader.isValidFileType(
          $attachObj.val(),
          KDfileLoader.fileTypesAllowed,
        )
      ) {
        valid = false;
      }
    }
    if (valid) {
      // user shouldn't modify the attachment file at this point
      $attachObj.prop('readonly', 'readonly');

      // submit the form data
      var formData = new FormData(document.getElementById('fileUploadForm'));
      var request = new XMLHttpRequest();
      request.open(
        'POST',
        'UploadFile?sessionID=' + sessionID + '&questionID=' + qstnID,
      );
      request.onreadystatechange = function() {
        if (request.readyState === 4) {
          EW.Portal.hideLoadingImage();
          if (
            request.responseText.indexOf(
              'Your file has successfully been uploaded.',
            ) > -1
          ) {
            $(KDfileLoader.questionObj).val($attachObj[0].files[0].name);
            $(KDfileLoader.questionObj).prop(
              'Attachment',
              $attachObj[0].files[0].name,
            );
            KD.utils.Action._setFileLink(KDfileLoader.questionObj);
            EW.ServiceItems.resizeSectionBoxes();

            $('#fileUploadResponse').html(
              'Your file has successfully been uploaded.',
            );
            window.setTimeout(function() {
              EW.FileLoader.hide();
            }, 500);
          } else {
            $('#fileUploadResponse').html(request.responseText);
          }
        }
      };
      request.send(formData);
      EW.Portal.displayLoadingImage();
    } else {
      $('#fileUploadResponse').html(
        'Please only upload files with the following extensions :\n' +
          fileLoader.fileTypesAllowed,
      );
    }
  },

  hide: function() {
    $('#overlay').remove();
    $('#fileUploader').fadeOut(200, function() {
      EW.FileLoader._updateFilePreview(null);
      $('#CsrfToken').val('');
      $('#fileUploadResponse').empty();
      KD.utils.FileLoader.questionObj = null;
    });
  },

  _updateFilePreview: function(value) {
    if (
      value == null ||
      value === '' ||
      typeof value.files == 'undefined' ||
      value.files.length < 1
    ) {
      document.getElementById('fileUploadAttachment').value = '';
      document.getElementsByClassName('fileUploadPreview')[0].innerHTML =
        'Select an attachment...';
    } else {
      document.getElementsByClassName('fileUploadPreview')[0].innerHTML =
        value.files[0].name;
    }

    $('#fileUploadAttachment').prop('readonly', null);
  },
};
// EW.User contains functions and variables about a user.
// There are two users: logged in and person of interest.
EW.User = {
  LoggedIn: {},
  PersonOfInterest: {},
  setLoggedInUserHomepage: function() {
    /* Add Load Landing Page Function To LoggedIn Object */
    $.extend(EW.User.LoggedIn, {
      loadLandingPage: function() {
        $.ajax({
          method: 'GET',
          url:
            BUNDLE.packagePath +
            'interface/callbacks/MyProfile/userSettings_Settings.jsp',
          success: function(data) {
            data = JSON.parse(data);

            var landingPage = data.landingpages.current;

            if (landingPage === 'myProfile') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-myprofile").click();',
              );
              EW.Modules.MyProfile.load();
            }
            if (landingPage === 'Dashboard') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-dashboard").click();',
              );
              EW.Modules.Dashboard.load();
            }
            if (landingPage === 'Reporting') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-reports").click();',
              );
              EW.Modules.Reports.load();
            }
            if (landingPage === 'eFactory') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-efactory").click();',
              );
              EW.Modules.EFactory.load();
            }
            if (landingPage === 'Asset Management') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-assets").click();',
              );
              EW.Modules.Assets.load();
            }
            if (landingPage === 'Process Owner') {
              $('#masthead-logo').attr(
                'onclick',
                '$("#masthead-processowner").click();',
              );
              EW.Modules.ProcessOwner.load();
            }

            /* Open Menu If User's Menu State Prefernece is Set to Expand */
            $.extend(EW.User.LoggedIn, { timezone: jstz.determine().name() });
            $.extend(EW.User.LoggedIn, { menustate: data.menustate });
            $.extend(EW.User.LoggedIn, { tempunit: data.tempunit });
            $.extend(EW.User.LoggedIn, { dateformat: data.dateformat });
            if (EW.User.LoggedIn.menustate === 'expand') {
              $('#myE-menu').trigger('open.mm');
            }
            $.extend(EW.User.LoggedIn, { defaultrows: data.defaultrows });
            $.extend(EW.User.LoggedIn, {
              defaultrowsInt:
                data.defaultrows === 'Show All'
                  ? -1
                  : parseInt(
                      EW.User.LoggedIn.defaultrows.split('Show ')[1],
                      10,
                    ),
            });
            $.extend(EW.User.LoggedIn, { tooltips: data.tooltips });
            if (EW.User.LoggedIn.tooltips === 'show') {
              $(
                '#masthead-menu-ul > li, #masthead-more-items > li, #masthead-user-image, .masthead-search-container, .menu-icon-link, .myE-leftnav-quicklink, #assistMe',
              ).tooltipster();
            } else {
              $(
                '#masthead-menu-ul > li, #masthead-more-items > li, #masthead-user-image, .masthead-search-container, .menu-icon-link, .myE-leftnav-quicklink, #assistMe',
              ).attr('title', '');
            }
          },
        });
      },
    });

    // Test querystring parameters for possible redirection
    if (EW.Helpers.querystring('r').length > 0) {
      $.ajax({
        method: 'GET',
        url:
          BUNDLE.packagePath +
          'interface/callbacks/MyProfile/Submission_StatusCheck.jsp',
        data: { r: EW.Helpers.querystring('r')[0] },
        success: function(data) {
          if (data.found) {
            var state = data.state,
              type = data.type === 'approval' ? 'actions' : 'requests',
              arrayParam = ['MyRequests', state + ' ' + type, data.id];

            EW.Modules.MyProfile.load(arrayParam);
          } else {
            // Did not find the request. Load landing page as default action.
            EW.User.LoggedIn.loadLandingPage();
          }
        },
      });
    } else if (EW.Helpers.querystring('s').length > 0) {
      // Load the user's landing page
      EW.User.LoggedIn.loadLandingPage();

      // Load service item
      EW.ServiceItems.open(
        '/kinetic/DisplayPage?srv=' + EW.Helpers.querystring('s')[0],
      );
    } else if (EW.Helpers.querystring('aid').length > 0) {
      // Load the user's landing page
      EW.User.LoggedIn.loadLandingPage();

      EW.Portal.showModalOverlay(
        BUNDLE.packagePath + 'interface/fragments/Omnibox/Article_FullView.jsp',
        {
          id: EW.Helpers.querystring('aid')[0],
          form: EW.Helpers.querystring('form')[0],
        },
        $.noop,
      );
    } else if (EW.Helpers.querystring('k').length > 0) {
      // Load the user's landing page
      EW.User.LoggedIn.loadLandingPage();

      // TO DO:
      // View submission
    } else if (EW.Helpers.querystring('i').length > 0) {
      // Load the user's landing page
      EW.User.LoggedIn.loadLandingPage();

      // TO DO:
      // View incident
    } else {
      // Load the user's landing page
      EW.User.LoggedIn.loadLandingPage();
    }
  },
};

// EW.ServiceItem contains functions and variables used to load
// and work with service items.
EW.ServiceItems = {
  //These Variables Are Used For Kinetics iFrame Override
  pageCacheItems: {},
  pageCustomEvents: {},
  pageSubmitButtons: {},
  panelDescriptions: [],
  //This Variable Is Used For Keeping Track Of The Callback URL
  serviceItemToOpenURL: '',
  //This Variable Is Used For The Slider And Is Equivalent to the Total Width of a Section (Div Width + Borders)
  serviceItemWidthForSlider: 560 + 6,
  //This Variable Is Used For The Slider And Is Equivalent to the Left Padding That Is To Be Added To Preceding Sections (Not The First)
  serviceItemLeftMarginForSlider: 30,

  //Interface Protection Variables
  serviceItemOpen: false,
  openServiceItemWarningTitle: 'Unsaved Service Request',
  openServiceItemWarningText:
    'You are viewing a service request. If you leave this page before you submit the form your changes will be lost. Do you wish to leave this service request?',
  openApprovalWarningTitle: 'Unsaved Approval',
  openApprovalWarningText:
    'You are viewing an approval for service. If you leave this page before you submit the form your changes will be lost. Do you wish to leave this approval request?',

  //Adds Listeners To Elements
  wireup: function() {
    $('#masthead').on('click', EW.ServiceItems.close);

    //Monitor Text Input and Textareas For Keyup Events Inside Service Item
    $('#siViewport').on(
      'keyup',
      '#si-container input[type=text], textarea',
      function() {
        EW.ServiceItems.setOpenServiceItemWarning();
      },
    );

    $('#siViewport').on(
      'change',
      '#si-container input[type=checkbox], select',
      function() {
        EW.ServiceItems.setOpenServiceItemWarning();
      },
    );

    $(window).on('resize', $.debounce(250, EW.ServiceItems.resize));
  },

  // perform any service item related screen resizing tasks here.
  resize: function() {},

  //Get URL For Callback From Element And Check For Interface Protection or Call Load Event
  open: function(url) {
    if ($('#obsViewport').is(':visible')) {
      $('#content-wrapper').scrollTop(0);
      $('#obsSearchInput').val('');
      $('#obsViewport').hide();
      $(
        '#masthead-logo, #masthead-menu-ul > li, #masthead-more-items > li, span[data-action]',
      )
        .not('#masthead-menu-more')
        .off('click', EW.Modules.Omnibox.close)
        .off('click', EW.Portal.CrumbView.hide);
    }

    if (EW.ServiceItems.serviceItemToOpenURL === '') {
      if (typeof url === 'string') {
        url = url.split('kinetic/')[1];
      } else {
        url = url[0].split('kinetic/')[1];
      }
      // If user is _not_ specifed on the query string add the POI Remedy Id
      if (url.split('&u=').length === 1) {
        url += '&u=' + EW.User.PersonOfInterest.remedyid;
      }

      // If compnay is _not_ specified on the query string add the POI company
      if (url.split('&c=').length === 1) {
        url += '&c=' + EW.User.PersonOfInterest.company;
      }

      EW.ServiceItems.serviceItemToOpenURL = url;
    }

    if (EW.ServiceItems.serviceItemOpen) {
      EW.ServiceItems.showInterfaceProtectionWarning(EW.ServiceItems.open);
    } else {
      EW.Portal.displayLoadingImage();
      EW.ServiceItems.load(
        EW.ServiceItems.serviceItemToOpenURL,
        EW.ServiceItems.loadDataComplete,
      );
    }
  },

  //Load Service Item JSP
  load: function(url, callback) {
    EW.Portal.extendSession();

    var timeNow = new Date().getTime();

    if (typeof affectedUserChange === 'function') {
      affectedUserChange = undefined;
    }
    if (typeof load === 'function') {
      load = undefined;
    }

    $.ajax({
      cache: false,
      type: 'GET',
      url: url + '&_' + timeNow,
      success: function(data, textStatus, jqXHR) {
        var pageId = $('#pageID').val();
        EW.ServiceItems.removeFormContext();
        $('#content-wrapper').scrollTop(0);
        $('#siViewport').html(data);
        EW.Portal.Viewports.swap('siViewport', callback);
        /* Hide until reformat finishes */
        $('#siViewport').hide();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status !== 401) {
          $('#si-container').html('Could not load description.');
        }
      },
    });
  },

  //Load Event Callback
  loadDataComplete: function() {
    //Set Variables Used In Load Callback
    var pageId = $('#pageID').val();
    var requestId = $('#surveyRequestID').val();
    KD.utils.ClientManager.submitButtons.length = 0;
    KD.utils.ClientManager.templateId = $('#templateID').val();

    //Override For Date Fields
    override_initDateFields = function() {
      var yearEls = KD.utils.Util.getElementsByClassName(
          'dateYear',
          'input',
          $('#PAGE_' + pageId)[0],
        ),
        instId;

      for (var i = 0; i < yearEls.length; i++) {
        instId = yearEls[i].id.substring(5);
        if (instId) {
          KD.utils.Action.setDateFields(instId);
        }
      }

      yearEls = KD.utils.Util.getElementsByClassName(
        'dtYear',
        'input',
        $('#PAGE_' + pageId)[0],
      );

      for (i = 0; i < yearEls.length; i++) {
        instId = yearEls[i].id.substring(5);
        if (instId) {
          KD.utils.Action.setDateTimeFields(instId);
        }
      }
    };

    //Override initialization of Date Fields
    orig_initDateFields = KD.utils.ClientManager.initDateFields;
    KD.utils.ClientManager.initDateFields = override_initDateFields;

    //Initialize Client Manager and Set Variables for Submit
    KD.utils.ClientManager.init();
    EW.ServiceItems.pageCacheItems[pageId] = KD.utils.Util.labelIDCache;
    EW.ServiceItems.pageSubmitButtons[pageId] = $.extend(
      [],
      KD.utils.ClientManager.submitButtons,
    );

    //Set Date Fields Back To Regular Initialization
    KD.utils.ClientManager.initDateFields = orig_initDateFields;

    //Override Attachment Load Functionality

    $('input[id*="ATTACHLOAD_"]').attr(
      'onclick',
      'EW.FileLoader.showFileUploadDialog($(this).attr("id"));',
    );

    //Button Overrides
    $('#PAGE_' + pageId)
      .parent()
      .eq(0)
      .prop('action', 'javascript:EW.ServiceItems.serviceItemSubmit();');
    $('#PAGE_' + pageId + " input[name='Previous']")
      .eq(0)
      .unbind('click');
    $('#PAGE_' + pageId + " input[name='Previous']").attr(
      'onclick',
      "javascript:EW.ServiceItems.loadItemPreviousPage('" + pageId + "');",
    );

    //Extend Events
    clientManager.templateId = $('#templateID').val();
    var actionsInit = window[pageId + '_ks_actionsInit'];
    if (typeof actionsInit === 'function') {
      actionsInit();
    }
    if (EW.ServiceItems.pageCustomEvents[pageId] === undefined) {
      EW.ServiceItems.pageCustomEvents[pageId] = $.extend(
        {},
        KD.utils.ClientManager.customEvents,
      );
    } else {
      KD.utils.ClientManager.customEvents =
        EW.ServiceItems.pageCustomEvents[pageId];
    }

    //Call Other Supplimental Functions After Load
    EW.ServiceItems.handleGoToReview();
    EW.ServiceItems.restoreFormContext(pageId);
    $('.centerPanelLoader').remove();
    $('#content-wrapper').scrollTop(0);

    //Clear URL
    EW.ServiceItems.serviceItemToOpenURL = '';

    //Load Title Bar Content
    $('#si-title').html($('#si-container .templateContent').attr('label'));

    //Default Value For Dropdown if searches are allowed
    $('#requestedForSelector')
      .val(ServiceItem.AffectedUser.personid)
      .select2({
        width: '250px',
        placeholder: 'Search for a Person..',
        minimumInputLength: 2,
        id: function(user) {
          return user.peopleid;
        },
        ajax: {
          url:
            BUNDLE.packagePath +
            '/interface/callbacks/ServiceItem/UserSearch.jsp?restricted=true&company=' +
            ServiceItem.AffectedUser.company,
          dataType: 'json',
          quietMillis: 250,
          data: function(searchterm, page) {
            return {
              term: searchterm, // search term
            };
          },
          results: function(users, page) {
            return { results: users.employees };
          },
          cache: true,
        },
        initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== '') {
            var data = { id: element.val(), text: element.val() };
            $.ajax(
              BUNDLE.packagePath +
                '/interface/callbacks/ServiceItem/UserSearch.jsp?restricted=true&company=' +
                ServiceItem.AffectedUser.company +
                '&term=' +
                id,
              {
                dataType: 'json',
              },
            ).done(function(data) {
              callback(data.employees[0]);
              EW.ServiceItems.affectedUserChanged(data.employees[0].value);
            });
          }
        },
        formatResult: EW.ServiceItems.userFormatResult,
        formatSelection: EW.ServiceItems.userFormatSelection,
        dropdownCssClass: 'si-select-results',
        escapeMarkup: function(m) {
          return m;
        },
      })
      .on('change', EW.ServiceItems.affectedUserChanged);

    $.ajax({
      url:
        BUNDLE.packagePath +
        '/interface/callbacks/ServiceItem/TemplateInformation.jsp?t=' +
        clientManager.templateId,
      method: 'GET',
      success: function(data) {
        if (data.noSlider === 'false') {
          EW.ServiceItems.reformatServiceItem();
        } else {
          EW.ServiceItems.reformatServiceItemWithoutSlider();
        }
      },
    });

    /* Put help text in as a placeholder attribute for the question answer field */
    $('.questionLayer[title]')
      .not('.questionLayer[title=""]')
      .each(function() {
        if (
          $(this).find('input[type="text"], input[type="textarea"]').length > 0
        ) {
          $(this)
            .find('input[type="text"], input[type="textarea"]')
            .attr('placeholder', $(this).attr('title'));
        }
      });
  },

  reformatServiceItemWithoutSlider: function() {
    var sectionCount = 0;

    $('.si-section-header').each(function() {
      var $titleDiv = $(this)
        .children()
        .first();
      var $section = $.merge($(this), $(this).next());
      sectionCount = sectionCount + 1;

      $titleDiv.addClass('si-section-title');
    });

    /* Insert Submit Section */
    var $lastSection = $('.si-section').last();
    $(
      '<div id="Submit"><div class="si-section" style="min-height:200px;"><div style="border-top: 1px solid #898989; text-align: center; padding: 20px;">' +
        $("[label*='Submit Section Text']").text() +
        '</div><div class="si-submit-buttons"></div></div></div>',
    ).insertAfter($lastSection);

    $('.si-wrapper .submitButton')
      .children()
      .addClass('mye-icon-sicheck si-submit-button')
      .val('');
    $('.si-wrapper .submitButton').appendTo('.si-submit-buttons');
    $(
      '<div class="mye-icon-siclose si-cancel-button" onclick="EW.ServiceItems.close();"></div>',
    ).appendTo('.si-submit-buttons');

    $('#siViewport').show();
    if (typeof load === 'function') {
      load();
    }
    EW.Portal.hideLoadingImage();
  },

  userFormatResult: function(user) {
    var resultHtml =
      '<div class="si-search-result-container">' +
      '<div class="si-image-container-search">' +
      '<img id="si-person-image" class="si-image-image-search" alt="No Images Found..." onError="this.onerror = null; this.src=\'' +
      BUNDLE.packagePath +
      'resources/images/common/missingPerson.png\';"' +
      ' src="DownloadAttachment/CTM:People/1000003962/' +
      user.peopleid +
      '?disposition=inline" />' +
      '</div>' +
      '<div class="si-search-result-text-container">' +
      '<div class="si-search-result-text">' +
      '<div class="requestedFor-fullName">' +
      user.fullname +
      '</div>' +
      '<div class="requestedFor-otherInfo">' +
      user.jobtitle +
      '</div>' +
      '</div>';

    resultHtml += '</div></div>';

    return resultHtml;
  },

  userFormatSelection: function(user) {
    return user.fullname;
  },

  //Kinetic Provided Function For Handling Service Items Without iFrames
  restoreFormContext: function(pageId) {
    EW.ServiceItems.removeFormContext();

    $('#' + pageId + '-pageQuestionsForm').attr('id', 'pageQuestionsForm');
    $('#' + pageId + '-templateID').attr('id', 'templateID');
    $('#' + pageId + '-surveyRequestID').attr('id', 'surveyRequestID');
    $('#' + pageId + '-pageID').attr('id', 'pageID');
    $('#' + pageId + '-sessionID').attr('id', 'sessionID');
    clientManager.templateId = $('#templateID').val();

    KD.utils.Util.labelIDCache = $.extend(
      KD.utils.Util.labelIDCache,
      EW.ServiceItems.pageCacheItems[pageId],
    );
    KD.utils.ClientManager.submitButtons =
      EW.ServiceItems.pageSubmitButtons[pageId];
    KD.utils.ClientManager.customEvents =
      EW.ServiceItems.pageCustomEvents[pageId];
  },

  //Kinetic Provided Function For Handling Service Items Without iFrames
  removeFormContext: function() {
    var id = $('#pageID').val();
    $('#pageQuestionsForm').attr('id', id + '-pageQuestionsForm');
    $('#templateID').attr('id', id + '-templateID');
    $('#surveyRequestID').attr('id', id + '-surveyRequestID');
    $('#pageID').attr('id', id + '-pageID');
    $('#sessionID').attr('id', id + '-sessionID');
  },

  //Called When Affected User Changes
  affectedUserChanged: function() {
    var personId = $('#requestedForSelector').val();
    $('.si-image-container').html(
      '<img id="si-person-image" class="si-image-image" alt="No Images Found..." onError="this.onerror = null; this.src=\'' +
        BUNDLE.packagePath +
        'resources/images/common/missingPerson.png\';"' +
        ' src="DownloadAttachment/CTM:People/1000003962/' +
        personId +
        '?disposition=inline"></img>',
    );

    $.ajax({
      url:
        BUNDLE.packagePath +
        '/interface/callbacks/ServiceItem/User.jsp?p=' +
        personId,
      method: 'GET',
      success: function(data) {
        if (typeof data !== 'undefined' && data.length > 0) {
          data = JSON.parse(data);
          if (ServiceItem.AffectedUser.remedyid !== data.remedyid) {
            ServiceItem.AffectedUser = data;
            EW.ServiceItems.setBaseAffectedUserValues();
            if (typeof affectedUserChange === 'function') {
              affectedUserChange();
            }
          } else {
            ServiceItem.AffectedUser = data;
            EW.ServiceItems.setBaseAffectedUserValues();
          }
        }
      },
    });
  },

  setBaseAffectedUserValues: function() {
    if (
      $(
        KD.utils.Util.getElementObject(
          'Param-AffectedUserRemedyId',
          'SRVQSTN_',
        ),
      ).length > 0
    ) {
      KD.utils.Action.setQuestionValue(
        'Param-AffectedUserRemedyId',
        ServiceItem.AffectedUser.remedyid,
      );
    }
    if (
      $(KD.utils.Util.getElementObject('Param-AffectedUserCompany', 'SRVQSTN_'))
        .length > 0
    ) {
      KD.utils.Action.setQuestionValue(
        'Param-AffectedUserCompany',
        ServiceItem.AffectedUser.company,
      );
    }
    if (
      $(KD.utils.Util.getElementObject('Param-AffectedUserName', 'SRVQSTN_'))
        .length > 0
    ) {
      KD.utils.Action.setQuestionValue(
        'Param-AffectedUserName',
        ServiceItem.AffectedUser.fullname,
      );
    }
  },

  //Submit Override Function
  serviceItemSubmit: function() {
    EW.Portal.extendSession();
    var url = BUNDLE.applicationPath + 'SubmitPage';

    // Get submit button on page - getting from the pageSubmitButtons cache to handle any button text.
    var el = KD.utils.Util.getButtonObject(
      EW.ServiceItems.pageSubmitButtons[$('#pageID').val()][0],
    );

    // Remove the button from view
    if (el !== null) {
      KD.utils.Action.removeElement(el.id);
      if ($('.si-submit-buttons').length > 0) {
        $('.si-submit-buttons').hide();
      }
    }

    // Scroll to top of wrapper so the loader message is visible.
    $('#content-wrapper').scrollTop(0);

    //Final Ajax Call For Posting Answers
    EW.Portal.displayLoadingImage();

    $.ajax({
      url: url + '?srv=' + $('#templateID').val(),
      data: $('#pageQuestionsForm').serialize(),
      type: 'POST',
      success: function(data) {
        $('#si-container')
          .fadeIn(100)
          .html(data);

        $('#si-nav-inner, #si-title-edit-button').remove();
        if ($('#requestedForSelector')) {
          $('#requestedForSelector').prop('disabled', true);
        }

        KD.utils.ClientManager.isSubmitting = false;
      },
    }).always(EW.Portal.hideLoadingImage);
  },

  //Kinetic Provided Function For Handling Service Items Without iFrames
  loadItemPreviousPage: function(pageId) {
    form = $('#' + pageId + '-pageQuestionsForm');
    if (form.length === 0) {
      form = $('#pageQuestionsForm');
    }

    $.ajax({
      url: BUNDLE.applicationPath + 'PreviousPage',
      data: form.serialize(),
      type: 'POST',
      success: function(data) {
        $('#siViewport').html(data);
        EW.ServiceItems.loadDataComplete(
          $('.name.navigation .active').attr('data-id'),
        );
      },
    });
  },

  //Kinetic Provided Function For Handling Service Items Without iFrames
  handlePrevious: function() {
    $.ajax({
      url: BUNDLE.applicationPath + 'PreviousPage',
      data: $('#pageQuestionsForm').serialize(),
      type: 'POST',
      success: function(data) {
        $('#service-item-container').html(data);
        EnterpriseWatch.ServiceItem.loadDataComplete(
          $('.name.navigation .active').attr('data-id'),
        );
      },
    });
  },

  //Kinetic Provided Function For Handling Service Items Without iFrames
  handleGoToReview: function() {
    if ($('.SI_InComplete').length > 0) {
      if ($('input[value="Continue"]').length > 0) {
        KD.utils.Action.disableButton('Continue');
      }
    } else {
      if ($('input[value="Continue"]').length > 0) {
        KD.utils.Action.enableButton('Continue');
      }
    }
  },

  close: function() {
    //Check Interface Protection
    if (EW.ServiceItems.serviceItemOpen === true) {
      EW.ServiceItems.showInterfaceProtectionWarning(function() {
        EW.ServiceItems.close();
      });
      return false;
    }

    if (typeof load === 'function') {
      load = undefined;
    }

    //If No Warning Go Ahead and Leave
    EW.ServiceItems.clearOpenServiceItemWarning();
    if ($('#siViewport, #modalViewport').is(':visible')) {
      $('#content-wrapper').scrollTop(0);
      $('#siViewport').html('');
      if ($('#modalViewport').is(':visible')) {
        EW.Portal.hideModalOverlay();
      } else {
        EW.Portal.Viewports.restore();
      }
    }
  },

  setOpenServiceItemWarning: function() {
    EW.ServiceItems.serviceItemOpen = true;
  },

  clearOpenServiceItemWarning: function() {
    EW.ServiceItems.serviceItemOpen = false;
  },

  //Call Dialog For Interface Protection
  showInterfaceProtectionWarning: function(callback, callbackData) {
    var winH = $(window).height(),
      winW = $(window).width(),
      $dialog = null;

    if ($('#siViewport').is(':visible')) {
      $dialog = $('#serviceItemWarningDialog');
    } else {
      $dialog = $('#approvalWarningDialog');
    }

    if ($('#overlay').length < 1) {
      $('body').prepend(
        $('<div></div>')
          .attr('id', 'overlay')
          .addClass('portalOverlay'),
      );
    }

    $dialog
      .show()
      .position({ my: 'center center', at: 'center center', of: window });

    $('#' + $dialog.attr('id') + ' .portal-dialog-leave-button')
      .off('click')
      .one('click', function() {
        if (
          typeof callbackData != 'undefined' &&
          typeof callback != 'undefined'
        ) {
          EW.ServiceItems.processInterfaceProtectionLeave(
            callback,
            callbackData,
          );
        } else {
          EW.ServiceItems.processInterfaceProtectionLeave(callback);
        }
      });

    $(document).on('keypress', function(e) {
      if (e.which === 13) {
        EW.ServiceItems.hideInterfaceProtectionWarning();
      }
    });
  },

  //Go Ahead And Leave As The User Decided To Continue
  processInterfaceProtectionLeave: function(callback, callbackData) {
    EW.ServiceItems.clearOpenServiceItemWarning();
    EW.ServiceItems.hideInterfaceProtectionWarning();

    if (typeof callback === 'function') {
      callback();
    }
    if (typeof callback === 'string') {
      EW.ServiceItems.close();
      EW.Helpers.executeFunctionByName(callback, window, callbackData);
    }
  },

  //If The User Chooses To Cancel
  hideInterfaceProtectionWarning: function() {
    if ($('#siViewport').is(':visible')) {
      $('#serviceItemWarningDialog').hide();
    } else {
      $('#approvalWarningDialog').hide();
    }

    $('#overlay').remove();
    $(document).off('keypress');
  },

  /* Handles Wrapping Div Sections and Moving Submit Button */
  reformatServiceItem: function() {
    var sectionCount = 0;

    $('.si-section-header').each(function() {
      var $titleDiv = $(this)
        .children()
        .first();
      var helpText = $(this)
        .children()
        .last()
        .text();
      var $section = $.merge($(this), $(this).next());
      sectionCount = sectionCount + 1;

      $titleDiv.addClass('si-section-title');
      $section.wrapAll(
        '<div id="' +
          $titleDiv.text().replace(/\s/g, '_') +
          '" class="si-section-outer ' +
          (sectionCount === 1 ? 'si-section-outer-selected' : '') +
          '" mye-section-counter="' +
          sectionCount +
          '"></div>',
      );
      if (helpText !== '') {
        $(this).prepend(
          '<div class="si-section-help" title="' + helpText + '">?</div>',
        );
      }
      $(this).prepend(
        '<div class="si-sectionNumber-outer"><div id="' +
          sectionCount +
          '" class="si-sectionNumber">' +
          sectionCount +
          '</div></div>',
      );
    });

    /* Insert Submit Section */
    var $lastSection = $('.si-section-outer').last();
    $(
      '<div id="Submit" class="si-section-outer mye-30-left-margin" mye-section-counter="' +
        (sectionCount + 1) +
        '"><div class="si-section-header"><div class="si-section-title">Submit</div></div><div class="si-section"><div class="si-submit-section-text">' +
        $("[label*='Submit Section Text']").text() +
        '</div><div class="si-submit-buttons"></div></div></div>',
    ).insertAfter($lastSection);

    $('.si-wrapper .submitButton')
      .children()
      .addClass('mye-icon-sicheck si-submit-button')
      .val('');
    $('.si-wrapper .submitButton').appendTo('.si-submit-buttons');
    $(
      '<div class="mye-icon-siclose si-cancel-button" onclick="EW.ServiceItems.close();"></div>',
    ).appendTo('.si-submit-buttons');

    $('.mye-icon-siclose, .mye-icon-siclosefill').hover(
      function() {
        $(this)
          .addClass('mye-icon-siclosefill')
          .removeClass('mye-icon-siclose');
      },
      function() {
        $(this)
          .addClass('mye-icon-siclose')
          .removeClass('mye-icon-siclosefill');
      },
    );

    EW.ServiceItems.resizeSectionBoxes();
    EW.ServiceItems.initializeSlider();
  },

  /* Resize Slider Elements And Slider Wrapper Height */
  resizeSectionBoxes: function() {
    var maxHeight = 0;
    $('.si-section').each(function() {
      var height = $(this).height();
      if (height > maxHeight) {
        maxHeight = height;
      }

      if (
        $(this)
          .parent()
          .index() > 0
      ) {
        $(this)
          .parent()
          .addClass('mye-30-left-margin');
      }
    });

    /* Resize Elements Based On Height */
    if (maxHeight < 200) {
      maxHeight = 200;
    }

    $('.si-section').css('min-height', maxHeight + 'px');

    if ($('.sliderWrapper')) {
      $('.sliderWrapper').height(
        $('.si-section-outer')
          .eq(0)
          .outerHeight(),
      );
    }
    if ($('.si-slider')) {
      $('.si-slider').height(
        $('.si-section-outer')
          .eq(0)
          .outerHeight(),
      );
    }
    if ($('.si-slider-left').length > 0) {
      $('.mye-icon-siprev').position({
        my: 'center center',
        at: 'center center',
        of: '.si-slider-left',
      });
    }
    if ($('.si-slider-right').length > 0) {
      $('.mye-icon-sinext').position({
        my: 'center center',
        at: 'center center',
        of: '.si-slider-right',
      });
    }
  },

  /* Following Three Functions Handle The Editing of Service Item Descriptions */
  openEditDescription: function() {
    $('#si-title').hide();
    $('#si-title-edit-button').hide();
    $('#si-title-edit-section').show();
    $('#si-title-edit-input').focus();
  },

  closeEditDescription: function() {
    $('#si-title-edit-section').hide();
    $('#si-title').show();
    $('#si-title-edit-button').show();

    $('#si-title-edit-input').val('');
  },

  saveEditDescription: function() {
    $('#si-title-edit-section').hide();
    $('#si-title').show();
    $('#si-title-edit-button').show();

    var $description = $('#si-title-edit-input').val();

    if ($description.length > 0) {
      $('#si-title').text($description);
      KD.utils.Action.setQuestionValue('InstanceName', $description);
      $('#si-title-edit-input').val('');
    } else {
      return;
    }
  },

  /* Initialize The Service Item Slider and Nav Bar */
  initializeSlider: function() {
    $('#siViewport').show();
    $('.si-section-help').tooltipster({
      contentAsHTML: true,
      position: ['bottom'],
    });
    /* Create Nav Bar */
    $('<span style="float:left; display:inline-block; color: #707070;"></span>')
      .prependTo('#si-nav-inner')
      .html('Step: ');

    var number = 0;
    $('.si-section').each(function() {
      number = number + 1;

      $(
        '<div id="' +
          number +
          '" class="si-page-number ' +
          (number === 1 ? 'si-page-number-selected' : '') +
          '">' +
          number +
          '</div>',
      ).appendTo('#si-nav-inner');
    });

    /* Append Slider Sections to SI Page */
    $('.si-page')
      .prepend(
        '<div id="si-slider-left" class="si-slider"><div class="mye-icon-siprev"></div></div>',
      )
      .append(
        '<div id="si-slider-right" class="si-slider si-slider-right"><div class="mye-icon-sinext"></div></div>',
      );
    $('.si-slider').height(
      $('.si-section-outer')
        .eq(0)
        .outerHeight(),
    );

    /* Apply Master Slider Div Around Section Divs */
    $('.si-section-outer').wrapAll(
      '<div class="sliderWrapper"><div class="sliderOuter"></div></div>',
    );
    $('.sliderWrapper').height(
      $('.si-section-outer')
        .eq(0)
        .outerHeight(),
    );
    $('.sliderOuter').width(
      EW.ServiceItems.serviceItemWidthForSlider * number +
        EW.ServiceItems.serviceItemLeftMarginForSlider * (number - 1),
    );

    /* Add Click Handlers to Various Components */
    $('.si-page').on(
      'click',
      '.si-slider-right',
      EW.ServiceItems.sliderNextPage,
    );
    $('.si-page').on(
      'click',
      '.si-slider-left',
      EW.ServiceItems.sliderPreviousPage,
    );
    $('.si-nav-outer').on('click', '.si-page-number', function() {
      EW.ServiceItems.sliderNavToSection($(this).attr('id'));
    });
    $('.si-section-outer')
      .not('#Submit')
      .on('click focusin keydown', function(e) {
        /* Prevent Multi-select fields from opening into the slider is finished if they selected one */
        var $element = $('#' + e.target.id),
          $parent = $element
            .parents('.questionAnswer')
            .find('select[multiple="multiple"]');
        if (
          $parent.length > 0 &&
          !$(this).hasClass('si-section-outer-selected')
        ) {
          $parent.one('select2-opening', function(e) {
            e.preventDefault();
          });
          EW.ServiceItems.sliderNavToSection(
            parseInt($(this).attr('mye-section-counter'), 10),
            function() {
              $parent.select2('open');
            },
          );
        } else {
          EW.ServiceItems.sliderNavToSection(
            parseInt($(this).attr('mye-section-counter'), 10),
          );
        }

        EW.ServiceItems.resizeSectionBoxes();
        EW.ServiceItems.verifyRequiredFieldsAndShowSubmitButton();
      });

    /* Add Resize Event Listener to Window */
    $(window).on('resize', EW.ServiceItems.sliderResize);

    EW.ServiceItems.sliderShowHideIcons();
    EW.ServiceItems.sliderResize();

    /* Call Custom Service Item JS Now That Slider Has Been Initialized */
    if (typeof load === 'function') {
      load();
      EW.ServiceItems.verifyRequiredFieldsAndShowSubmitButton();
    }

    EW.ServiceItems.resizeSectionBoxes();
    EW.Portal.hideLoadingImage();
  },

  /* Handles Sliding Right */
  sliderNextPage: function() {
    /* Get Necessary Indexes */
    var currentIndex = EW.ServiceItems.sliderGetCurrentIndex();
    var nextIndex = EW.ServiceItems.sliderGetCurrentIndex() + 1;

    /* Change Selected Nav Option */
    EW.ServiceItems.sliderCurrentPanelIsActive(nextIndex);
    EW.ServiceItems.sliderShowHideIcons();

    /* Animate --> All SI Containers are 566px Wide and All others except for the first have 40px Added on the left */
    $('.sliderOuter').animate(
      {
        left:
          '-=' +
          (EW.ServiceItems.serviceItemWidthForSlider +
            EW.ServiceItems.serviceItemLeftMarginForSlider),
      },
      {
        duration: 800,
        easing: 'swing',
        complete: function() {
          if (typeof callback === 'function') {
            callback();
          }
        },
      },
    );
  },

  /* Handles Sliding Left */
  sliderPreviousPage: function(callback) {
    /* Get Necessary Indexes */
    var currentIndex = EW.ServiceItems.sliderGetCurrentIndex();
    var prevIndex = EW.ServiceItems.sliderGetCurrentIndex() - 1;

    /* Change Selected Nav Option */
    EW.ServiceItems.sliderCurrentPanelIsActive(prevIndex);
    EW.ServiceItems.sliderShowHideIcons();

    /* Animate --> All SI Containers are 566px Wide and All others except for the first have 40px Added on the left */
    $('.sliderOuter').animate(
      {
        left:
          '+=' +
          (EW.ServiceItems.serviceItemWidthForSlider +
            EW.ServiceItems.serviceItemLeftMarginForSlider),
      },
      {
        duration: 800,
        easing: 'swing',
        complete: function() {
          if (typeof callback === 'function') {
            callback();
          }
        },
      },
    );
  },

  /* Go to a Specific Section Number (Not Index Number) */
  sliderNavToSection: function(sectionNumber, callback) {
    /* Get Necessary Indexes */
    var currentIndex = EW.ServiceItems.sliderGetCurrentIndex(),
      desiredIndex = parseInt(sectionNumber, 10) - 1,
      difference = 0;

    if (currentIndex == desiredIndex) {
      return;
    }

    /* Change Selected Nav Option */
    EW.ServiceItems.sliderCurrentPanelIsActive(desiredIndex);
    EW.ServiceItems.sliderShowHideIcons();

    if (currentIndex < desiredIndex) {
      difference = desiredIndex - currentIndex;
      $('.sliderOuter').animate(
        {
          left:
            '-=' +
            (EW.ServiceItems.serviceItemWidthForSlider +
              EW.ServiceItems.serviceItemLeftMarginForSlider) *
              difference,
        },
        {
          duration: 800,
          easing: 'swing',
          complete: function() {
            if (typeof callback === 'function') {
              callback();
            }
          },
        },
      );
    }

    if (currentIndex > desiredIndex) {
      difference = currentIndex - desiredIndex;
      $('.sliderOuter').animate(
        {
          left:
            '+=' +
            (EW.ServiceItems.serviceItemWidthForSlider +
              EW.ServiceItems.serviceItemLeftMarginForSlider) *
              difference,
        },
        {
          duration: 800,
          easing: 'swing',
          complete: function() {
            if (typeof callback === 'function') {
              callback();
            }
          },
        },
      );
    }
  },

  /* This will Hide and Show the Left and Right Slider Icons */
  sliderShowHideIcons: function() {
    var currentIndex = EW.ServiceItems.sliderGetCurrentIndex();
    var maxIndex = $('.si-section').length - 1;

    if (currentIndex === 0) {
      $('#si-slider-left')
        .removeClass('si-slider-left')
        .children()
        .hide();
    } else {
      $('#si-slider-left')
        .addClass('si-slider-left')
        .children()
        .show();
    }

    if (currentIndex == maxIndex) {
      $('#si-slider-right')
        .removeClass('si-slider-right')
        .children()
        .hide();
    } else {
      $('#si-slider-right')
        .addClass('si-slider-right')
        .children()
        .show();
    }

    if ($('.si-slider-left').length > 0) {
      $('.mye-icon-siprev').position({
        my: 'center center',
        at: 'center center',
        of: '.si-slider-left',
      });
    }
    if ($('.si-slider-right').length > 0) {
      $('.mye-icon-sinext').position({
        my: 'center center',
        at: 'center center',
        of: '.si-slider-right',
      });
    }
  },

  /* Adds Active Classes to the new selected section */
  sliderCurrentPanelIsActive: function(newIndex) {
    /* Change Selected Nav Option */
    $('.si-page-number')
      .removeClass('si-page-number-selected')
      .eq(newIndex)
      .addClass('si-page-number-selected');
    $('.si-section-outer')
      .removeClass('si-section-outer-selected')
      .eq(newIndex)
      .addClass('si-section-outer-selected');
  },

  /* Get The Index of the Currently Selected Section */
  sliderGetCurrentIndex: function() {
    var number = 0;

    if ($('#si-nav-outer').find('.si-page-number-selected').length > 0) {
      number =
        parseInt(
          $('#si-nav-outer')
            .find('.si-page-number-selected')
            .attr('id'),
          10,
        ) - 1;
    }

    return number;
  },

  /* Resizes the Slider Wrapper When Window Is Resized */
  sliderResize: function() {
    //If Outer Width is less than Two Sections Resize The Window To Fit Single Section
    if (
      $('.si-page').outerWidth() <
      EW.ServiceItems.serviceItemWidthForSlider * 2 +
        EW.ServiceItems.serviceItemLeftMarginForSlider +
        90 * 2
    ) {
      $('.sliderWrapper').css(
        'width',
        EW.ServiceItems.serviceItemWidthForSlider,
      );
    } else {
      $('.sliderWrapper').css('width', '');
    }

    //Move Nav to the middle of the page
    if ($('#si-nav-inner').size() > 0 && $('#si-header').size() > 0) {
      $('#si-nav-inner').position({
        my: 'center center',
        at: 'center bottom',
        of: '#si-header',
      });
    }
  },

  /* This is called for the required field validation */
  verifyRequiredFieldsAndShowSubmitButton: function() {
    var valid = EW.ServiceItems.validateRequiredFields(
      document.getElementById('pageQuestionsForm').elements,
    );

    if (valid === true) {
      $("[name*='Submit']")
        .addClass('mye-icon-sicheckfill')
        .removeClass('mye-icon-sicheck');
    } else {
      $("[name*='Submit']")
        .removeClass('mye-icon-sicheckfill')
        .addClass('mye-icon-sicheck');
    }
  },

  /* Function Derived From Kinetic Function KD.utils.Action.validateFields; Intended to Only Check If Required Fields Have Been Filled In */
  validateRequiredFields: function(fieldsArray) {
    var valid = true;
    var nextElement = null;
    var firstErrorElement = null;
    var requiredFieldList = '';
    var formatFieldList = '';
    var characterCount = 0;
    var disabledFields = new Array();
    var checkedFields = new Array();
    var checkboxFields = {};
    var fieldPrefix = '<li>';
    var fieldSuffix = '</li>';

    // Process all required checkbox questions first, and store the parentId
    // to indicate if any of the children have been checked
    //
    for (var f = 0; f < fieldsArray.length; f++) {
      var cb =
        fieldsArray[f].type && fieldsArray[f].type.toLowerCase() == 'checkbox';
      if (cb) {
        var cbChecked = fieldsArray[f].checked;
        var reqAttr = fieldsArray[f].getAttribute('required');
        if (reqAttr && reqAttr.toLowerCase() == 'true') {
          var cbParentId = KD.utils.Util.getCheckboxParentId(fieldsArray[f]);
          if (cbParentId) {
            var cbParentExists =
              typeof checkboxFields[cbParentId] !== 'undefined';
            // update if this is first child, or if parent isn't already checked
            if (
              !cbParentExists ||
              (cbParentExists && checkboxFields[cbParentId] === false)
            ) {
              checkboxFields[cbParentId] = fieldsArray[f].checked;
            }
          }
        }
      }
    }

    for (var i = 0; i < fieldsArray.length; i++) {
      nextElement = fieldsArray[i];
      if (
        nextElement.disabled == true &&
        nextElement.type &&
        nextElement.type.toUpperCase() != 'SUBMIT' &&
        nextElement.type.toUpperCase() != 'BUTTON'
      ) {
        disabledFields.push(nextElement);
      }

      // if the question is transient, skip validation for it
      if (KD.utils.Action._isTransient(nextElement)) {
        continue;
      }

      var foundValue = false;

      if (nextElement.type == 'radio' || nextElement.type == 'checkbox') {
        if (nextElement.checked) {
          characterCount += nextElement.value.length;
          foundValue = true;
        }
      } else if (nextElement.type != 'button' && nextElement.value.length > 0) {
        characterCount += nextElement.value.length;
        foundValue = true;
      }

      if (foundValue) {
        characterCount += nextElement.name.length + 1;
      }

      if (
        nextElement.getAttribute('required') == 'true' &&
        checkedFields[nextElement.name] != 'true'
      ) {
        checkedFields[nextElement.name] = 'true';

        if (nextElement.tagName.toLowerCase() == 'select') {
          if (
            nextElement.options.length === 0 ||
            nextElement.selectedIndex === null ||
            nextElement.value === ''
          ) {
            if (firstErrorElement === null) {
              firstErrorElement = nextElement;
            }
            valid = false;
          } else {
            KD.utils.Action.resetRequiredField(nextElement);
          }
        } else if (nextElement.type.toLowerCase() == 'radio') {
          var radioGroup = document.getElementsByName(nextElement.name);
          var isChecked = false;
          for (var j = 0; j < radioGroup.length; j++) {
            if (radioGroup[j].checked === true) {
              isChecked = true;
            }
          }
          if (isChecked === false) {
            if (firstErrorElement === null) {
              firstErrorElement = radioGroup[0];
            }
            valid = false;
          } else {
            KD.utils.Action.resetRequiredField(radioGroup[0]);
          }
        } else if (nextElement.type.toLowerCase() == 'checkbox') {
          cbParentId = KD.utils.Util.getCheckboxParentId(nextElement);
          if (cbParentId && typeof checkedFields[cbParentId] == 'undefined') {
            if (checkboxFields[cbParentId] != true) {
              if (firstErrorElement == null) {
                firstErrorElement = nextElement;
              }
              valid = false;
            }
            checkedFields[cbParentId] = 'true';
          }
          if (cbParentId && checkboxFields[cbParentId] == true) {
            KD.utils.Action.resetRequiredField(nextElement);
          }
        } else if (
          (!nextElement.value || nextElement.value.length < 1) &&
          nextElement.type.toLowerCase() != 'checkbox'
        ) {
          if (firstErrorElement === null) {
            firstErrorElement = nextElement;
          }
          var isDate = false;
          var myParent = nextElement.parentNode;
          for (var iIdx = 0; iIdx < myParent.children.length; iIdx++) {
            if (
              myParent.children[iIdx].id !== undefined &&
              myParent.children[iIdx].id.indexOf('year_') != -1
            ) {
              KD.utils.Action.highlightField(myParent.children[iIdx]);
              isDate = true;
              break;
            }
          }
          valid = false;
          continue;
        } else {
          KD.utils.Action.resetRequiredField(nextElement);
        }
      }
    }
    if (!valid) {
      return false;
    }

    return true;
  },

  /* If You Need To Hide Or Show A Section Call this function along with the Section Lable and a True (show) Or a False (hide) */
  showSection: function(sectionName, option) {
    var section = $('#' + sectionName.replace(/\s/g, '_')),
      currentIndex = EW.ServiceItems.sliderGetCurrentIndex(),
      showHide = option;

    if (showHide === true) {
      section
        .find('.si-section-hidden')
        .addClass('si-section')
        .removeClass('si-section-hidden');
      section
        .addClass('si-section-outer')
        .removeClass('si-section-outer-hidden');
      section.show('slide', { direction: 'right' }, 1000);
    } else {
      section
        .find('.si-section')
        .addClass('si-section-hidden')
        .removeClass('si-section');
      section
        .removeClass('si-section-outer')
        .addClass('si-section-outer-hidden');
      section.slideUp();
    }

    EW.ServiceItems.resizeSectionBoxes();
    EW.ServiceItems.sliderShowHideIcons();
    EW.ServiceItems.verifyRequiredFieldsAndShowSubmitButton();

    /* Recreate Nav Bar And Renumber Section Divs */
    $('.si-page-number').remove();
    var number = 0;
    $('.si-section').each(function() {
      number = number + 1;
      $(
        '<div id="' +
          number +
          '" class="si-page-number ' +
          (number - 1 === currentIndex ? 'si-page-number-selected' : '') +
          '">' +
          number +
          '</div>',
      ).appendTo('#si-nav-inner');

      $(this)
        .parent()
        .attr('mye-section-counter', number);
      $(this)
        .parent()
        .find('.si-sectionNumber')
        .html(number)
        .attr('id', number);
    });
  },

  loadServiceItemSubmitted: function() {
    /* Change Title */
    $('#si-title').html('Request Successfully Submitted');

    /* Add Event Listener to process Step titles */
    $('.si-processStep-title-outer').on(
      'click',
      '.si-processStep-title',
      function() {
        $('.si-processStep-title-selected').removeClass(
          'si-processStep-title-selected',
        );
        $('.si-processStep-value-selected').removeClass(
          'si-processStep-value-selected',
        );
        $(this).addClass('si-processStep-title-selected');

        $(
          ".si-processStep-value[mye-processstep='" +
            $(this).attr('mye-processStep') +
            "']",
        ).addClass('si-processStep-value-selected');
      },
    );

    $('#viewSubmissions').on('click', function() {
      $.ajax({
        method: 'GET',
        url:
          BUNDLE.packagePath +
          'interface/callbacks/MyProfile/Submission_StatusCheck.jsp',
        data: { r: $('[name="csrv"]').val() },
        success: function(data) {
          if (data.found) {
            var state = data.state,
              type = data.type === 'approval' ? 'actions' : 'requests',
              arrayParam = ['MyRequests', state + ' ' + type, data.id];

            EW.Modules.MyProfile.load(arrayParam);
            EW.ServiceItems.close();
          }
        },
      });
    });

    EW.Portal.hideLoadingImage();
  },

  // Creates a dropdown list populated from json array
  // question : name of the Question (should be list type)
  // array : a json array
  // text : property of object in array (as a string) to use as the label of the option
  // value : property of object in array (as a string) to use as the value of the option
  // <option value="value">text</option> // - shows placement of value and text
  // value is in quotes and is assigned to the attribute value
  // often you will use the same value for text and value
  populateList: function(question, array, text, value) {
    var el = KD.utils.Util.getElementObject(question, 'SRVQSTN_');
    for (var h = el.options.length - 1; h >= 0; h--) {
      el.remove(h);
    }

    // add a blank option
    var blankOption = document.createElement('option');
    blankOption.setAttribute('value', '');
    el.appendChild(blankOption);

    //Reset width to avoid IE bug
    el.style.width = '1';
    // add the unique option(s) found by the request
    var uniqueValueHash = new KD.utils.Hash();
    for (var h2 = 0; h2 < array.length; h2++) {
      var opt = array[h2];
      var optionValue = opt[value];
      var optionText = '';

      if (text.split(',').length > 1) {
        var optionTextArray = text.split(',');
        for (var q = 0; q < optionTextArray.length; q++) {
          if (optionTextArray[q] !== '') {
            if (optionText === '') {
              optionText += opt[optionTextArray[q]];
            } else {
              optionText += ' - ' + opt[optionTextArray[q]];
            }
          }
        }
      } else {
        optionText = opt[text];
      }

      var checkExists = uniqueValueHash.getItem(
        optionValue + '--' + optionText,
      );
      if (typeof checkExists === 'undefined' || checkExists === null) {
        var newOption = $('<option></option>')
          .attr('value', optionValue)
          .append(optionText)
          .data('ewatch', {
            tier1: opt.tier1,
            tier2: opt.tier2,
            tier3: opt.tier3,
          });

        $(el).append(newOption);

        uniqueValueHash.setItem(optionValue + '--' + optionText, optionValue);
      }
    }

    //Reset width back to avoid IE bug
    el.style.width = 'auto';

    el.setAttribute('menuAttached', 'true');
    // this is for multipage submission
    KD.utils.Action.setQuestionValue(
      question,
      el.getAttribute('originalValue'),
    );
  },

  // Adds a list option to Question List created with populateList
  // question : name of the Question that will have the option added
  // text : property of object in array (as a string) to use as the label of the option
  // value : property of object in array (as a string) to use as the value of the option
  // t1, t2, t3 are options to include tier information lookup for lists with categorization
  // The following example adds "something" as a choice to Question qlist
  // EW.ServiceItems.appendToList("qlist","something","something");
  appendToList: function(question, text, value, t1, t2, t3) {
    var el = KD.utils.Util.getElementObject(question, 'SRVQSTN_');

    //Reset width to avoid IE bug
    el.style.width = '1';

    var newOption = $('<option></option>')
      .attr('value', value)
      .append(text);

    if (t1 || t2 || t3) {
      newOption.data('ewatch', { tier1: t1, tier2: t2, tier3: t3 });
    }
    $(el).append(newOption);

    //Reset width back to avoid IE bug
    el.style.width = 'auto';
    el.setAttribute('menuAttached', 'true');
  },

  // adds Select All and Deselect All buttons to "select2" multiple selection
  // EW.ServiceItems.addSelectAllToList("#id");
  addSelectAllToList: function(selectString) {
    var newHtml =
      '<button class="si-select-select-all">Select all</button><button class="si-select-deselect-all">Deselect all</button>';
    var listSelect = $(selectString)
      .closest('div')
      .append(newHtml);
    $('.si-select-select-all').click(function(e) {
      $('option', listSelect).prop('selected', true);
      $('select', listSelect).trigger('change');
      e.preventDefault();
    });
    $('.si-select-deselect-all', listSelect).click(function(e) {
      $('option', listSelect).prop('selected', false);
      $('select', listSelect).trigger('change');
      e.preventDefault();
    });
  },
};

// EW.Modules is a namespace for feature code. The modules are empty to
// start and are loaded when first called upon.
EW.Modules = {};

// EW.Modules.Omnibox is used to perform Omnibox Searches
EW.Modules.Omnibox = {
  load: function() {
    if (typeof EW.Modules.Omnibox.wireup === 'undefined') {
      $.getScript(BUNDLE.packagePath + 'resources/js/mye-omnibox.js', function(
        data,
        textStatus,
      ) {
        EW.Modules.Omnibox.load();
      });
    } else {
      EW.Modules.Omnibox.wireup();
    }
  },
};

//EW.Modules.assistMe is used to show Q&As, documentation and Guided OBS
EW.Modules.assistMe = {
  load: function(categoryName) {
    if (typeof EW.Modules.assistMe.wireup === 'undefined') {
      $.getScript(BUNDLE.packagePath + 'resources/js/mye-assistMe.js', function(
        data,
        textStatus,
      ) {
        EW.Modules.assistMe.load(categoryName);
      });
    } else {
      EW.Modules.assistMe.wireup(categoryName);
    }
  },
};

// EW.Modules.Dashboard is empty to start and is loaded when user first uses
// the Dashboard feature.
EW.Modules.Dashboard = {
  load: function() {
    if (typeof EW.Modules.Dashboard.wireup === 'undefined') {
      $.getScript(
        BUNDLE.packagePath + 'resources/js/mye-dashboard.js',
        function(data, textStatus) {
          EW.Modules.Dashboard.load();
        },
      );
    } else {
      EW.Portal.changeModuleTo('masthead-dashboard');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath +
          'interface/fragments/Dashboard/DashboardFramework.jsp',
        EW.Modules.Dashboard.wireup,
      );
    }
  },
};

// EW.Modules.MyProfile is empty to start and is loaded when user first uses
// the myProfile feature.
EW.Modules.MyProfile = {
  load: function(parameterArray) {
    if (typeof EW.Modules.MyProfile.wireup === 'undefined') {
      $.getScript(
        BUNDLE.packagePath + 'resources/js/mye-myprofile.js',
        function(data, textStatus) {
          EW.Modules.MyProfile.load(parameterArray);
        },
      );
    } else {
      EW.Portal.changeModuleTo('masthead-myprofile');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath +
          'interface/fragments/MyProfile/MyProfileFramework.jsp',
        EW.Modules.MyProfile.wireup,
        parameterArray,
      );
    }
  },
};

// EW.Modules.Reports is empty to start and is loaded when user first uses
// the reports feature.
EW.Modules.Reports = {
  load: function() {
    if (typeof EW.Modules.Reports.wireup === 'undefined') {
      $.getScript(BUNDLE.packagePath + 'resources/js/mye-reports.js', function(
        data,
        textStatus,
      ) {
        EW.Modules.Reports.load();
      });
    } else {
      EW.Portal.changeModuleTo('masthead-reports');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath + 'interface/fragments/Reports/ReportsFramework.jsp',
        EW.Modules.Reports.wireup,
      );
    }
  },
};

// EW.Modules.Assets is empty to start and is loaded when user first uses
// the asset management feature.
EW.Modules.Assets = {
  load: function() {
    if (typeof EW.Modules.Assets.wireup === 'undefined') {
      $.getScript(BUNDLE.packagePath + 'resources/js/mye-assets.js', function(
        data,
        textStatus,
      ) {
        EW.Modules.Assets.load();
      });
    } else {
      EW.Portal.changeModuleTo('masthead-assets');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath + 'interface/fragments/Assets/AssetsFramework.jsp',
        EW.Modules.Assets.wireup,
      );
    }
  },
};

// EW.Modules.ProcessOwner is empty to start and is loaded when user first uses
// the Process Owner feature.
EW.Modules.ProcessOwner = {
  load: function() {
    if (typeof EW.Modules.ProcessOwner.wireup === 'undefined') {
      $.getScript(
        BUNDLE.packagePath + 'resources/js/mye-processowner.js',
        function(data, textStatus) {
          EW.Modules.ProcessOwner.load();
        },
      );
    } else {
      EW.Portal.changeModuleTo('masthead-processowner');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath +
          'interface/fragments/ProcessOwner/ProcessOwnerFramework.jsp',
        EW.Modules.ProcessOwner.wireup,
      );
    }
  },
};

// EW.Modules.EFactory is empty to start and is loaded when user first uses
// the eFactory Connect management feature.
EW.Modules.EFactory = {
  load: function() {
    if (typeof EW.Modules.EFactory.wireup === 'undefined') {
      $.getScript(BUNDLE.packagePath + 'resources/js/mye-efactory.js', function(
        data,
        textStatus,
      ) {
        EW.Modules.EFactory.load();
      });
    } else {
      EW.Portal.changeModuleTo('masthead-efactory');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath +
          'interface/fragments/EFactory/EFactoryFramework.jsp',
        EW.Modules.EFactory.wireup,
      );
    }
  },
};

// EW.Modules.ChangeCalendar is empty to start and is loaded when user first uses
// the change calendar viewer.
EW.Modules.ChangeCalendar = {
  load: function() {
    if (typeof EW.Modules.ChangeCalendar.wireup === 'undefined') {
      $.getScript(
        BUNDLE.packagePath + 'resources/js/mye-change-calendar.js',
        function(data, textStatus) {
          EW.Modules.ChangeCalendar.load();
        },
      );
    } else {
      EW.Portal.changeModuleTo('masthead-changecal');
      EW.Portal.loadContent(
        null,
        BUNDLE.packagePath +
          'interface/fragments/ChangeCalendar/CalendarFramework.jsp',
        EW.Modules.ChangeCalendar.wireup,
      );
    }
  },
};

/*
 *  JQuery plugins used in portal.
 *
 * Configuration options:
 *      duration: length of animation in milliseconds (default 400)
 *      easing: easing type (default swing)
 *      title: optional string to display in header - if specified this text will override the panel name
 *      titlePrefix: optional string that may be prepended to the panel name
 *      data: optional data to send to panel url. This provides default for all panels. May be overriden by panel data.
 *      headerClass: class names for header (default mye-content-title mye-40-bottom-margin)
 *      headerTextClass: class names for header text (default mye-content-title-text mye-largest)
 *      panels: array of panel objects
 *
 * Panel options:
 *      name: string to display in header when panel is shown
 *      useTitlePrefix: true if panel is to prepend the options.titlePrefix (default false)
 *      url: url path to call when panel is shown
 *      data: object of data to be passed to url
 *      alwaysRefresh: true if panel should reload from ajax every time (default false)
 *      content: static text to display instead of retrieving via 
 *      fullSize: true if panel should appear as a full size panel instead of contained in widget (default false)
 *      onLoadCallback: optional function to call after panel content is loaded.
 *      icon: object describing icon
 *
 * Icon options:
 *      type: 'FontAwesome' || 'images' || 'sprites'
 *      iconClass: optional class to apply icon. Class slider-icon is applied automatically.
 *      normal: class names (for FontAwesome or sprites) or url (for images) for unselected icon
 *      selected: class names or url for selected icon
 */
(function($, undefined) {
  $.widget('myE.myeSlider', {
    options: {
      duration: 400,
      easing: 'swing',
      title: '',
      titlePrefix: '',
      data: {},
      headerClass: 'mye-content-title mye-40-bottom-margin',
      headerTextClass: 'mye-content-title-text mye-largest',
      headerLineHeight: 24,
      panels: [],
    },

    currentPanel: '',
    maxHeight: 0,
    viewportElement: null,
    headerTextElement: null,

    _create: function() {
      var that = this,
        opts = this.options,
        uid = this.uuid,
        $head = $('<div></div>').addClass(opts.headerClass),
        $headText = $('<div></div>').addClass(opts.headerTextClass),
        $icons = $('<div></div>').addClass('slider-icons-wrapper'),
        $panels = $('<div></div>').addClass('slider-panels-wrapper'),
        $view = $('<div></div>').addClass('slider-panels-viewport');

      this.currentPanel = 'slider_panel_' + uid + '_0';
      this.viewportElement = $view;
      this.headerText = $headText;
      this.headerTextElement = $('<div></div>').addClass('mye-30-left-padding'); // $('<span></span>').addClass('mye-30-left-padding');

      for (var p = 0; p < opts.panels.length; p++) {
        var panel = opts.panels[p],
          $panel = $('<div></div>')
            .addClass('slider-panel')
            .attr('id', 'slider_panel_' + uid + '_' + p),
          $panelContent = $('<div></div>')
            .addClass('slider-panel-content')
            .append(
              $('<div></div>')
                .css('margin-top', '150px')
                .addClass('mye-portal-loader-image'),
            ),
          $icon = null;

        this.options.panels[p].contentLoaded = false;

        if (typeof panel.icon.type === 'string') {
          if (panel.icon.type.toLowerCase() === 'sprites') {
            $icon = $('<div></div>')
              .addClass('slider-icon-sprite')
              .addClass(panel.icon.normal);
          } else if (panel.icon.type.toLowerCase() === 'images') {
            $icon = $('<img />').attr('src', panel.icon.normal);
          } else {
            $icon = $('<i></i>').addClass(panel.icon.normal);
          }

          $icon
            .attr('id', 'slider_icon_' + uid + '_' + p)
            .addClass('slider-icon')
            .on(
              'click',
              { i: 'slider_icon_' + uid + '_' + p, w: that },
              that._iconClick,
            );

          if (typeof panel.icon.iconClass === 'string') {
            $icon.addClass(panel.icon.iconClass);
          }

          if (
            typeof panel.icon.helperText === 'string' &&
            EW.User.LoggedIn.tooltips === 'show'
          ) {
            $icon.attr('title', panel.icon.helperText);
          }

          $icons.append($icon);
        } else {
          alert('Icon type is not specified for panel ' + panel.name);
        }

        if (p > 0) {
          $panel.css({ left: '66.66666666%', display: 'none' });
        } else {
          $panel.css('left', '33.33333333%');
        }

        if (
          !!!panel.fullScreen &&
          typeof panel.content === 'string' &&
          panel.content !== ''
        ) {
          $panelContent.html(panel.content);
          this.options.panels[p].contentLoaded = true;
        }

        $panel.append($panelContent);
        $view.append($panel);
      }

      // Build up head
      $icons.children().wrap('<div class="slider-icon-container"></div>');
      $head.append($icons);

      $headText.append(this.headerTextElement);
      $head.append($headText);

      // Append head to element
      this.element.append($head);

      // Append panels to element
      $panels.append($view);
      this.element.addClass('mye-slider').append($panels);

      // Load first panel
      if (opts.panels.length > 0) {
        var firstPanel = opts.panels[0],
          $newIcon = $('#slider_icon_' + this.uuid + '_0');

        if (firstPanel.icon.type.toLowerCase() === 'images') {
          $newIcon.attr('src', firstPanel.icon.selected);
        } else {
          $newIcon.addClass(firstPanel.icon.selected);

          if (firstPanel.icon.type.toLowerCase() === 'sprites') {
            $newIcon.removeClass(firstPanel.icon.normal);
          }
        }

        if (typeof opts.title === 'string' && opts.title !== '') {
          this.headerTextElement.html(opts.title);
        } else {
          this._updatePanelTitle(firstPanel, this);
        }
      }

      this._loadContent(
        this.options.panels[0],
        $('#slider_panel_' + this.uuid + '_0'),
        this,
      ).done(function() {
        $(window).trigger('resize');
      });

      $(window).on('resize', { w: that }, this._resize);
      $('.slider-icon[title]').tooltipster();
    },

    _setOption: function(n, v) {
      if (n === 'titlePrefix') {
        this.options.titlePrefix = v;
        this._updatePanelTitle(
          this.options.panels[parseInt(this.currentPanel.split('_').pop(), 10)],
          this,
        );
      }

      this._super(n, v);
    },

    // public method 'refresh' to refresh the current panel's data.
    refresh: function() {
      var panelId = parseInt(this.currentPanel.split('_').pop(), 10),
        $panel = $('#' + this.currentPanel),
        panel = this.options.panels[panelId];

      // Set current panel's contentLoaded flag to false to force reload.
      this.options.panels[panelId].contentLoaded = false;

      $panel
        .find('.slider-panel-content')
        .empty()
        .append(
          $('<div></div>')
            .css('margin-top', '150px')
            .addClass('mye-portal-loader-image'),
        );
      this._loadContent(panel, $panel, this).done(function() {
        $(window).trigger('resize');
      });
    },

    // public method 'refreshAll' to refresh current panel to reload and resets contentLoaded flag for all other panels.
    refreshAll: function() {
      var panelId = parseInt(this.currentPanel.split('_').pop(), 10),
        $panel = $('#' + this.currentPanel),
        panel = this.options.panels[panelId];

      // Set all panel's contentLoaded flag to false to force reload.
      for (var p = 0; p < this.options.panels.length; p++)
        this.options.panels[p].contentLoaded = false;

      $panel
        .find('.slider-panel-content')
        .empty()
        .append(
          $('<div></div>')
            .css('margin-top', '150px')
            .addClass('mye-portal-loader-image'),
        );
      this._loadContent(panel, $panel, this).done(function() {
        $(window).trigger('resize');
      });
    },

    panelName: function(panelId, panelName) {
      this.options.panels[panelId].name = panelName;

      this._updatePanelTitle(
        this.options.panels[parseInt(this.currentPanel.split('_').pop(), 10)],
        this,
      );
    },

    _iconClick: function(e) {
      e.stopPropagation();

      var i = e.data.i,
        w = e.data.w,
        panelId = i.split('_').pop(),
        panel = w.options.panels[panelId],
        $newPanel = $('#slider_panel_' + w.uuid + '_' + panelId),
        $curPanel = $('#' + w.currentPanel),
        curPanelId = w.currentPanel.split('_').pop(),
        curPanel = w.options.panels[curPanelId],
        $newIcon = $('#slider_icon_' + w.uuid + '_' + panelId),
        $curIcon = $('#slider_icon_' + w.uuid + '_' + curPanelId);

      if (panelId === curPanelId) return;

      if (!!panel.fullScreen) {
        w._showFullSize(panel, w);
        return;
      }

      if (panelId < curPanelId) {
        $newPanel.css('left', '0%').show();
        $curPanel.animate(
          { left: '66.66666666%' },
          w.options.duration,
          w.options.easing,
          function() {
            $curPanel.hide();
          },
        );
        $newPanel.animate(
          { left: '33.33333333%' },
          w.options.duration,
          w.options.easing,
        );
      } else {
        $newPanel.css('left', '66.66666666%').show();
        $curPanel.animate(
          { left: '0%' },
          w.options.duration,
          w.options.easing,
          function() {
            $curPanel.hide();
          },
        );
        $newPanel.animate(
          { left: '33.33333333%' },
          w.options.duration,
          w.options.easing,
        );
      }

      w.currentPanel = 'slider_panel_' + w.uuid + '_' + panelId;

      // Update the icons by adding or removing classes or updating source URLs.
      if (panel.icon.type.toLowerCase() === 'images') {
        $newIcon.attr('src', panel.icon.selected);
        $curIcon.attr('src', curPanel.icon.normal);
      } else {
        $newIcon.addClass(panel.icon.selected);
        $curIcon.removeClass(curPanel.icon.selected);

        if (panel.icon.type.toLowerCase() === 'sprites') {
          $newIcon.removeClass(panel.icon.normal);
          $curIcon.addClass(curPanel.icon.normal);
        }
      }

      w._updatePanelTitle(panel, w);
      w._loadContent(
        w.options.panels[parseInt(panelId, 10)],
        $newPanel,
        w,
      ).done(function() {
        $(window).trigger('resize');
      });
    },

    _updatePanelTitle: function(panel, w) {
      // if title is empty or not a string then update the panel title
      if (typeof w.options.title !== 'string' || w.options.title === '') {
        if (!!panel.useTitlePrefix) {
          w.headerTextElement.html(w.options.titlePrefix + panel.name);
        } else {
          w.headerTextElement.html(panel.name);
        }
      }
    },

    _loadContent: function(p, pnl, w) {
      var def = $.Deferred(),
        $panelContent = pnl.find('.slider-panel-content');

      // if content is not loaded or if panel's alwaysRefresh is true
      if (!!!p.contentLoaded || !!p.alwaysRefresh) {
        if (typeof p.content === 'string' && p.content !== '') {
          $panelContent.html(p.content);
          if (typeof p.onLoadCallback === 'function') {
            p.onLoadCallback();
          }
        } else {
          $.ajax({
            type: 'GET',
            url: p.url,
            data: $.extend({ _: new Date().getTime() }, w.options.data, p.data),
            success: function(data) {
              $panelContent.empty().html(data);

              if (typeof p.onLoadCallback === 'function') {
                p.onLoadCallback();
              }

              w.options.panels[
                parseInt(
                  pnl
                    .attr('id')
                    .split('_')
                    .pop(),
                  10,
                )
              ].contentLoaded = true;

              w._setHeight(pnl.height(), w);
              def.resolve();
            },
            error: function(data) {
              $panelContent.html(data);
            },
          });
        }
      } else {
        w._setHeight(pnl.height(), w);
        def.resolve();
      }

      return def.promise();
    },

    _showFullSize: function(p, w) {
      if (typeof p.content === 'string' && p.content !== '')
        EW.Portal.showModalOverlay(p.content, p.onLoadCallback);
      else
        EW.Portal.showModalOverlay(
          p.url,
          $.extend({ _: new Date().getTime() }, w.options.data, p.data),
          p.onLoadCallback,
        );
    },

    _hideFullSize: function() {
      $('#sliderFullscreen')
        .fadeOut(500, function() {
          $('#contentViewport').fadeIn(500);
        })
        .off('clickoutside');
    },

    _setHeight: function(h, w) {
      if (h > w.maxHeight) {
        this.viewportElement.css('height', h);
      }
    },

    _resize: function(e) {
      e.data.w._setHeight($('#' + e.data.w.currentPanel).height(), e.data.w);
      if (
        e.data.w.headerTextElement.height() > e.data.w.options.headerLineHeight
      ) {
        e.data.w.headerText.addClass('slider-title-two-lines');
      } else {
        e.data.w.headerText.removeClass('slider-title-two-lines');
      }
    },

    _destroy: function() {
      $(window).off('resize', this._resize);
      $('#sliderFullscreen').remove();
      this.element.empty();
      this._super();
    },

    /*
		* public method 'gotoPanelIndex' to mimic clicking an icon to change panels.
		* The zero based id is used to determine which panel to switch.
		*/
    gotoPanelIndex: function(panelId) {
      // Check that the panelId is within the panels array length.
      if (parseInt(panelId, 10) < this.options.panels.length) {
        $('#slider_icon_' + this.uuid + '_' + panelId).trigger('click');
      }
    },

    resizePanelViewport: function() {
      $('.slider-panels-viewport').height(
        $('.slider-panel-content:visible').height(),
      );
    },

    /*
		* public method 'gotoPanelName' to mimic clicking an icon to change panels. 
		* The Name property of panel is used to determine which panel to switch.
		*/
    gotoPanelName: function(panelName) {
      // Loop through panels and determine if the panel name matches the panelName parameter.
      for (var p = 0; p < this.options.panels.length; p++) {
        if (this.options.panels[p].name === panelName) {
          $('#slider_icon_' + this.uuid + '_' + p).trigger('click');
          break;
        }
      }
    },
  });
})(jQuery);

/*
 * Data Table Column Filter Widget
 *
 * Adds column filtering to all visible columns in a data table. Must be applied after dataTable().
 *
 * $(table).dataTable(options to dataTable).myeTableFilter();
 *
 * options:
 *     iconType: 'FontAwesome' || 'images' || 'sprites'
 *      normal: class names (for FontAwesome or sprites) or url (for images) for normal/unfiltered icon
 *      active: class names or url for filter applied icon
 */
(function($, undefined) {
  $.widget('myE.myeTableFilter', {
    options: {
      iconType: 'sprites',
      normal: 'mye-icon-filter',
      active: 'mye-icon-filterfill',
      excludeColumns: [],
    },

    _lastIdx: 0, // stores the last column index clicked for future use
    _filterDialog: null, // dialog div. Saved for easy access and removal
    _filterInput: null, // input field for easy access
    _filterHeader: null, // header span for easy access

    _create: function() {
      var that = this,
        opts = this.options,
        heads = $('thead tr th', this.element); // visible column headers

      // Build up input, header, and dialog
      this._filterInput = $('<input type="text" id="myeETableFilterText" />')
        .addClass('mye-dt-filter-text')
        .on('keyup', function(e) {
          that._filterInputKeypress(e, that);
        });
      this._filterHeader = $('<span></span>');

      this._filterDialog = $('<div></div>')
        .addClass('mye-dt-filter-wrapper')
        .append(
          $('<div></div>')
            .addClass('mye-dt-filter-header')
            .text('Filter by ')
            .append(this._filterHeader),
        )
        .append(this._filterInput)
        .append(
          $('<div></div>')
            .addClass('mye-dt-filter-button mye-dt-filter-okay')
            .append($('<i></i>').addClass('fa fa-check'))
            .on('click', function() {
              that._filterOkayClick(that);
            }),
        )
        .append(
          $('<div></div>')
            .addClass('mye-dt-filter-button mye-dt-filter-cancel')
            .append($('<i></i>').addClass('fa fa-times'))
            .on('click', function() {
              that._filterCancelClick(that);
            }),
        );

      // append dialog to body
      $('body').append(this._filterDialog);

      // for each column header add the filter icon/image and bind click event
      heads.each(function(idx, el) {
        var $filter,
          f = true;

        // Is the current column in the exclude array?
        for (var i = 0; i < opts.excludeColumns.length; i++) {
          if (opts.excludeColumns[i] === idx) {
            f = false;
            break;
          }
        }

        // If not excluded then create the filter icon.
        if (f) {
          if (opts.iconType === 'FontAwesome') {
            $filter = $('<i></i>')
              .addClass('mye-dt-filter-icon')
              .addClass(opts.normal);
          } else if (opts.iconType === 'images') {
            $filter = $('<img />')
              .attr('src', opts.normal)
              .addClass('mye-dt-filter-icon');
          } else {
            $filter = $('<div></div>')
              .addClass(opts.normal)
              .addClass('mye-dt-filter-icon');
          }

          $filter.on('click', function(e) {
            e.stopPropagation();
            that._filterClick($(this));
          });

          $(el).append(' ');
          $(el).append($filter);
        }
      });
    },

    applyFilter: function(col) {
      this._lastIdx = col;
      this._setIconActive(this);
    },

    // Called when any filter icon is clicked. el is the icon.
    _filterClick: function(el) {
      var that = this, // needed for clickoutside event
        table = $(this.element).DataTable(), // Data Table API
        colIdx = table.column(el.parent()).index(), // table column accounting for hidden columns
        curSearch = table.column(colIdx).search(); // current column search term

      this._lastIdx = colIdx; // Save column index for later use.

      this._filterHeader.text(el.parent().text()); // Update header text with name of column

      // Show dialog and position right above filter icon
      this._filterDialog
        .show()
        .position({
          my: 'center bottom-10',
          at: 'center top',
          of: el,
        })
        .one('clickoutside', function() {
          that._filterOkayClick(that);
        });
      this._filterInput.val(curSearch).focus(); // update input default with current search and give focus.
    },

    _filterInputKeypress: function(e, w) {
      if (e.which === 13) {
        // enter
        w._filterOkayClick(w);
      } else if (w._filterInput.val().length > 0) {
        // update icon to active state
        w._setIconActive(w);
      } else {
        // update icon to normal state
        w._setIconNormal(w);
      }

      $(w.element)
        .DataTable()
        .column(w._lastIdx)
        .search(w._filterInput.val())
        .draw();
    },

    // Called when checkmark is clicked. Hides dialog.
    _filterOkayClick: function(w) {
      w._filterDialog.hide().off('clickoutside');
    },

    // Called when user clicks cancel button. Clears search term and updates icon.
    _filterCancelClick: function(w) {
      // Clear search term
      $(w.element)
        .DataTable()
        .column(w._lastIdx)
        .search('')
        .draw();

      // Restore icon to normal state
      w._setIconNormal(w);

      w._filterDialog.hide();
    },

    _setIconNormal: function(w) {
      if (w.options.iconType === 'FontAwesome') {
        $h = $(
          'i',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.removeClass(w.options.active);
      } else if (w.options.iconType === 'images') {
        $h = $(
          'img',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.attr('src', w.options.normal);
      } else {
        $h = $(
          'div',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.removeClass(w.options.active).addClass(w.options.normal);
      }
    },

    _setIconActive: function(w) {
      if (w.options.iconType === 'FontAwesome') {
        $h = $(
          'i',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.addClass(w.options.active);
      } else if (w.options.iconType === 'images') {
        $h = $(
          'img',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.attr('src', w.options.active);
      } else {
        $h = $(
          'div',
          $(w.element)
            .DataTable()
            .columns(w._lastIdx)
            .header(),
        );
        $h.removeClass(w.options.normal).addClass(w.options.active);
      }
    },

    // Removes elements created by widget.
    _destroy: function() {
      $('thead tr th .mye-dt-filter-icon', this.element).remove();
      this._filterDialog.remove();
      this._super();
    },
  });
})(jQuery);

/*
 * Data Table Filter Widget
 *
 * Wires up any text input to search/fiter all columns in a datatable.
 * This mimics the functionality of Data Table's built-in search input but allows
 * the search input to be placed anywhere in the page/DOM.
 *
 * $('#searchInput').myeTableSearchInput(options);
 *
 * options:
 *		api: required reference to Data Table API of target table
 *		searchClass: CSS class to add to the input element
 *		placeholder: Text to add as placeholder
 */
(function($, undefined) {
  $.widget('myE.myeTableSearchInput', {
    options: {
      api: null,
      searchClass: 'mye-dt-search',
      placeholder: 'Search',
    },

    _create: function() {
      var that = this,
        opts = this.options;

      this.element.addClass(opts.searchClass);
      if (typeof opts.placeholder === 'string' && opts.placeholder !== '') {
        this.element.attr('placeholder', opts.placeholder);
      }

      this.element.on('keyup', function(e) {
        that._filterInputKeypress(e, that);
      });
    },

    _filterInputKeypress: function(e, w) {
      w.options.api.search(w.element.val()).draw();
    },

    _destroy: function() {
      this.element.off(this._filterInputKeypress);
      this._super();
    },
  });
})(jQuery);

/*
 * jQuery throttle / debounce - v1.1 - 3/7/2010
 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function(b, c) {
  var $ = b.jQuery || b.Cowboy || (b.Cowboy = {}),
    a;
  $.throttle = a = function(e, f, j, i) {
    var h,
      d = 0;
    if (typeof f !== 'boolean') {
      i = j;
      j = f;
      f = c;
    }
    function g() {
      var o = this,
        m = +new Date() - d,
        n = arguments;
      function l() {
        d = +new Date();
        j.apply(o, n);
      }
      function k() {
        h = c;
      }
      if (i && !h) {
        l();
      }
      h && clearTimeout(h);
      if (i === c && m > e) {
        l();
      } else {
        if (f !== true) {
          h = setTimeout(i ? k : l, i === c ? e - m : e);
        }
      }
    }
    if ($.guid) {
      g.guid = j.guid = j.guid || $.guid++;
    }
    return g;
  };
  $.debounce = function(d, e, f) {
    return f === c ? a(d, e, false) : a(d, f, e !== false);
  };
})(this);

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($, c, b) {
  $.map(
    'click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup'.split(
      ' ',
    ),
    function(d) {
      a(d);
    },
  );
  a('focusin', 'focus' + b);
  a('focusout', 'blur' + b);
  $.addOutsideEvent = a;
  function a(g, e) {
    e = e || g + b;
    var d = $(),
      h = g + '.' + e + '-special-event';
    $.event.special[e] = {
      setup: function() {
        d = d.add(this);
        if (d.length === 1) {
          $(c).bind(h, f);
        }
      },
      teardown: function() {
        d = d.not(this);
        if (d.length === 0) {
          $(c).unbind(h);
        }
      },
      add: function(i) {
        var j = i.handler;
        i.handler = function(l, k) {
          l.target = k;
          j.apply(this, arguments);
        };
      },
    };
    function f(i) {
      $(d).each(function() {
        var j = $(this);
        if (this !== i.target && !j.has(i.target).length) {
          j.triggerHandler(e, [i.target]);
        }
      });
    }
  }
})(jQuery, document, 'outside');

/*
 * jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010
 * http://benalman.com/projects/jquery-dotimeout-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($) {
  var a = {},
    c = 'doTimeout',
    d = Array.prototype.slice;
  $[c] = function() {
    return b.apply(window, [0].concat(d.call(arguments)));
  };
  $.fn[c] = function() {
    var f = d.call(arguments),
      e = b.apply(this, [c + f[0]].concat(f));
    return typeof f[0] === 'number' || typeof f[1] === 'number' ? this : e;
  };
  function b(l) {
    var m = this,
      h,
      k = {},
      g = l ? $.fn : $,
      n = arguments,
      i = 4,
      f = n[1],
      j = n[2],
      p = n[3];
    if (typeof f !== 'string') {
      i--;
      f = l = 0;
      j = n[1];
      p = n[2];
    }
    if (l) {
      h = m.eq(0);
      h.data(l, (k = h.data(l) || {}));
    } else {
      if (f) {
        k = a[f] || (a[f] = {});
      }
    }
    k.id && clearTimeout(k.id);
    delete k.id;
    function e() {
      if (l) {
        h.removeData(l);
      } else {
        if (f) {
          delete a[f];
        }
      }
    }
    function o() {
      k.id = setTimeout(function() {
        k.fn();
      }, j);
    }
    if (p) {
      k.fn = function(q) {
        if (typeof p === 'string') {
          p = g[p];
        }
        p.apply(m, d.call(n, i)) === true && !q ? o() : e();
      };
      o();
    } else {
      if (k.fn) {
        j === undefined ? e() : k.fn(j === false);
        return true;
      } else {
        e();
      }
    }
  }
})(jQuery);

/**
 * jQuery plugin wrapper for TinySort v2.1.0
 * Does not use the first argument in tinysort.js since that is handled internally by the jQuery selector.
 * Sub-selections (option.selector) do not use the jQuery selector syntax but regular CSS3 selector syntax.
 * @summary jQuery plugin wrapper for TinySort v2.1.0
 * @version 2.0.0
 * @requires tinysort v2.1.0
 * @license MIT/GPL
 * @author Ron Valstar (http://www.sjeiti.com/)
 * @copyright Ron Valstar <ron@ronvalstar.nl>
 */
/**
 * TinySort is a small script that sorts HTML elements. It sorts by text- or attribute value, or by that of one of it's children.
 * @summary A nodeElement sorting script.
 * @version 2.1.2
 * @license MIT/GPL
 * @author Ron Valstar <ron@ronvalstar.nl> (http://www.sjeiti.com/)
 * @copyright Ron Valstar <ron@ronvalstar.nl>
 * @namespace tinysort
 */
!(function(a, b) {
  'use strict';
  function c() {
    return b;
  }
  'function' == typeof define && define.amd
    ? define('tinysort', c)
    : (a.tinysort = b);
})(
  this,
  (function() {
    'use strict';
    function a(a) {
      function f() {
        0 === arguments.length
          ? j({})
          : d(arguments, function(a) {
              j(c(a) ? { selector: a } : a);
            }),
          (q = C.length);
      }
      function j(a) {
        var b = !!a.selector,
          c = b && ':' === a.selector[0],
          d = e(a || {}, s);
        C.push(
          e(
            {
              bFind: b,
              bAttr: !(d.attr === i || '' === d.attr),
              bData: d.data !== i,
              bFilter: c,
              mFilter: i,
              fnSort: d.sortFunction,
              iAsc: 'asc' === d.order ? 1 : -1,
            },
            d,
          ),
        );
      }
      function m() {
        d(a, function(a, b) {
          x ? x !== a.parentNode && (D = !1) : (x = a.parentNode);
          var c = C[0],
            d = c.bFilter,
            e = c.selector,
            f = !e || (d && a.matchesSelector(e)) || (e && a.querySelector(e)),
            g = f ? A : B,
            h = { elm: a, pos: b, posn: g.length };
          z.push(h), g.push(h);
        }),
          (w = A.slice(0));
      }
      function t() {
        A.sort(u);
      }
      function u(a, e) {
        var f = 0;
        for (0 !== r && (r = 0); 0 === f && q > r; ) {
          var i = C[r],
            j = i.ignoreDashes ? o : n;
          if (
            (d(p, function(a) {
              var b = a.prepare;
              b && b(i);
            }),
            i.sortFunction)
          )
            f = i.sortFunction(a, e);
          else if ('rand' == i.order) f = Math.random() < 0.5 ? 1 : -1;
          else {
            var k = h,
              m = b(a, i),
              s = b(e, i);
            if (!i.forceStrings) {
              var t = c(m) ? m && m.match(j) : h,
                u = c(s) ? s && s.match(j) : h;
              if (t && u) {
                var v = m.substr(0, m.length - t[0].length),
                  w = s.substr(0, s.length - u[0].length);
                v == w && ((k = !h), (m = l(t[0])), (s = l(u[0])));
              }
            }
            f = m === g || s === g ? 0 : i.iAsc * (s > m ? -1 : m > s ? 1 : 0);
          }
          d(p, function(a) {
            var b = a.sort;
            b && (f = b(i, k, m, s, f));
          }),
            0 === f && r++;
        }
        return 0 === f && (f = a.pos > e.pos ? 1 : -1), f;
      }
      function v() {
        var a = A.length === z.length;
        D && a
          ? (A.forEach(function(a) {
              y.appendChild(a.elm);
            }),
            x.appendChild(y))
          : (A.forEach(function(a) {
              var b = a.elm,
                c = k.createElement('div');
              (a.ghost = c), b.parentNode.insertBefore(c, b);
            }),
            A.forEach(function(a, b) {
              var c = w[b].ghost;
              c.parentNode.insertBefore(a.elm, c), c.parentNode.removeChild(c);
            }));
      }
      c(a) && (a = k.querySelectorAll(a)),
        0 === a.length && console.warn('No elements to sort');
      var w,
        x,
        y = k.createDocumentFragment(),
        z = [],
        A = [],
        B = [],
        C = [],
        D = !0;
      return (
        f.apply(i, Array.prototype.slice.call(arguments, 1)),
        m(),
        t(),
        v(),
        A.map(function(a) {
          return a.elm;
        })
      );
    }
    function b(a, b) {
      var d,
        e = a.elm;
      return (
        b.selector &&
          (b.bFilter
            ? e.matchesSelector(b.selector) || (e = i)
            : (e = e.querySelector(b.selector))),
        b.bAttr
          ? (d = e.getAttribute(b.attr))
          : b.useVal
            ? (d = e.value)
            : b.bData
              ? (d = e.getAttribute('data-' + b.data))
              : e && (d = e.textContent),
        c(d) &&
          (b.cases || (d = d.toLowerCase()), (d = d.replace(/\s+/g, ' '))),
        d
      );
    }
    function c(a) {
      return 'string' == typeof a;
    }
    function d(a, b) {
      for (var c, d = a.length, e = d; e--; ) (c = d - e - 1), b(a[c], c);
    }
    function e(a, b, c) {
      for (var d in b) (c || a[d] === g) && (a[d] = b[d]);
      return a;
    }
    function f(a, b, c) {
      p.push({ prepare: a, sort: b, sortBy: c });
    }
    var g,
      h = !1,
      i = null,
      j = window,
      k = j.document,
      l = parseFloat,
      m = Array.prototype.indexOf,
      n = /(-?\d+\.?\d*)\s*$/g,
      o = /(\d+\.?\d*)\s*$/g,
      p = [],
      q = 0,
      r = 0,
      s = {
        selector: i,
        order: 'asc',
        attr: i,
        data: i,
        useVal: h,
        place: 'start',
        returns: h,
        cases: h,
        forceStrings: h,
        ignoreDashes: h,
        sortFunction: i,
      };
    return (
      j.Element &&
        (function(a) {
          a.matchesSelector =
            a.matchesSelector ||
            a.mozMatchesSelector ||
            a.msMatchesSelector ||
            a.oMatchesSelector ||
            a.webkitMatchesSelector ||
            function(a) {
              for (
                var b = this,
                  c = (b.parentNode || b.document).querySelectorAll(a),
                  d = -1;
                c[++d] && c[d] != b;

              );
              return !!c[d];
            };
        })(Element.prototype),
      e(f, { indexOf: m, loop: d }),
      e(a, { plugin: f, defaults: s })
    );
  })(),
);
!(function(a) {
  'use strict';
  'function' == typeof define && define.amd
    ? define(['jquery', 'tinysort'], a)
    : jQuery && !jQuery.fn.tsort && a(jQuery, tinysort);
})(function(a, b) {
  'use strict';
  (a.tinysort = { defaults: b.defaults }),
    a.fn.extend({
      tinysort: function() {
        var a,
          c,
          d = Array.prototype.slice.call(arguments);
        d.unshift(this), (a = b.apply(null, d)), (c = a.length);
        for (var e = 0, f = this.length; f > e; e++)
          c > e ? (this[e] = a[e]) : delete this[e];
        return (this.length = c), this;
      },
    }),
    (a.fn.tsort = a.fn.tinysort);
});

/*
 * jquery.pajinate.js - version 0.4
 * A jQuery plugin for paginating through any number of DOM elements
 * 
 * Copyright (c) 2010, Wes Nolte (http://wesnolte.com)
 * Licensed under the MIT License (MIT-LICENSE.txt)
 * http://www.opensource.org/licenses/mit-license.php
 * Created: 2010-04-16 | Updated: 2010-04-26
 *
 * Modifications by Mykl Klingenberg to support filtering.
 */
(function($) {
  $.fn.pajinate = function(options) {
    // Set some state information
    var current_page = 'current_page';
    var items_per_page = 'items_per_page';

    var meta;

    // Setup default option values
    var defaults = {
      item_container_id: '.content',
      items_per_page: 10,
      nav_panel_id: '.page_navigation',
      nav_info_id: '.info_text',
      num_page_links_to_display: 20,
      start_page: 0,
      wrap_around: false,
      nav_label_first: 'First',
      nav_label_prev: 'Prev',
      nav_label_next: 'Next',
      nav_label_last: 'Last',
      nav_order: ['first', 'prev', 'num', 'next', 'last'],
      nav_label_info: 'Showing {0}-{1} of {2} results',
      show_first_last: true,
      abort_on_small_lists: false,
      jquery_ui: false,
      jquery_ui_active: 'ui-state-highlight',
      jquery_ui_default: 'ui-state-default',
      jquery_ui_disabled: 'ui-state-disabled',
      filtered_class: '',
      show_hide_class: '',
      show_ellipses: true,
      use_nav_divs: false,
      nav_div_class: '',
    };

    var options = $.extend(defaults, options);
    var $item_container;
    var $page_container;
    var $items;
    var $nav_panels;
    var total_page_no_links;
    var jquery_ui_default_class = options.jquery_ui
      ? options.jquery_ui_default
      : '';
    var jquery_ui_active_class = options.jquery_ui
      ? options.jquery_ui_active
      : '';
    var jquery_ui_disabled_class = options.jquery_ui
      ? options.jquery_ui_disabled
      : '';

    return this.each(function() {
      $page_container = $(this);
      $item_container = $(this).find(options.item_container_id);
      // MK - Added filtered_class support. filtered_class is applied to items hidden because htey are filtered out.
      if (options.filtered_class !== '')
        $items = $page_container
          .find(options.item_container_id)
          .children()
          .not('.' + options.filtered_class);
      else $items = $page_container.find(options.item_container_id).children();

      if (
        options.abort_on_small_lists &&
        options.items_per_page >= $items.size()
      ) {
        $page_container.find(options.nav_panel_id).empty();
        return $page_container;
      }
      meta = $page_container;

      // Initialize meta data
      meta.data(current_page, 0);
      meta.data(items_per_page, options.items_per_page);

      // Get the total number of items
      // MK - Added filtered_class support
      var total_items;
      if (options.filtered_class !== '')
        total_items = $item_container
          .children()
          .not('.' + options.filtered_class)
          .size();
      else total_items = $item_container.children().size();

      // Calculate the number of pages needed
      var number_of_pages = Math.ceil(total_items / options.items_per_page);

      // Construct the nav bar
      var more = options.show_ellipses
        ? '<span class="ellipse more">...</span>'
        : '';
      var less = options.show_ellipses
        ? '<span class="ellipse less">...</span>'
        : '';
      var first = '',
        last = '';

      if (options.use_nav_divs) {
        first = !options.show_first_last
          ? ''
          : '<div class="first_link ' +
            options.nav_div_class +
            ' ' +
            jquery_ui_default_class +
            '">' +
            options.nav_label_first +
            '</div>';
        last = !options.show_first_last
          ? ''
          : '<div class="last_link ' +
            options.nav_div_class +
            ' ' +
            jquery_ui_default_class +
            '">' +
            options.nav_label_last +
            '</div>';
      } else {
        first = !options.show_first_last
          ? ''
          : '<a class="first_link ' +
            jquery_ui_default_class +
            '" href="">' +
            options.nav_label_first +
            '</a>';
        last = !options.show_first_last
          ? ''
          : '<a class="last_link ' +
            jquery_ui_default_class +
            '" href="">' +
            options.nav_label_last +
            '</a>';
      }

      var navigation_html = '';

      for (var i = 0; i < options.nav_order.length; i++) {
        switch (options.nav_order[i]) {
          case 'first':
            navigation_html += first;
            break;
          case 'last':
            navigation_html += last;
            break;
          case 'next':
            if (options.use_nav_divs) {
              navigation_html +=
                '<div class="next_link ' +
                options.nav_div_class +
                ' ' +
                jquery_ui_default_class +
                '">' +
                options.nav_label_next +
                '</div>';
            } else {
              navigation_html +=
                '<a class="next_link ' +
                jquery_ui_default_class +
                '" href="">' +
                options.nav_label_next +
                '</a>';
            }
            break;
          case 'prev':
            if (options.use_nav_divs) {
              navigation_html +=
                '<div class="previous_link ' +
                options.nav_div_class +
                ' ' +
                jquery_ui_default_class +
                '">' +
                options.nav_label_prev +
                '</div>';
            } else {
              navigation_html +=
                '<a class="previous_link ' +
                jquery_ui_default_class +
                '" href="">' +
                options.nav_label_prev +
                '</a>';
            }
            break;
          case 'num':
            navigation_html += less;
            var current_link = 0;
            while (number_of_pages > current_link) {
              if (options.use_nav_divs) {
                navigation_html +=
                  '<div class="page_link ' +
                  options.nav_div_class +
                  ' ' +
                  jquery_ui_default_class +
                  '" longdesc="' +
                  current_link +
                  '">' +
                  (current_link + 1) +
                  '</div>';
              } else {
                navigation_html +=
                  '<a class="page_link ' +
                  jquery_ui_default_class +
                  '" href="" longdesc="' +
                  current_link +
                  '">' +
                  (current_link + 1) +
                  '</a>';
              }
              current_link++;
            }
            navigation_html += more;
            break;
          default:
            break;
        }
      }

      // And add it to the appropriate area of the DOM
      $nav_panels = $page_container.find(options.nav_panel_id);
      $nav_panels.html(navigation_html).each(function() {
        $(this)
          .find('.page_link:first')
          .addClass('first');
        $(this)
          .find('.page_link:last')
          .addClass('last');
      });

      // Hide the more/less indicators
      $nav_panels.children('.ellipse').hide();

      // Set the active page link styling
      $nav_panels
        .find('.previous_link')
        .next()
        .next()
        .addClass('active_page ' + jquery_ui_active_class);

      /* Setup Page Display */
      // And hide all pages
      // MK - Added show_hide_class support. show_hide_class is used to hide via CSS rather than jQuery show() and hide()
      if (options.show_hide_class !== '') {
        $items.addClass(options.show_hide_class);
        // Show the first page
        $items
          .slice(0, meta.data(items_per_page))
          .removeClass(options.show_hide_class);
      } else {
        $items.hide();
        // Show the first page
        $items.slice(0, meta.data(items_per_page)).show();
      }

      /* Setup Nav Menu Display */
      // Page number slices
      total_page_no_links = $page_container
        .find(options.nav_panel_id + ':first')
        .children('.page_link')
        .size();
      options.num_page_links_to_display = Math.min(
        options.num_page_links_to_display,
        total_page_no_links,
      );

      $nav_panels.children('.page_link').hide(); // Hide all the page links
      // And only show the number we should be seeing
      $nav_panels.each(function() {
        $(this)
          .children('.page_link')
          .slice(0, options.num_page_links_to_display)
          .show();
      });

      /* Bind the actions to their respective links */

      // Event handler for 'First' link
      $page_container.find('.first_link').click(function(e) {
        e.preventDefault();

        movePageNumbersRight($(this), 0);
        gotopage(0);
      });

      // Event handler for 'Last' link
      $page_container.find('.last_link').click(function(e) {
        e.preventDefault();
        var lastPage = total_page_no_links - 1;
        movePageNumbersLeft($(this), lastPage);
        gotopage(lastPage);
      });

      // Event handler for 'Prev' link
      $page_container.find('.previous_link').click(function(e) {
        e.preventDefault();
        showPrevPage($(this));
      });

      // Event handler for 'Next' link
      $page_container.find('.next_link').click(function(e) {
        e.preventDefault();
        showNextPage($(this));
      });

      // Event handler for each 'Page' link
      $page_container.find('.page_link').click(function(e) {
        e.preventDefault();
        gotopage($(this).attr('longdesc'));
      });

      // Goto the required page
      gotopage(parseInt(options.start_page, 10));
      toggleMoreLess();
      if (!options.wrap_around) tagNextPrev();
    });

    function showPrevPage(e) {
      new_page = parseInt(meta.data(current_page), 10) - 1;

      // Check that we aren't on a boundary link
      if (
        $(e)
          .siblings('.active_page')
          .prev('.page_link').length == true
      ) {
        movePageNumbersRight(e, new_page);
        gotopage(new_page);
      } else if (options.wrap_around) {
        gotopage(total_page_no_links - 1);
      }
    }

    function showNextPage(e) {
      new_page = parseInt(meta.data(current_page), 10) + 1;

      // Check that we aren't on a boundary link
      if (
        $(e)
          .siblings('.active_page')
          .next('.page_link').length == true
      ) {
        movePageNumbersLeft(e, new_page);
        gotopage(new_page);
      } else if (options.wrap_around) {
        gotopage(0);
      }
    }

    function gotopage(page_num) {
      page_num = parseInt(page_num, 10);

      var ipp = parseInt(meta.data(items_per_page), 10);

      // Find the start of the next slice
      start_from = page_num * ipp;

      // Find the end of the next slice
      end_on = start_from + ipp;
      // Hide the current page
      // MK - Added show_hide_class support
      var items;
      if (options.show_hide_class !== '') {
        items = $items
          .addClass(options.show_hide_class)
          .slice(start_from, end_on);

        items.removeClass(options.show_hide_class);
      } else {
        items = $items.hide().slice(start_from, end_on);

        items.show();
      }

      // Reassign the active class
      $page_container
        .find(options.nav_panel_id)
        .children('.page_link[longdesc=' + page_num + ']')
        .addClass('active_page ' + jquery_ui_active_class)
        .siblings('.active_page')
        .removeClass('active_page ' + jquery_ui_active_class);

      // Set the current page meta data
      meta.data(current_page, page_num);
      /*########## Ajout de l'option page courante + nombre de pages*/
      var $current_page = parseInt(meta.data(current_page) + 1, 10);
      // Get the total number of items
      var total_items = $item_container.children().size();
      // Calculate the number of pages needed
      var $number_of_pages = Math.ceil(total_items / options.items_per_page);
      /*##################################################################*/
      $page_container.find(options.nav_info_id).html(
        options.nav_label_info
          .replace('{0}', start_from + 1)
          .replace('{1}', start_from + items.length)
          .replace(
            '{2}',
            options.ignore_filtered_count
              ? $items.not('.' + options.filtered_class).length
              : $items.length,
          )
          .replace('{3}', $current_page)
          .replace('{4}', $number_of_pages),
      );

      // Hide the more and/or less indicators
      toggleMoreLess();

      // Add a class to the next or prev links if there are no more pages next or previous to the active page
      tagNextPrev();

      // check if the onPage callback is available and call it
      if (typeof options.onPageDisplayed !== 'undefined') {
        options.onPageDisplayed.call(this, page_num + 1);
      }
    }

    // Methods to shift the diplayed index of page numbers to the left or right

    function movePageNumbersLeft(e, new_p) {
      var new_page = new_p;

      var $current_active_link = $(e).siblings('.active_page');

      if (
        $current_active_link
          .siblings('.page_link[longdesc=' + new_page + ']')
          .css('display') == 'none'
      ) {
        $nav_panels.each(function() {
          $(this)
            .children('.page_link')
            .hide() // Hide all the page links
            .slice(
              parseInt(new_page - options.num_page_links_to_display + 1, 10),
              new_page + 1,
            )
            .show();
        });
      }
    }

    function movePageNumbersRight(e, new_p) {
      var new_page = new_p;

      var $current_active_link = $(e).siblings('.active_page');

      if (
        $current_active_link
          .siblings('.page_link[longdesc=' + new_page + ']')
          .css('display') == 'none'
      ) {
        $nav_panels.each(function() {
          $(this)
            .children('.page_link')
            .hide() // Hide all the page links
            .slice(
              new_page,
              new_page + parseInt(options.num_page_links_to_display, 10),
            )
            .show();
        });
      }
    }

    // Show or remove the ellipses that indicate that more page numbers exist in the page index than are currently shown

    function toggleMoreLess() {
      if (!$nav_panels.children('.page_link:visible').hasClass('last')) {
        $nav_panels.children('.more').show();
      } else {
        $nav_panels.children('.more').hide();
      }

      if (!$nav_panels.children('.page_link:visible').hasClass('first')) {
        $nav_panels.children('.less').show();
      } else {
        $nav_panels.children('.less').hide();
      }
    }

    /* Add the style class ".no_more" to the first/prev and last/next links to allow custom styling */

    function tagNextPrev() {
      if ($nav_panels.children('.last').hasClass('active_page')) {
        $nav_panels
          .children('.next_link')
          .add('.last_link')
          .addClass('no_more ' + jquery_ui_disabled_class);
      } else {
        $nav_panels
          .children('.next_link')
          .add('.last_link')
          .removeClass('no_more ' + jquery_ui_disabled_class);
      }

      if ($nav_panels.children('.first').hasClass('active_page')) {
        $nav_panels
          .children('.previous_link')
          .add('.first_link')
          .addClass('no_more ' + jquery_ui_disabled_class);
      } else {
        $nav_panels
          .children('.previous_link')
          .add('.first_link')
          .removeClass('no_more ' + jquery_ui_disabled_class);
      }
    }
  };
})(jQuery);

/*
 * Data Tables plugin to jump to page containing particular row
 * Usage - tableApi.page.jumpToData(data, columnIdx);
 * Example:
 *
 * var oTable = $('#example').DataTable();
 * oTable.page.jumpToData('KSR 123', 1);
 */
jQuery.fn.dataTable.Api.register('page.jumpToData()', function(data, column) {
  var pos = this.column(column, { order: 'current' })
    .data()
    .indexOf(data);

  if (pos >= 0) {
    var page = Math.floor(pos / this.page.info().length);
    this.page(page).draw(false);
  }

  return this;
});

jQuery.fn.dataTableExt.oApi.fnExtStylePagingInfo = function(oSettings) {
  return {
    iStart: oSettings._iDisplayStart,
    iEnd: oSettings.fnDisplayEnd(),
    iLength: oSettings._iDisplayLength,
    iTotal: oSettings.fnRecordsTotal(),
    iFilteredTotal: oSettings.fnRecordsDisplay(),
    iPage:
      oSettings._iDisplayLength === -1
        ? 0
        : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
    iTotalPages:
      oSettings._iDisplayLength === -1
        ? 0
        : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength),
  };
};

/*
 * Data Tables pagination plugin
 * Displays a simple previous and next links
 */
jQuery.fn.dataTableExt.oPagination.myE = {
  fnInit: function(oSettings, nPaging, fnCallbackDraw) {
    var oLang = oSettings.oLanguage.oPaginate;
    var fnClickHandler = function(e) {
      e.preventDefault();
      oSettings.oApi._fnPageChange(oSettings, e.data.action);
      fnCallbackDraw(oSettings);
    };

    $(nPaging)
      .addClass('mye-dt-pagination')
      .append(
        '<a href="#" class="mye-dt-pagination-prev disabled"><i class="fa fa-angle-left"></i> ' +
          oLang.sPrevious +
          '</a> | <a href="#" class="mye-dt-pagination-next disabled">' +
          oLang.sNext +
          ' <i class="fa fa-angle-right"></i></a>',
      );
    var els = $('a', nPaging);
    $(els[0]).bind('click.DT', { action: 'previous' }, fnClickHandler);
    $(els[1]).bind('click.DT', { action: 'next' }, fnClickHandler);
  },
  fnUpdate: function(oSettings, fnDraw) {
    var iListLength = 5;
    var oPaging = oSettings.oInstance.fnExtStylePagingInfo();
    var an = oSettings.aanFeatures.p;
    var i,
      j,
      sClass,
      iStart,
      iEnd,
      iHalf = Math.floor(iListLength / 2);

    iStart = 1;
    iEnd = oPaging.iTotalPages;

    for (i = 0, iLen = an.length; i < iLen; i++) {
      // Add / remove disabled classes from the static elements
      if (oPaging.iPage === 0) {
        //    $('li:first', an[i]).addClass('disabled');
        $('a.mye-dt-pagination-prev', an[i]).addClass('disabled');
      } else {
        //    $('li:first', an[i]).removeClass('disabled');
        $('a.mye-dt-pagination-prev', an[i]).removeClass('disabled');
      }

      if (
        oPaging.iPage === oPaging.iTotalPages - 1 ||
        oPaging.iTotalPages === 0
      ) {
        //    $('li:last', an[i]).addClass('disabled');
        $('a.mye-dt-pagination-next', an[i]).addClass('disabled');
      } else {
        //    $('li:last', an[i]).removeClass('disabled');
        $('a.mye-dt-pagination-next', an[i]).removeClass('disabled');
      }
    }
  },
};

// KSR number sorting
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
  'ksr-pre': function(a) {
    return EW.Helpers.zeroPad(a.substr(4), 12);
  },

  'ksr-asc': function(a, b) {
    return a < b ? -1 : a > b ? 1 : 0;
  },

  'ksr-desc': function(a, b) {
    return a < b ? 1 : a > b ? -1 : 0;
  },
});

/*
 ***
 *** Modified from KD supplied code. Updated to search for QLAYER or SECTION if type is not provided.
 ***
 *
 * Gets the HTML element object for the supplied label.
 * For optimum performance, provide the type of element you are looking for:
 *   QLAYER_, SRVQSTN_, DYNAMIC_TEXT_, etc...
 * @method getElementObject
 * @param {String} label_id Either the menu label or the instanceId of the element
 * @param {String} type (optional) The type of element to search for
 * @return {HTMLElement} The DOM element for this label or id
   */
KD.utils.Util.getElementObject = function(label_id, type) {
  var obj, $objs;

  if (typeof type === 'undefined') {
    // Try QLAYER_ first
    obj = KD.utils.Util.getElementObject(label_id, 'QLAYER_');

    if (typeof obj === 'undefined') {
      // Try SECTION_ next
      obj = KD.utils.Util.getElementObject(label_id, 'SECTION_');

      if (typeof obj === 'undefined') {
        type = '';
      } else {
        type = 'SECTION_';
      }
    } else {
      type = 'QLAYER_';
    }
  }

  // If type is still unknown determine the type of element if possible using KD's code
  if (type === '' || type === 'no type given') {
    type = KD.utils.Util.getPrefixFromType(
      KD.utils.ClientManager.getElementType(label_id),
    );
    if (type == 'no type given') {
      type = '';
    }
  }

  /*
	 * If obj hasn't yet been found by search for QLAYER or SECTION when type is unknown
	 * then look up the element using jQuery first search for label and id starts with.
	 * If that fials try locating by id equals the label_id.
	 * The label and id starts with is most common use case.
	 */
  if (typeof obj === 'undefined') {
    $objs = $('[label="' + label_id + '"][id^="' + type + '"]');

    if ($objs.size() === 0) {
      $objs = $('[id="' + type + label_id + '"]');
    }

    if ($objs.size() > 0) {
      obj = $objs.get(0);
    } else {
      // Still can't find the element. Try KD's cache
      obj = KD.utils.Util.loadFromCache(type + label_id);
    }
  }

  return obj;
};

/**
 ***
 *** Modified from KD supplied code. Updated to add resize of section boxes
 ***
 * Inserts an element into the display of the page.  Elements below this
 * element will shift downward.
 * Uses the CSS property display:block;
 * @method insertElement
 * @param {String} label The instanceID of the element (GUID) or the label of the element.
 * @return void
 */
KD.utils.Action.insertElement = function(label) {
  /* default the label as the first argument */
  var label_id = label;
  var isRequired = false;
  /* if the argument is an array, the label id is the first item,
	 * and the second item indicates whether the element should also be
	 * made required
	 */
  if (label.constructor == Array) {
    label_id = label[0];
    isRequired = label[1];
  }
  var qLayer = KD.utils.Util.getElementObject(label_id);
  if (qLayer) {
    //If element is hidden, setting display to block still won't show element, need to set it.
    var visib = Dom.getStyle(qLayer, 'visibility');
    if (visib && visib.toUpperCase() == 'HIDDEN') {
      qLayer.style.visibility = 'visible';
    }
    //inserting only divs, so default to block. Will then override any style=none that may be in the css.
    qLayer.style.display = 'block';
    if (isRequired) {
      KD.utils.Action.makeQuestionRequired(label_id);
    }
  }

  EW.ServiceItems.resizeSectionBoxes();
};

/**
 ***
 *** Modified from KD supplied code. Updated to add resize of section boxes
 ***
 * Removes an element from the display of the page.  The space that this
 * element occuppied on the page will also be removed causing any lower
 * elements to shift up.
 * Uses the CSS property display:none;
 * @method removeElement
 * @param {String} label The instanceID of the element (GUID) or the label of the element.
 * @param {Boolean} keepValue (optional) Whether or not to clear the value of the question when removed.  Defaults to false.
 * @return void
 */
KD.utils.Action.removeElement = function(label, keepValue) {
  // default the label as the first argument
  var label_id = label;
  var isOptional = false;
  // if the argument is an array, the label id is the first item, and
  // the second item indicates whether the element should also be
  // made optional
  if (label.constructor == Array) {
    label_id = label[0];
    isOptional = label[1];
  }
  var qLayer = KD.utils.Util.getElementObject(label_id);
  if (qLayer) {
    if (!keepValue) {
      var theEls = KD.utils.Util.getElementsByClassName(
        'answerValue',
        '*',
        qLayer,
      );
      for (var i = 0; i < theEls.length; i++) {
        KD.utils.Action.setQuestionValue(
          KD.utils.Util.getIDPart(theEls[i].id),
          '$NULL$',
        );
      }
    }
    qLayer.style.display = 'none';
    if (isOptional) {
      KD.utils.Action.makeQuestionOptional(label_id);
    }
  }

  EW.ServiceItems.resizeSectionBoxes();
};

/* Rewrite of KD form validation and Alert Panel */
KD.utils.Action.validateFields = function(fieldsArray) {
  var valid = true;
  var nextElement = null;
  var firstErrorElement = null;
  var siSearchFieldList = '';
  var requiredFieldList = '';
  var formatFieldList = '';
  var requiredWarning = KD.utils.ClientManager.requiredWarningStr;
  var formatWarning = KD.utils.ClientManager.validFormatWarningStr;
  var maxCharsOnSubmit = KD.utils.ClientManager.maxCharsOnSubmit;
  var characterCount = 0;
  var disabledFields = []; /* new Array(); */
  var checkedFields = []; /* new Array(); */
  var checkboxFields = {};
  var fieldPrefix = '<li>';
  var fieldSuffix = '</li>';

  // Process all required checkbox questions first, and store the parentId
  // to indicate if any of the children have been checked
  for (var f = 0; f < fieldsArray.length; f++) {
    var cb =
      fieldsArray[f].type && fieldsArray[f].type.toLowerCase() == 'checkbox';
    if (cb) {
      var cbChecked = fieldsArray[f].checked;
      var reqAttr = fieldsArray[f].getAttribute('required');
      if (reqAttr && reqAttr.toLowerCase() == 'true') {
        var cbParentId = KD.utils.Util.getCheckboxParentId(fieldsArray[f]);
        if (cbParentId) {
          var cbParentExists =
            typeof checkboxFields[cbParentId] !== 'undefined';
          // update if this is first child, or if parent isn't already checked
          if (
            !cbParentExists ||
            (cbParentExists && checkboxFields[cbParentId] === false)
          ) {
            checkboxFields[cbParentId] = fieldsArray[f].checked;
          }
        }
      }
    }
  }

  // Process service item search fields. If they are in edit state then fail validation.
  $(":data('serviceItemSearch')").each(function(idx, el) {
    if (
      !$(this).serviceItemSearch('option', 'suppressValidation') &&
      $(this).serviceItemSearch('isEditing')
    ) {
      var fieldLabel = $(
        '.questionLabel',
        $(this)
          .parent()
          .parent(),
      )
        .text()
        .replace(':', '');
      siSearchFieldList += fieldPrefix + fieldLabel + fieldSuffix;
      valid = false;
    }
  });

  for (var i = 0; i < fieldsArray.length; i++) {
    nextElement = fieldsArray[i];
    if (
      nextElement.disabled === true &&
      nextElement.type &&
      nextElement.type.toUpperCase() !== 'SUBMIT' &&
      nextElement.type.toUpperCase() !== 'BUTTON'
    ) {
      disabledFields.push(nextElement);
    }

    // if the question is transient, skip validation for it
    if (KD.utils.Action._isTransient(nextElement)) {
      continue;
    }

    var foundValue = false;

    if (nextElement.type === 'radio' || nextElement.type === 'checkbox') {
      if (nextElement.checked) {
        characterCount += nextElement.value.length;
        foundValue = true;
      }
    } else if (nextElement.type != 'button' && nextElement.value.length > 0) {
      characterCount += nextElement.value.length;
      foundValue = true;
    }

    if (foundValue) {
      characterCount += nextElement.name.length + 1;
    }

    if (
      typeof nextElement.name !== 'undefined' &&
      nextElement.name !== '' &&
      nextElement.getAttribute('required') === 'true' &&
      checkedFields[nextElement.name] !== 'true'
    ) {
      checkedFields[nextElement.name] = 'true';

      if (nextElement.tagName.toLowerCase() == 'select') {
        if (
          nextElement.options.length === 0 ||
          nextElement.selectedIndex === null ||
          nextElement.value === ''
        ) {
          requiredFieldList +=
            fieldPrefix +
            KD.utils.Action.getErrorValue(nextElement) +
            fieldSuffix;
          if (firstErrorElement === null) {
            firstErrorElement = nextElement;
          }
          KD.utils.Action.highlightField(nextElement);
          valid = false;
        } else {
          KD.utils.Action.resetRequiredField(nextElement);
        }
      } else if (nextElement.type.toLowerCase() == 'radio') {
        var radioGroup = document.getElementsByName(nextElement.name);
        var isChecked = false;
        for (var j = 0; j < radioGroup.length; j++) {
          if (radioGroup[j].checked === true) {
            isChecked = true;
          }
        }
        if (isChecked === false) {
          if (firstErrorElement === null) {
            firstErrorElement = radioGroup[0];
          }
          KD.utils.Action.highlightField(radioGroup[0]);
          requiredFieldList +=
            fieldPrefix +
            KD.utils.Action.getErrorValue(radioGroup[0]) +
            fieldSuffix;
          valid = false;
        } else {
          KD.utils.Action.resetRequiredField(radioGroup[0]);
        }
      } else if (nextElement.type.toLowerCase() === 'checkbox') {
        cbParentId = KD.utils.Util.getCheckboxParentId(nextElement);
        if (cbParentId && typeof checkedFields[cbParentId] === 'undefined') {
          if (checkboxFields[cbParentId] !== true) {
            if (firstErrorElement === null) {
              firstErrorElement = nextElement;
            }
            KD.utils.Action.highlightField(nextElement);
            requiredFieldList +=
              fieldPrefix +
              KD.utils.Action.getErrorValue(nextElement) +
              fieldSuffix;
            valid = false;
          }
          checkedFields[cbParentId] = 'true';
        }
        if (cbParentId && checkboxFields[cbParentId] == true) {
          KD.utils.Action.resetRequiredField(nextElement);
        }
      } else if (
        (!nextElement.value || nextElement.value.length < 1) &&
        nextElement.type.toLowerCase() != 'checkbox'
      ) {
        if (firstErrorElement === null) {
          firstErrorElement = nextElement;
        }
        var isDate = false;
        var myParent = nextElement.parentNode;
        for (var iIdx = 0; iIdx < myParent.children.length; iIdx++) {
          if (
            myParent.children[iIdx].id !== undefined &&
            myParent.children[iIdx].id.indexOf('year_') !== -1
          ) {
            KD.utils.Action.highlightField(myParent.children[iIdx]);
            isDate = true;
            break;
          }
        }
        if (!isDate) {
          KD.utils.Action.highlightField(nextElement);
        }
        requiredFieldList +=
          fieldPrefix +
          KD.utils.Action.getErrorValue(nextElement) +
          fieldSuffix;
        valid = false;
        continue;
      } else {
        KD.utils.Action.resetRequiredField(nextElement);
      }
    }
    if (
      nextElement.value.length > 0 &&
      nextElement.getAttribute('validationFormat')
    ) {
      var expStr = nextElement.getAttribute('validationFormat');
      var exp = new RegExp(expStr);
      if (exp.test(nextElement.value) === false) {
        KD.utils.Action.highlightField(nextElement);
        var validationText = nextElement.getAttribute('validationText');
        if (validationText) {
          formatFieldList += fieldPrefix + validationText + fieldSuffix;
        }
        valid = false;
      } else {
        KD.utils.Action.resetRequiredField(nextElement);
      }
    } else if (
      nextElement.value.length === 0 &&
      nextElement.getAttribute('required') !== 'true' &&
      nextElement.getAttribute('validationFormat')
    ) {
      KD.utils.Action.resetRequiredField(nextElement);
    }

    // ensure date type questions contain a valid date
    if (nextElement.name && nextElement.name.indexOf('SRVQSTN_') != -1) {
      var isDateField = KD.utils.Util.isDate(nextElement);
      if (isDateField) {
        var validDate = KD.utils.Action.validateDateField(nextElement);
        if (!validDate) {
          if (firstErrorElement === null) {
            firstErrorElement = nextElement;
          }
          KD.utils.Action.highlightField(nextElement);

          // get the date field label
          var label = '';
          var labelObj = KD.utils.Util.getQuestionLabel(
            KD.utils.Util.getLabelFromAnswerEl(nextElement),
          );
          if (labelObj && labelObj.getAttribute('label')) {
            label = labelObj.getAttribute('label');
          }

          // add the date field to the format field list string
          formatFieldList +=
            fieldPrefix + label + ' is not a valid date' + fieldSuffix;
          valid = false;
        } else {
          KD.utils.Action.resetRequiredField(nextElement);
        }
      }
    }
  }

  if (!valid) {
    var errorStr = '';
    if (siSearchFieldList) {
      errorStr +=
        "<div class='warningTitle'><h3>Open search fields:</h3>You must select a result from the search result or cancel the search.<ul>" +
        siSearchFieldList +
        '</ul></div>';
    }
    if (requiredFieldList) {
      errorStr +=
        "<div class='warningTitle'><h3>" +
        requiredWarning +
        '</h3><ul>' +
        requiredFieldList +
        '</ul></div>';
    }
    if (formatFieldList) {
      errorStr +=
        "<div class='warningTitle'><h3>" +
        formatWarning +
        '</h3><ul>' +
        formatFieldList +
        '</ul></div>';
    }

    // display the fields that failed validation
    KD.utils.ClientManager.alertPanel({
      header: 'Form Validation Errors',
      body: errorStr,
      element: firstErrorElement,
    });
    return false;
  }

  if (characterCount > maxCharsOnSubmit) {
    KD.utils.ClientManager.alertPanel({
      header: 'Form Error',
      body: KD.utils.ClientManager.tooManyCharactersForSubmitStr,
      element: null,
    });
    return false;
  }

  /* Enable disabled radio buttons/checkboxes so they submit */
  for (var jj = 0; jj < disabledFields.length; jj++) {
    // make sure the element is a question
    if (disabledFields[jj].id.indexOf('SRVQSTN_') != -1) {
      disabledFields[jj].disabled = false;
    }
  }
  return true;
};

KD.utils.ClientManager.alertPanel = function(options) {
  if (options === null) {
    options = {
      header: 'Alert',
      body: '',
      element: null,
    };
  }

  $('body').prepend(
    $('<div></div>')
      .attr('id', 'overlay')
      .addClass('portalOverlay'),
  );

  var validationPanelEl = $('#validationAlert');

  if (validationPanelEl.length === 0) {
    validationPanelEl = $('<div></div>')
      .prop('id', 'validationAlert')
      .addClass('portal-dialog');
    validationPanelEl.append(
      $('<div></div>')
        .prop('id', 'validationHeader')
        .addClass('portal-dialog-header mye-larger mye-bolder-weight'),
    );
    validationPanelEl.append(
      $('<div></div>')
        .prop('id', 'validationMessage')
        .addClass('portal-dialog-message'),
    );
    validationPanelEl.append(
      $('<div></div>')
        .addClass('portal-dialog-buttons')
        .append(
          $('<div></div>')
            .prop('id', 'validationOkButton')
            .addClass('portal-dialog-button')
            .append('OK'),
        ),
    );

    validationPanelEl.appendTo('body');
  }

  $('#validationOkButton').one('click', function() {
    $('#overlay').remove();
    $('#validationAlert').hide();

    if (options.element !== null) {
      if (
        options.element &&
        options.element.focus &&
        options.element.style &&
        options.element.style.visiblity != 'hidden'
      ) {
        /* Add Because Of Select2 */
        if (
          options.element.style.display != 'none' &&
          options.element.tagName != 'SELECT'
        ) {
          try {
            // set focus back to a form field
            options.element.focus();
            options.element.select();
          } catch (error) {}
        } else {
          /* Add Because Of Select2 */
          try {
            // set focus back to a form field
            options.element.parentNode.click();
          } catch (error) {}
        }
      }
    }
  });

  $('#validationHeader').text(options.header.toUpperCase() || 'FORM ERRORS');
  $('#validationMessage').html((options.body || '').replace(/\n/g, '<br/>'));

  validationPanelEl
    .show()
    .position({ my: 'center center', at: 'center center', of: window });
};

/**
 * Adds class names to the various question elements to indicate the question
 * is required.
 *   Question Layer:  'preRequiredLayer',  'preRequiredLayer_' + type
 *   Question Label:  'preRequiredLabel',  'preRequiredLabel_' + type
 *   Answer Layer:    'preRequiredAnswer', 'preRequireAnswer_' + type
 *   Answer Input:    'preRequiredInput',  'preRequiredInput_' + type
 *
 * @method setPreRequire
 * @param {String | HTMLElement} qElement the question element, question id, or question label
 * @return void
 */
KD.utils.ClientManager.setPreRequire = function(qElement) {
  var oInput = KD.utils.ClientManager._findPreRequireObject(qElement);
  if (oInput == null) {
    return;
  }
  var type = oInput.type;
  var inputField = oInput.qInput;
  var dateInput = oInput.dateInput;
  var qEl = oInput.qElement;
  var iIdx;

  /*Check For Select2 Questions */
  var isSelect2 = false;
  var isSelect2Required = false;
  if (inputField) {
    if (inputField.className.indexOf('select2-container') > -1) {
      isSelect2 = true;
      if (
        document
          .getElementById(inputField.id.split('s2id_')[1])
          .getAttribute('required') == 'true'
      ) {
        isSelect2Required = true;
      }
    }
  }

  if (
    inputField &&
    (inputField.getAttribute('required') == 'true' ||
      (isSelect2 && isSelect2Required))
  ) {
    KD.utils.Util.removeClass(qEl, 'preRequiredRemovedLayer');
    KD.utils.Util.addClass(qEl, 'preRequiredLayer');
    KD.utils.Util.addClass(qEl, 'preRequiredLayer_' + type);

    for (iIdx = 0; iIdx < qEl.children.length; iIdx++) {
      if (
        qEl.children[iIdx].id != undefined &&
        qEl.children[iIdx].id.indexOf('QLABEL_') != -1
      ) {
        KD.utils.Util.removeClass(
          qEl.children[iIdx],
          'preRequiredRemovedLabel',
        );
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredLabel');
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredLabel_' + type);
        break;
      }
    }

    for (iIdx = 0; iIdx < qEl.children.length; iIdx++) {
      if (
        qEl.children[iIdx].id != undefined &&
        qEl.children[iIdx].id.indexOf('QANSWER_') != -1
      ) {
        KD.utils.Util.removeClass(
          qEl.children[iIdx],
          'preRequiredRemovedAnswer',
        );
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredAnswer');
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredAnswer_' + type);
        break;
      }
    }

    var inputEl = dateInput ? dateInput : inputField;
    KD.utils.Util.removeClass(inputEl, 'preRequiredRemovedInput');
    KD.utils.Util.addClass(inputEl, 'preRequiredInput');
    KD.utils.Util.addClass(inputEl, 'preRequiredInput_' + type);
  }
};

/**
 * Removes class names from the various question elements when a question is
 * no longer required.  This will remove any styling that was applied to
 * the required questions based on the pre-required class names.
 * @method clearPreRequire
 * @param {String | HTMLElement} qElement the question element, question id, or question label
 * @return void
 */
KD.utils.ClientManager.clearPreRequire = function(qElement) {
  var oInput = KD.utils.ClientManager._findPreRequireObject(qElement);
  if (oInput == null) {
    return;
  }
  var type = oInput.type;
  var inputField = oInput.qInput;
  var dateInput = oInput.dateInput;
  var qEl = oInput.qElement;
  var iIdx;

  if (inputField) {
    KD.utils.Util.removeClass(qEl, 'preRequiredLayer');
    KD.utils.Util.removeClass(qEl, 'preRequiredLayer_' + type);
    KD.utils.Util.addClass(qEl, 'preRequiredRemovedLayer');

    for (iIdx = 0; iIdx < qEl.children.length; iIdx++) {
      if (
        qEl.children[iIdx].id != undefined &&
        qEl.children[iIdx].id.indexOf('QLABEL_') != -1
      ) {
        KD.utils.Util.removeClass(qEl.children[iIdx], 'preRequiredLabel');
        KD.utils.Util.removeClass(
          qEl.children[iIdx],
          'preRequiredLabel_' + type,
        );
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredRemovedLabel');
        break;
      }
    }

    for (iIdx = 0; iIdx < qEl.children.length; iIdx++) {
      if (
        qEl.children[iIdx].id != undefined &&
        qEl.children[iIdx].id.indexOf('QANSWER_') != -1
      ) {
        KD.utils.Util.removeClass(qEl.children[iIdx], 'preRequiredAnswer');
        KD.utils.Util.removeClass(
          qEl.children[iIdx],
          'preRequiredAnswer_' + type,
        );
        KD.utils.Util.addClass(qEl.children[iIdx], 'preRequiredRemovedAnswer');
        break;
      }
    }

    var inputEl = dateInput ? dateInput : inputField;
    KD.utils.Util.removeClass(inputEl, 'preRequiredInput');
    KD.utils.Util.removeClass(inputEl, 'preRequiredInput_' + type);
    KD.utils.Util.addClass(inputEl, 'preRequiredRemovedInput');
  }
};
