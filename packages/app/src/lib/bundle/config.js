// assign jquery to $
const $ = window.$;

// Ensure the bundle global object exists
const bundle = typeof window.bundle !== 'undefined' ? window.bundle : {};

// Ensure the bundle object has a config prop
bundle.config = bundle.config || {};

bundle.config.fields = {
  text: {
    render: (field, triggerFn) => {
      if($(field.element())[0].nodeName == 'TEXTAREA'){
        window.KD.noConflict.$(field.element()).on('keyup', function(){
          $(field.element()).height(0).height(this.scrollHeight);
        });
      }
      
    },
    callback: (field) => {}
  },
  // checkbox: {
  //   render: (field, triggerFn) => {},
  //   callback: (field) => {}
  // },
  // radio: {
  //   render: (field, triggerFn) => {},
  //   callback: (field) => {}
  // },
  textarea: {
    render: (field, triggerFn) => {
      
    },
    callback: (field) => {}
  },
  dropdown: {
    render: (field, triggerFn) => {
      const displayText = $(field.element()[0]).attr('display-text');
      const firstOption = $(field.element()).children()[0];
      $(firstOption)
        .text(displayText)
        .attr('selected', true)
        .attr('disabled', true)
        .attr('hidden', true);
      // The triggerFn need to be readded to the element after the render!!!
      // If not readded the on change events defined in the Form builder will not work.
      window.KD.noConflict.$(field.element()).on('change', triggerFn);
      window.KD.noConflict.$(field.element()).on('change', function(){
        $(field.element()).css({color:'#000'})
      });
    },
    // callback: (field) => {}
  },
};
