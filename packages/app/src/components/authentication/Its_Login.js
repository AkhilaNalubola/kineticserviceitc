import React, { useEffect } from 'react';
import { Link } from '@reach/router';

import { useLocation } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import { get } from 'immutable';
import { services } from '@kineticdata/bundle-common';
import { I18n } from '@kineticdata/react';
import logo from '../../assets/images/Logo_Company_Tagline_small.jpg';

import { LoginWrapper } from './LoginWrapper';

import { Helmet } from 'react-helmet';

// import '../assets/styles/_login.scss';
import '../../assets/styles/_its_login.scss';

export const LoginForm = ({
  error,
  username,
  onChangeUsername,
  password,
  onChangePassword,
  onLogin,
  onSso,
  pending,
  children,
}) => {
  const fields = {
    username: (
      <div className="form-group">
        <label htmlFor="username">
          <I18n>User Name</I18n>
        </label>
        <input
          type="text"
          className="form-control"
          id="username"
          value={username}
          onChange={onChangeUsername}
        />
      </div>
    ),
    password: (
      <div className="form-group">
        <label htmlFor="password">
          <I18n>Password</I18n>
        </label>
        <input
          type="password"
          className="form-control"
          id="password"
          value={password}
          onChange={onChangePassword}
        />
      </div>
    ),
  };
  const buttons = {
    login: (
      <button
        className="btn btn-primary"
        type="submit"
        disabled={pending || !username || !password}
      >
        <I18n>Submit</I18n>
      </button>
    ),
  };
  return (
    <form className="login-form-container" onSubmit={onLogin}>
      {typeof children === 'function' ? (
        children({ fields, buttons })
      ) : (
        <>
          <div>{Object.values(fields)}</div>
          <div>{Object.values(buttons)}</div>
        </>
      )}
    </form>
  );
};

export const Login = ({ loginProps }) => {
  console.log('loginProps', loginProps);
  let location = useLocation();
  if (document.getElementsByClassName('widget-floating')[0]) {
    if (location.pathname == '/login') {
      document.getElementsByClassName('widget-floating')[0].style.display =
        'none';
    } else {
      document.getElementsByClassName('widget-floating')[0].style.display =
        'block';
    }
  }
  return (
    <div>
      <Helmet>
        <title>signin | Kinetic CE </title>
      </Helmet>
      <div className="">
        <div className="its-login-page   col-sm-12">
          <div className="row  ">
            <div className="col-sm-7 border-right" />
            <div className="col-sm-5 its-login-container bg-white p-5">
              <div class="card text-center">
                <div>
                  <img src={logo} className="img-fluid" width="50%" />
                </div>
                <div class="card-header font-weight-bold">Login</div>
                <div class="card-body">
                  <LoginForm {...loginProps}>
                    {({ fields, buttons }) => (
                      <>
                        <span className="text-danger">
                          <I18n>{loginProps.error || ' '}</I18n>
                        </span>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            textAlign: 'left',
                          }}
                        >
                          {fields.username}
                          {fields.password}
                        </div>
                        <div className="button-group">{buttons.login}</div>
                        <div>
                          <em>
                            <small className="text-danger">
                              For login assistance please contact your IT
                              service desk using contact methods shown below
                              <sup>*</sup>
                            </small>
                          </em>
                        </div>
                      </>
                    )}
                  </LoginForm>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
